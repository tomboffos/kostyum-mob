part of 'order_vendor_cubit.dart';

@immutable
abstract class OrderVendorState {}

class OrderVendorInitial extends OrderVendorState {}

class OrderVendorFetched extends OrderVendorState {
  final List<Order> orders;

  OrderVendorFetched({this.orders});
}
