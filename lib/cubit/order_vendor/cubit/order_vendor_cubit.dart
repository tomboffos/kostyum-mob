import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/order.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'order_vendor_state.dart';

class OrderVendorCubit extends Cubit<OrderVendorState> {
  final Repository repository;

  OrderVendorCubit({this.repository}) : super(OrderVendorInitial());

  void getOrders({search}) {
    repository
        .processVendorOrders(search: search)
        .then((value) => emit(OrderVendorFetched(orders: value)));
  }

  void updateOrder({form, Order order}) {
    repository
        .updateOrder(form: form, order: order)
        .then((value) => getOrders());
  }
}
