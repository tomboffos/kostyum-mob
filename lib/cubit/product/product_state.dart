part of 'product_cubit.dart';

@immutable
abstract class ProductState {}

class ProductInitial extends ProductState {}

class ProductFetched extends ProductState {
  final List<Product> products;
  final List<Product> oldProducts;

  ProductFetched({this.products, this.oldProducts});
}
