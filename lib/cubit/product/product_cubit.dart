import 'package:bloc/bloc.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'product_state.dart';

class ProductCubit extends Cubit<ProductState> {
  final Repository repository;
  ProductCubit({this.repository}) : super(ProductInitial());

  void getProducts({categoryId}) {
    List<Product> oldProducts = [];
    if (state is ProductFetched)
      oldProducts = (state as ProductFetched).products;

    repository.processProducts(categoryId: categoryId).then((products) {
      print(products);
      oldProducts.addAll(products);
      emit(ProductFetched(products: oldProducts));
    });
  }

  int page = 1;

  void getProdutsWithFilter({categoryId, form}) {
    List<Product> oldProducts = [];
    if (state is ProductFetched)
      oldProducts = (state as ProductFetched).products;
    repository
        .processProducts(categoryId: categoryId, filter: form)
        .then((products) {
      if (form['page'] != null) {
        oldProducts.addAll(products);
        emit(ProductFetched(products: oldProducts));
      } else {
        emit(ProductInitial());
        emit(ProductFetched(products: products));
      }
    });
  }

  void interactFavorites({productId, categoryId, BuildContext context}) {
    repository.interactFavorite(productId: productId, context: context).then(
        (value) => repository.processProducts(categoryId: categoryId, filter: {
              'sort_value': 'asc',
              'sort_type': 'price'
            }).then((products) => emit(ProductFetched(products: products))));
  }

  void storeReview({form, context}) {
    repository.storeReview(form: form, context: context).then((value) => {});
  }
}
