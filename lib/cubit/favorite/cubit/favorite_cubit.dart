import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'favorite_state.dart';

class FavoriteCubit extends Cubit<FavoriteState> {
  final Repository repository;
  FavoriteCubit({this.repository}) : super(FavoriteInitial());

  void getFavorites() {
    repository
        .processFavorites()
        .then((products) => emit(FavoriteLoaded(products: products)));
  }

  void interactFavorites({context,productId}) {
    emit(FavoriteInitial());

    repository.interactFavorite(productId: productId,context:context).then((products,) =>
        repository
            .processFavorites()
            .then((products) => emit(FavoriteLoaded(products: products))));
  }
}
