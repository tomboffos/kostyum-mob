part of 'users_cubit.dart';

@immutable
abstract class UsersState {}

class UsersInitial extends UsersState {}

class UsersReceived extends UsersState {
  final List<User> users;

  UsersReceived({this.users});
}
