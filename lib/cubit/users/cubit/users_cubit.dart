import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:kostyum/data/models/user.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'users_state.dart';

class UsersCubit extends Cubit<UsersState> {
  final Repository repository;
  UsersCubit({this.repository}) : super(UsersInitial());

  void getUsers({search}) {
    repository
        .getUsers(search: search)
        .then((value) => emit(UsersReceived(users: value)));
  }

  void clearUsers() {
    emit(UsersInitial());
  }
}
