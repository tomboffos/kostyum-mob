part of 'address_cubit.dart';

@immutable
abstract class AddressState {}

class AddressInitial extends AddressState {}

class AddressesFetched extends AddressState {
  final List<Address> addresses;

  AddressesFetched({this.addresses});
}

class AddressChoosen extends AddressState {
  final Address address;
  final List<Address> addresses;
  AddressChoosen({this.address, this.addresses});
}
