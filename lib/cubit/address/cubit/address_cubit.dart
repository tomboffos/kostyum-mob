import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/address.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'address_state.dart';

class AddressCubit extends Cubit<AddressState> {
  final Repository repository;
  AddressCubit({this.repository}) : super(AddressInitial());

  void getAddresses() {
    repository
        .processAddresses()
        .then((addresses) => emit(AddressesFetched(addresses: addresses)));
  }

  void storeAddresses({form, context}) {
    repository
        .requestAddress(form: form, context: context)
        .then((value) => getAddresses());
  }

  void chooseAddress({Address address, List<Address> addresses}) {
    emit(AddressChoosen(address: address, addresses: addresses));
  }

  void removeAddress({addressId, context}) {
    repository
        .removeProcessAddress(addressId: addressId, context: context)
        .then((value) => getAddresses());
  }
}
