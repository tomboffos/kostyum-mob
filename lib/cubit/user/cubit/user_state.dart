part of 'user_cubit.dart';

@immutable
abstract class UserState {}

class UserInitial extends UserState {}

class UserAreNotLogged extends UserState {}

class UserLogged extends UserState {
  final String token;
  final User user;
  UserLogged({this.token, this.user});
}

class UserRegisterOpen extends UserState {}

class UserForgetOpen extends UserState {}

class UserForgetPassword extends UserState {
  final User user;

  UserForgetPassword({this.user});
}

class RegisterConfirmation extends UserState {
  final User user;

  RegisterConfirmation({this.user});
}

class UserConfirmation extends UserState {
  final User user;

  UserConfirmation({this.user});
}
