import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/user.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  final Repository repository;
  UserCubit({this.repository}) : super(UserInitial());

  void checkIfAuthorized({bool faceId}) {
    repository.checkToken().then((value) => value
        ? repository.checkUserToken(faceId:faceId).then((secondValue) => secondValue != null
            ? emit(UserLogged(user: secondValue))
            : emit(UserAreNotLogged()))
        : emit(UserAreNotLogged()));
  }

  void registerOpen() {
    emit(UserRegisterOpen());
  }

  void loginOpen() {
    emit(UserAreNotLogged());
  }

  void register({Map form, context}) {
    repository
        .register(form: form, context: context)
        .then((value) => emit(RegisterConfirmation(user: value)));
  }

  Future update({Map form, context}) {
    repository
        .updateUser(form: form, context: context)
        .then((value) => checkIfAuthorized());
  }

  void login({Map form, context}) {
    repository
        .login(form: form, context: context)
        .then((value) => emit(UserLogged(user: value)));
  }

  void logout({context}) {
    repository.logout().then((value) => emit(UserAreNotLogged()));
  }

  void forgetOpen() {
    emit(UserForgetOpen());
  }

  void sendForgetRequest({context, form}) {
    repository
        .processForgetRequest(context: context, form: form)
        .then((value) => emit(UserForgetPassword(user: value)));
  }

  void setNewPassword({context, form, User user}) {
    repository
        .processNewPassword(context: context, form: form, user: user)
        .then((value) => emit(UserLogged(
              user: value,
            )));
  }

  void confirmRegistration({context, form, User user}) {
    repository
        .processConfirmation(context: context, form: form, user: user)
        .then((value) => emit(UserLogged(user: value)));
  }
}
