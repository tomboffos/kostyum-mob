part of 'cart_cubit.dart';

@immutable
abstract class CartState {}

class CartInitial extends CartState {}

class CartProcessing extends CartState {}

class CartProcessed extends CartState {
  final List<CartProduct> cart;

  CartProcessed({this.cart});
}
