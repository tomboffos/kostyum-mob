import 'package:bloc/bloc.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:kostyum/data/models/cart_product.dart';
import 'package:kostyum/data/models/order.dart';
import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'cart_state.dart';

class CartCubit extends Cubit<CartState> {
  final Repository repository;
  CartCubit({this.repository}) : super(CartInitial());

  void addProduct({product, quantity, context, size}) {
    emit(CartProcessing());
    repository.getCart().then((cart) {
      bool searched = false;
      if (cart != null)
        cart.forEach((element) {
          if (element.product.id == product.id) {
            element.quantity += quantity;
            searched = true;
          }
        });
      if (searched == false)
        cart.add(CartProduct.fromJsonProduct({
          "product": product,
          "quantity": quantity,
          "size": size != null ? size : null
        }));
      repository.storeCart(cart: cart);
      repository.networkService
          .showSuccessMessage(context: context, message: 'Добавлено в корзину');
      emit(CartProcessed(cart: cart));
      // emit(CartProcessed(cart: cart));
    });
  }

  void removeProduct({product}) {
    emit(CartProcessing());
    repository.getCart().then((cart) {
      List<CartProduct> cartToRemove = [];
      cart.forEach((element) {
        if (element.product.id == product.id) cartToRemove.add(element);
      });
      cart.removeWhere((product) => cartToRemove.contains(product));
      repository.storeCart(cart: cart);
      emit(CartProcessed(cart: cart));
    });
  }

  void editQuantity({product, quantity}) {
    emit(CartProcessing());
    repository.getCart().then((cart) {
      cart.forEach((element) {
        if (element.product.id == product) {
          element.quantity = quantity;
          if (element.quantity == 0) removeProduct(product: element.product);
        }
      });
      repository.storeCart(cart: cart);
      emit(CartProcessed(cart: cart));
    });
  }

  void getCart() {
    emit(CartProcessing());
    repository.getCart().then((cart) => emit(CartProcessed(cart: cart)));
  }

  void storeOrder({context, form}) {
    repository.processOrder(context: context, form: form);
  }

  void storeOrderByVendor({context, form}) {
    repository.processOrderByVendor(context: context, form: form);
  }

  void continueOrder({context, orderId, paymentTypeId}) {
    repository.continueOrderProcess(
        context: context, orderId: orderId, paymentTypeId: paymentTypeId);
  }

  // void updateOrder({BuildContext context, int orderId, Map form}) {
  //   repository.updateOrderProcess(form: form, orderId: orderId);
  // }
}
