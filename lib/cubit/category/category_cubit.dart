import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/category.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'category_state.dart';

class CategoryCubit extends Cubit<CategoryState> {
  final Repository repository;

  CategoryCubit({this.repository}) : super(CategoryInitial());

  void fetchCategories({categoryId}) {
    emit(CategoriesLoading());
    repository
        .getCategories(categoryId: categoryId)
        .then((categories) => emit(CategoriesFetched(categories: categories)));
  }

}
