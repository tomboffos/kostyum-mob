part of 'category_cubit.dart';

@immutable
abstract class CategoryState {}

class CategoryInitial extends CategoryState {}


class CategoriesLoading extends CategoryState {}

class CategoriesFetched extends CategoryState {
  final List<Category> categories;

  CategoriesFetched({this.categories});

}
