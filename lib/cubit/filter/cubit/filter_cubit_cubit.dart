import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/category.dart';
import 'package:kostyum/data/models/filter.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'filter_cubit_state.dart';

class FilterCubitCubit extends Cubit<FilterCubitState> {
  final Repository repository;
  FilterCubitCubit({this.repository}) : super(FilterCubitInitial());

  void getFiltersByCategory({Category category}) {
    repository
        .processFiltersByCategory(category: category)
        .then((filters) => emit(FilterFetched(filters: filters)));
  }

  void getFilters() {}
  Future saveFilters({Map filter}) {
    repository.saveFilters(filter: filter);
  }

  void clearFilters() {
    repository.clearFilter();
  }
}
