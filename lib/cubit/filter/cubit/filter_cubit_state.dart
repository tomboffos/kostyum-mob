part of 'filter_cubit_cubit.dart';

@immutable
abstract class FilterCubitState {}

class FilterCubitInitial extends FilterCubitState {}

class FilterFetched extends FilterCubitState {
  final List<Filter> filters;

  FilterFetched({this.filters});
}
