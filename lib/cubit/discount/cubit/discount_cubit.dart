import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/discount.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'discount_state.dart';

class DiscountCubit extends Cubit<DiscountState> {
  final Repository repository;
  DiscountCubit({this.repository}) : super(DiscountInitial());

  void getDiscounts() {
    repository
        .processDiscounts()
        .then((value) => emit(DiscountFetched(discounts: value)));
  }

  void fetchDiscount({price}) {
    repository
        .processDiscount(price: price)
        .then((value) => emit(DiscountOneFetched(discount: value)));
  }
}
