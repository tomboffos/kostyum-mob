part of 'discount_cubit.dart';

@immutable
abstract class DiscountState {}

class DiscountInitial extends DiscountState {}

class DiscountFetched extends DiscountState {
  final List<Discount> discounts;

  DiscountFetched({this.discounts});
}

class DiscountOneFetched extends DiscountState {
  final Discount discount;

  DiscountOneFetched({this.discount});
}
