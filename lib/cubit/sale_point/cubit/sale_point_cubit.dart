import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/sale_point.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'sale_point_state.dart';

class SalePointCubit extends Cubit<SalePointState> {
  final Repository repository;
  SalePointCubit({this.repository}) : super(SalePointInitial());

  getSalePoints() {
    repository
        .processSalePoints()
        .then((value) => emit(SalePointsFetched(salePoints: value)));
  }
}
