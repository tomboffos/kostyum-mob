part of 'sale_point_cubit.dart';

@immutable
abstract class SalePointState {}

class SalePointInitial extends SalePointState {}

class SalePointsFetched extends SalePointState {
  final List<SalePoint> salePoints;

  SalePointsFetched({this.salePoints});
}
