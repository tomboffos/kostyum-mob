import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/order.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'order_state.dart';

class OrderCubit extends Cubit<OrderState> {
  final Repository repository;
  OrderCubit({this.repository}) : super(OrderInitial());

  void getOrders() {
    repository
        .processOrders()
        .then((orders) => emit(OrdersFetched(orders: orders)));
  }

  void getCourierOrders() {
    repository
        .processCourierOrders()
        .then((orders) => emit(OrdersFetched(orders: orders)));
  }

  void updateOrder({form, int orderId, orderUser}) {
    emit(OrderInitial());
    repository.updateOrderProcess(form: form, orderId: orderId).then((value) {
      if (orderUser != null)
        getOrders();
      else
        getCourierOrders();
    });
  }
}
