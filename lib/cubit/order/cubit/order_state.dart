part of 'order_cubit.dart';

@immutable
abstract class OrderState {}

class OrderInitial extends OrderState {}

class OrdersFetched extends OrderState {
  final List<Order> orders;

  OrdersFetched({this.orders});
}
