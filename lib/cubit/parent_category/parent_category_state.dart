part of 'parent_category_cubit.dart';

@immutable
abstract class ParentCategoryState {}

class ParentCategoryInitial extends ParentCategoryState {}

class ParentCategoryChoose extends ParentCategoryState {
  final dynamic categoryId;

  ParentCategoryChoose({this.categoryId});
}
