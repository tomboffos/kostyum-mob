import 'package:bloc/bloc.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'parent_category_state.dart';

class ParentCategoryCubit extends Cubit<ParentCategoryState> {
  final Repository repository;
  ParentCategoryCubit({this.repository}) : super(ParentCategoryInitial());

  void chooseCategory({parentCategoryId}){
    emit(ParentCategoryChoose(categoryId: parentCategoryId));
  }

  void clearCategory(){
    emit(ParentCategoryInitial());
  }
}
