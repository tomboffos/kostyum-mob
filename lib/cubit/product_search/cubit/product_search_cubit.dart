import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'product_search_state.dart';

class ProductSearchCubit extends Cubit<ProductSearchState> {
  final Repository repository;
  ProductSearchCubit({this.repository}) : super(ProductSearchInitial());

  void getSearchProducts({form}) {
    emit(ProductSearchInitial());
    repository
        .processProducts(categoryId: null, filter: form)
        .then((value) => emit(ProductFounded(products: value)));
  }

  void cancelSearch() {
    emit(ProductSearchInitial());
  }
}
