part of 'product_search_cubit.dart';

@immutable
abstract class ProductSearchState {}

class ProductSearchInitial extends ProductSearchState {}

class ProductFounded extends ProductSearchInitial {
  final List<Product> products;

  ProductFounded({this.products});
}
