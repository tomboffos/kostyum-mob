part of 'ads_cubit.dart';

@immutable
abstract class AdsState {}

class AdsInitial extends AdsState {}

class AdsFetched extends AdsState {
  final List<Ad> ads;

  AdsFetched({this.ads});
}
