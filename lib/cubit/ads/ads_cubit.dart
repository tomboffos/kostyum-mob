import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/ad.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'ads_state.dart';

class AdsCubit extends Cubit<AdsState> {
  final Repository repository;
  AdsCubit({this.repository}) : super(AdsInitial());

  void getAds() {
    repository.processAds().then((ads) => emit(AdsFetched(ads: ads)));
  }
}
