import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/payment_type.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'payment_type_state.dart';

class PaymentTypeCubit extends Cubit<PaymentTypeState> {
  final Repository repository;
  PaymentTypeCubit({this.repository}) : super(PaymentTypeInitial());

  void getPaymentTypes() {
    repository.processPaymentTypes().then(
        (paymentTypes) => emit(PaymentTypeFetched(paymentTypes: paymentTypes)));
  }

  void choosePaymentTypes({paymentType, paymentTypes}) {
    emit(PaymentTypeChoosen(
        paymenType: paymentType, paymentTypes: paymentTypes));
  }
}
