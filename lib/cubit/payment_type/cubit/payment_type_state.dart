part of 'payment_type_cubit.dart';

@immutable
abstract class PaymentTypeState {}

class PaymentTypeInitial extends PaymentTypeState {}

class PaymentTypeFetched extends PaymentTypeState {
  final List<PaymentType> paymentTypes;

  PaymentTypeFetched({this.paymentTypes});
}

class PaymentTypeChoosen extends PaymentTypeState {
  final List<PaymentType> paymentTypes;
  final PaymentType paymenType;

  PaymentTypeChoosen({this.paymentTypes, this.paymenType});
}
