part of 'city_cubit.dart';

@immutable
abstract class CityState {}

class CityInitial extends CityState {}

class CityFetched extends CityState {
  final List<City> cities;

  CityFetched({this.cities});
}
