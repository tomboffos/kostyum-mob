import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/city.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'city_state.dart';

class CityCubit extends Cubit<CityState> {
  final Repository repository;
  CityCubit({this.repository}) : super(CityInitial());

  void getCities() {
    repository
        .processCities()
        .then((value) => emit(CityFetched(cities: value)));
  }
}
