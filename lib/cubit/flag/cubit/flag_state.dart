part of 'flag_cubit.dart';

@immutable
abstract class FlagState {}

class FlagInitial extends FlagState {}

class FlagLoaded extends FlagState {
  final List<dynamic> flags;

  FlagLoaded({this.flags});
}
