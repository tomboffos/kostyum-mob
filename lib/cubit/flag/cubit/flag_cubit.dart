import 'package:bloc/bloc.dart';
import 'package:kostyum/data/models/flag.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:meta/meta.dart';

part 'flag_state.dart';

class FlagCubit extends Cubit<FlagState> {
  final Repository repository;
  FlagCubit({this.repository}) : super(FlagInitial());

  void getFlags() {
    repository.processFlags().then((flags) {
      emit(FlagLoaded(flags: flags));
    });
  }
}
