import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:google_fonts/google_fonts.dart';

class ZoodPayment extends StatefulWidget {
  final String url;
  ZoodPayment({Key key, this.url}) : super(key: key);

  @override
  _ZoodPaymentState createState() => _ZoodPaymentState();
}

class _ZoodPaymentState extends State<ZoodPayment> {
  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  showSuccessStoreMessage({context, message}) {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.SUCCES,
      animType: AnimType.BOTTOMSLIDE,
      title: 'Успешно',
      desc: '$message',
      btnOkText: 'Продолжить покупки',
      btnOkOnPress: () => Navigator.pushNamed(context, '/catalog'),
      showCloseIcon: true,
    )..show();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    flutterWebviewPlugin.onUrlChanged.listen((String url) {
      if (url.contains('success')) {
        Navigator.pop(context);
        showSuccessStoreMessage(
          context: context,
          message: 'Заказ успешно оформлен',
        );
      }

      if (url.contains('failure')) {
        Navigator.pop(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: widget.url,
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Оплата',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      ),
    );
  }
}
