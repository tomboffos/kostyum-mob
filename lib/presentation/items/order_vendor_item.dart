import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/order_vendor/cubit/order_vendor_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/order.dart';

class OrderVendorItem extends StatelessWidget {
  final Order order;
  const OrderVendorItem({Key key, this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 7, vertical: 10),
      margin: EdgeInsets.only(bottom: 9),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 1.75,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    order.items.length != 0
                        ? Container(
                            height: 60,
                            width: 60,
                            margin: EdgeInsets.only(left: 24),
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: NetworkImage(
                                        '${jsonDecode(order.items[0].product['images'])[0]}'),
                                    fit: BoxFit.cover)),
                          )
                        : SizedBox.shrink(),
                    order.items.length != 0
                        ? Container(
                            // width: MediaQuery.of(context).size.width / 1.5,
                            child: Text('${order.items[0].product['name']}'),
                          )
                        : SizedBox.shrink(),
                    Container(
                      margin: EdgeInsets.only(top: 11),
                      child: Text(
                        'Пользователь : ${order.user.name}',
                        style: GoogleFonts.openSans(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: secondaryColor),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 11),
                      child: Text(
                        'Номер заказа : ${order.id}',
                        style: GoogleFonts.openSans(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: secondaryColor),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 11),
                      child: Text(
                        '${order.orderStatus.name}',
                        style: GoogleFonts.openSans(
                            fontSize: 11,
                            fontWeight: FontWeight.w700,
                            color: secondaryColor),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: Text(
                        '${order.price} тг.',
                        style: GoogleFonts.openSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: secondaryColor),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                                left: 8, top: 4, bottom: 4, right: 10),
                            decoration: BoxDecoration(
                                color: Color(0xff63B400),
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10),
                                    topLeft: Radius.circular(10))),
                            child: Text(
                              'Получено бонусов',
                              style: GoogleFonts.openSans(
                                  fontSize: 8,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                top: 1, bottom: 1, left: 11, right: 5),
                            decoration: BoxDecoration(
                                border: Border.all(color: Color(0xff63B400)),
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10),
                                    topRight: Radius.circular(10))),
                            child: Text(
                              '${order.bonus} Б',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.openSans(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff63B400)),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                                left: 8, top: 4, bottom: 4, right: 10),
                            decoration: BoxDecoration(
                                color: Color(0xffF14635),
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10),
                                    topLeft: Radius.circular(10))),
                            child: Text(
                              'Использовано бонусов',
                              style: GoogleFonts.openSans(
                                  fontSize: 8,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                top: 1, bottom: 1, left: 11, right: 5),
                            decoration: BoxDecoration(
                                border: Border.all(color: Color(0xffF14635)),
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10),
                                    topRight: Radius.circular(10))),
                            child: Text(
                              '${order.spentBonuses} Б',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.openSans(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffF14635)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30),
                      child: Text(
                        '${order.createdAt} ',
                        style: GoogleFonts.openSans(
                            fontSize: 11,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff000000)),
                        textAlign: TextAlign.right,
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              order.orderStatus.id != 5
                  ? ButtonTheme(
                      minWidth: (MediaQuery.of(context).size.width - 20) / 1.5,
                      child: RaisedButton(
                        padding: EdgeInsets.symmetric(
                          vertical: 4,
                        ),
                        color: secondaryColor,
                        onPressed: () =>
                            BlocProvider.of<OrderVendorCubit>(context)
                                .updateOrder(form: {
                          'order_status_id': '5',
                        }, order: order),
                        child: Text(
                          'Сделать возврат заказа',
                          style: GoogleFonts.openSans(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Colors.white),
                        ),
                      ),
                    )
                  : SizedBox.shrink()
            ],
          )
        ],
      ),
    );
  }
}
