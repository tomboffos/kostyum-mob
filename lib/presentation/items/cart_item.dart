import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/cart_product.dart';
import 'package:intl/intl.dart';

class CartItem extends StatelessWidget {
  final CartProduct product;
  const CartItem({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 22),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          product.product.images.length != 0
              ? GestureDetector(
                  onTap: () => Navigator.pushNamed(context, '/product',
                      arguments: product.product),
                  child: Container(
                    width: 75,
                    height: 75,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: NetworkImage(product.product.images[0]),
                            fit: BoxFit.fitHeight)),
                  ),
                )
              : SizedBox.shrink(),
          GestureDetector(
            onTap: () => Navigator.pushNamed(context, '/product',
                arguments: product.product),
            child: Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              width: MediaQuery.of(context).size.width / 2.5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width / 2.5,
                    child: Text(
                      '${product.product.name} ${product.size != null ? ", Размер : ${product.size.name}" : ''}',
                      style: GoogleFonts.openSans(
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: Color(0xff090909)),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                    children: [
                      ButtonTheme(
                          minWidth: 28,
                          child: RaisedButton(
                            padding: EdgeInsets.symmetric(
                                horizontal: 6, vertical: 2),
                            color: secondaryColor,
                            onPressed: () => BlocProvider.of<CartCubit>(context)
                                .editQuantity(
                                    product: product.product.id,
                                    quantity: product.quantity + 1),
                            child: Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                          )),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        '${product.quantity}',
                        style: GoogleFonts.openSans(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff191919)),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      ButtonTheme(
                          minWidth: 28,
                          child: RaisedButton(
                            padding: EdgeInsets.symmetric(
                                horizontal: 6, vertical: 2),
                            color: secondaryColor,
                            onPressed: () => BlocProvider.of<CartCubit>(context)
                                .editQuantity(
                                    product: product.product.id,
                                    quantity: product.quantity - 1),
                            child: Icon(
                              Icons.remove,
                              color: Colors.white,
                            ),
                          )),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: () => BlocProvider.of<CartCubit>(context)
                      .removeProduct(product: product.product),
                  child: Icon(
                    Icons.delete,
                    color: Color(
                      0xffEB5757,
                    ),
                    size: 30,
                  ),
                ),
                SizedBox(
                  height: 21,
                ),
                Text(
                  '${NumberFormat('###,000', 'zz').format(product.quantity * product.product.price)} KZT.',
                  style: GoogleFonts.openSans(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff191919)),
                ),
                SizedBox(
                  height: 7,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Color(0xff63B400),
                      borderRadius: BorderRadius.circular(10)),
                  padding:
                      EdgeInsets.only(top: 2, right: 6, bottom: 2, left: 11),
                  child: Text(
                    '${product.product.bonus * product.quantity} Б',
                    style: GoogleFonts.openSans(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: Colors.white),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
