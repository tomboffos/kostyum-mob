import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/data/models/order.dart';

class BonusItem extends StatelessWidget {
  final Order order;
  final bool earn;
  const BonusItem({Key key, this.order, this.earn}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 14),
      padding: EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.black))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Бонус за заказ номер ${order.id}',
            style: GoogleFonts.roboto(
                fontSize: 15,
                fontWeight: FontWeight.w300,
                color: Color(0xff121212)),
          ),
          Text(
            '${earn ? '+' : '-'}  ${earn ? order.bonus : order.spentBonuses} тг',
            style: GoogleFonts.roboto(
                fontSize: 15,
                fontWeight: FontWeight.w300,
                color: Color(0xff63B400)),
          )
        ],
      ),
    );
  }
}
