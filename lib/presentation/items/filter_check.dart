import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/data/constants.dart';

class FilterCheck extends StatelessWidget {
  final String name;
  final bool value;
  final action;
  const FilterCheck({Key key, this.name, this.value, this.action}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 4),
      color: Colors.white,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            name,
            style: GoogleFonts.openSans(
                fontSize: 16, fontWeight: FontWeight.w400, color: primaryColor),
          ),
          Checkbox(
            value: value,
            onChanged: action,
            activeColor: secondaryColor,
          )
        ],
      ),
    );
  }
}
