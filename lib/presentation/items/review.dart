import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/data/models/review.dart';
import 'package:kostyum/presentation/components/stars.dart';

class ReviewItem extends StatelessWidget {
  final Review review;
  const ReviewItem({Key key, this.review}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      padding: EdgeInsets.all(16),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CircleAvatar(
                backgroundColor: Colors.grey,
                foregroundImage: NetworkImage('${review.user.image}'),
              ),
              SizedBox(
                width: 12,
              ),
              Text(
                '${review.user.name}',
                style: GoogleFonts.openSans(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff262626)),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Stars(size: 20.0, rating: review.rating),
              SizedBox(
                width: 13,
              ),
              Text(
                '01.02.21',
                style: GoogleFonts.openSans(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff747474)),
              )
            ],
          ),
          review.advantages != null
              ? Container(
                  margin: EdgeInsets.symmetric(vertical: 17),
                  child: Text(
                    'Достоинства:',
                    style: GoogleFonts.openSans(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Color(0xff14CE04)),
                    textAlign: TextAlign.start,
                  ),
                )
              : SizedBox.shrink(),
          review.advantages != null
              ? Text(
                  '${review.advantages}',
                  style: GoogleFonts.openSans(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff434343)),
                )
              : SizedBox.shrink(),
          review.disadvantages != null
              ? Container(
                  margin: EdgeInsets.symmetric(vertical: 17),
                  child: Text(
                    'Недостатки:',
                    style: GoogleFonts.openSans(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Colors.red),
                    textAlign: TextAlign.start,
                  ),
                )
              : SizedBox.shrink(),
          review.disadvantages != null
              ? Text(
                  '${review.disadvantages}',
                  style: GoogleFonts.openSans(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff434343)),
                )
              : SizedBox.shrink(),
          review.comments != null
              ? Container(
                  margin: EdgeInsets.symmetric(vertical: 17),
                  child: Text(
                    'Комментарии:',
                    style: GoogleFonts.openSans(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Colors.black),
                    textAlign: TextAlign.start,
                  ),
                )
              : SizedBox.shrink(),
          review.comments != null
              ? Text(
                  '${review.comments}',
                  style: GoogleFonts.openSans(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff434343)),
                )
              : SizedBox.shrink()
        ],
      ),
    );
  }
}
