import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/data/models/ad.dart';
import 'package:kostyum/data/models/sale_point.dart';
import 'package:url_launcher/url_launcher.dart';

class AdsBanner extends StatelessWidget {
  final adsMain;
  final Ad ad;
  final int index;
  final SalePoint salePoint;

  const AdsBanner({
    Key key,
    this.adsMain,
    this.index,
    this.ad,
    this.salePoint,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: adsMain
          ? () => Navigator.pushNamed(
              context, salePoint.rent ? '/rent' : '/point',
              arguments: salePoint)
          : () => ad.link == null
              ? Navigator.pushNamed(context, '/ad/products', arguments: ad)
              : (ad.link),
      child: Container(
          width: MediaQuery.of(context).size.width / 1.2,
          margin: EdgeInsets.symmetric(horizontal: 12),
          decoration: BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(!adsMain ? ad.image : salePoint.image),
                fit: BoxFit.cover),
            color: Color(0xff000000),
            borderRadius: BorderRadius.circular(25),
          ),
          child: SizedBox.shrink()),
    );
  }
}
