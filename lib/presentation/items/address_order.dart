import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/address/cubit/address_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/address.dart';

class AddressOrder extends StatelessWidget {
  final Address address;
  final List<Address> addresses;
  const AddressOrder({Key key, this.address, this.addresses}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 8, bottom: 24),
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 14),
      decoration: BoxDecoration(
        border: Border.all(color: secondaryColor),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'г. ${address.city != null ? address.city.name : ''}, ${address.street}',
            style: GoogleFonts.openSans(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: Color(0xff010101)),
          ),
        ],
      ),
    );
  }
}
