import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kostyum/data/models/category.dart';

class CategorySlider extends StatelessWidget {
  final Category category;
  const CategorySlider({Key key, this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 75,
      height: 75,
      margin: EdgeInsets.only(left: 0, bottom: 4, right: 4),
      decoration: BoxDecoration(
          color: Colors.grey,
          image: DecorationImage(
              image: NetworkImage(category.image), fit: BoxFit.cover),
          // border: Border.all(
          //   color: Color(0xff674FED),
          //   width: 1
          // ),
          borderRadius: BorderRadius.circular(10)),
    );
  }
}
