import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/data/constants.dart';

class CardItem extends StatelessWidget {
  const CardItem({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Color(0xfffff7f7),

          boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            offset: Offset(0,1),
            spreadRadius: 2
          )
        ]
      ),
      margin: EdgeInsets.only(top: 24),
      padding: EdgeInsets.symmetric(vertical: 11, horizontal: 16),
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
              image:DecorationImage(
                image: NetworkImage('https://7kun.kz/wp-content/uploads/2017/10/MasterCard-vale-a-pena.jpeg'),
                fit: BoxFit.fill
              )
            ),
          ),
          SizedBox(
            width: 12,
          ),
          Text(
            '**** 5545',
            style: GoogleFonts.openSans(
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: Color(0xff191919)),
          ),
          SizedBox(width: MediaQuery.of(context).size.width/2.5,),
          Container(
            margin: EdgeInsets.only(right: 40),
            padding: EdgeInsets.all(2),
            decoration: BoxDecoration(
              color: secondaryColor,
              borderRadius: BorderRadius.circular(100),
            ),
            child: Icon(Icons.check,color: Colors.white,),
          ),
          Icon(Icons.chevron_right,color: secondaryColor,)

        ],
      ),
    );
  }
}
