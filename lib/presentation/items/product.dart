import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/favorite/cubit/favorite_cubit.dart';
import 'package:kostyum/cubit/product/product_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/presentation/screens/product_catalog.dart';
import 'package:intl/intl.dart';

class ProductItem extends StatefulWidget {
  final int categoryId;
  final openSlide;
  final Product product;
  final favoritePage;
  final action;

  const ProductItem(
      {Key key,
      this.openSlide,
      this.product,
      this.categoryId,
      this.favoritePage,
      this.action})
      : super(key: key);

  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 6, right: 6, bottom: 25, top: 3),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: Color(0xffffffff),
          boxShadow: [
            BoxShadow(
              color: Color(0xff000000),
              // offset: Offset(4,4),
              blurRadius: 2,
              // spreadRadius: 1,
            )
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () => Navigator.pushNamed(context, '/product',
                arguments: widget.product),
            child: Container(
              child: Stack(
                children: [
                  widget.product.bonus != 0
                      ? Positioned(
                          top: 16,
                          child: Container(
                            alignment: Alignment.topLeft,
                            width: 47,
                            height: 25,
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 6),
                            color: Color(0xff63B400),
                            child: Text(
                              '${widget.product.bonus} Б',
                              style: GoogleFonts.openSans(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xffffffff)),
                            ),
                          ),
                        )
                      : SizedBox.shrink(),
                  widget.product.oldPrice != null
                      ? Positioned(
                          top: 50,
                          child: Container(
                            alignment: Alignment.center,
                            width: 47,
                            height: 25,
                            color: Color(0xffF14635),
                            child: Text(
                              '-${widget.product.discount}%',
                              style: GoogleFonts.openSans(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xffffffff)),
                            ),
                          ),
                        )
                      : SizedBox.shrink(),
                  Positioned(
                      top: 8,
                      right: 10,
                      child: GestureDetector(
                        onTap: () => widget.favoritePage
                            ? BlocProvider.of<FavoriteCubit>(context)
                                .interactFavorites(
                                    productId: widget.product.id,
                                    context: context)
                            : BlocProvider.of<ProductCubit>(context)
                                .interactFavorites(
                                    context: context,
                                    productId: widget.product.id,
                                    categoryId: widget.categoryId),
                        child: Icon(
                          widget.product.favorites
                              ? Icons.favorite
                              : Icons.favorite_border,
                          color: Colors.red,
                          size: 25,
                        ),
                      )),
                  widget.product.video == null
                      ? SizedBox.shrink()
                      : Positioned(
                          top: 36,
                          right: 10,
                          child: GestureDetector(
                            child: FaIcon(
                              FontAwesomeIcons.youtube,
                              color: Colors.red,
                              size: 25,
                            ),
                          ))
                ],
              ),
              width: 400,
              height: 140,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                      '${widget.product.images.length != 0 ? widget.product.images[0] : ''}'),
                  fit: BoxFit.fitHeight,
                ),
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(15),
                    topLeft: Radius.circular(15)),
              ),
            ),
          ),
          Container(
            height: 30,
            margin: EdgeInsets.symmetric(vertical: 14),
            child: Text(
              '${widget.product.name}',
              textAlign: TextAlign.center,
              style: GoogleFonts.roboto(
                  fontSize: 13,
                  fontWeight: FontWeight.w400,
                  color: secondDarkColor),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 19),
            child: ButtonTheme(
              minWidth: 185,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(0),
                ),
                color: Color(0xff674FED),
                onPressed: widget.action != null
                    ? widget.action
                    : () => widget.product.stock
                        ? widget.openSlide(productMain: widget.product)
                        : null,
                child: Text(
                  widget.product.stock
                      ? '${NumberFormat('###,000', 'zz').format(widget.product.price)} KZT'
                      : 'Нет в наличии',
                  style: GoogleFonts.montserrat(
                      color: Color(0xffffffff),
                      fontWeight: FontWeight.w400,
                      fontSize: 14),
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 9, vertical: 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Icon(
                      Icons.star,
                      size: 13,
                      color: widget.product.rating >= 1
                          ? Color(0xffF3F804)
                          : Colors.grey,
                    ),
                    Icon(
                      Icons.star,
                      size: 13,
                      color: widget.product.rating >= 2
                          ? Color(0xffF3F804)
                          : Colors.grey,
                    ),
                    Icon(
                      Icons.star,
                      size: 13,
                      color: widget.product.rating >= 3
                          ? Color(0xffF3F804)
                          : Colors.grey,
                    ),
                    Icon(
                      Icons.star,
                      size: 13,
                      color: widget.product.rating >= 4
                          ? Color(0xffF3F804)
                          : Colors.grey,
                    ),
                    Icon(
                      Icons.star,
                      size: 13,
                      color: widget.product.rating >= 5
                          ? Color(0xffF3F804)
                          : Colors.grey,
                    ),
                  ],
                ),
                Text(
                  '${widget.product.reviews.length} отзывов',
                  style: GoogleFonts.roboto(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff000000)),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
