import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/cubit/order/cubit/order_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/order.dart';

class OrderItem extends StatefulWidget {
  final Order order;
  const OrderItem({Key key, this.order}) : super(key: key);

  @override
  State<OrderItem> createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItem> {
  orderPaymentTypeChooseDialog(context, Order order) {
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              content: Container(
                height: 200,
                width: 400,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ButtonTheme(
                      minWidth: MediaQuery.of(context).size.width - 26,
                      child: RaisedButton(
                        padding: EdgeInsets.symmetric(
                          vertical: 14,
                        ),
                        color: secondaryColor,
                        onPressed: () {
                          BlocProvider.of<CartCubit>(context).continueOrder(
                              context: context,
                              orderId: widget.order.id,
                              paymentTypeId: 3);
                          Navigator.pop(_);
                        },
                        child: Text(
                          'Онлайн оплата от Halyk Bank',
                          style: GoogleFonts.openSans(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Colors.white),
                        ),
                      ),
                    ),
                    ButtonTheme(
                      minWidth: MediaQuery.of(context).size.width - 26,
                      child: RaisedButton(
                        padding: EdgeInsets.symmetric(
                          vertical: 14,
                        ),
                        color: secondaryColor,
                        onPressed: () {
                          BlocProvider.of<CartCubit>(context).continueOrder(
                              context: context,
                              orderId: widget.order.id,
                              paymentTypeId: 1);
                          Navigator.pop(_);
                        },
                        child: Text(
                          'Оплата в рассрочку с Zoodpay',
                          style: GoogleFonts.openSans(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Colors.white),
                        ),
                      ),
                    ),
                    ButtonTheme(
                      minWidth: MediaQuery.of(context).size.width - 26,
                      child: RaisedButton(
                        padding: EdgeInsets.symmetric(
                          vertical: 14,
                        ),
                        color: secondaryColor,
                        onPressed: () => Navigator.pop(_),
                        child: Text(
                          'Отменить',
                          style: GoogleFonts.openSans(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 7, vertical: 10),
      margin: EdgeInsets.only(bottom: 9),
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 1.75,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    widget.order.items != null
                        ? widget.order.items.length != 0
                            ? Container(
                                height: 60,
                                width: 60,
                                margin: EdgeInsets.only(left: 24),
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: NetworkImage(
                                            '${widget.order.items[0].product['images'] != null ? 'https://kostyum.it-lead.net/' + jsonDecode(widget.order.items[0].product['images'])[0] : ''}'),
                                        fit: BoxFit.cover)),
                              )
                            : SizedBox.shrink()
                        : SizedBox.shrink(),
                    widget.order.items != null
                        ? widget.order.items.length != 0
                            ? Container(
                                // width: MediaQuery.of(context).size.width / 1.5,
                                child: Text(
                                    '${widget.order.items[0].product['name']}'),
                              )
                            : SizedBox.shrink()
                        : SizedBox.shrink(),
                    Container(
                      margin: EdgeInsets.only(top: 11),
                      child: Text(
                        'Номер заказа : ${widget.order.id}',
                        style: GoogleFonts.openSans(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: secondaryColor),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 11),
                      child: Text(
                        '${widget.order.orderStatus.name}, ${widget.order.paymentStatus.name}',
                        style: GoogleFonts.openSans(
                            fontSize: 11,
                            fontWeight: FontWeight.w700,
                            color: secondaryColor),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: Text(
                        '${NumberFormat('###,000', 'zz').format(widget.order.price - widget.order.spentBonuses)} KZT.',
                        style: GoogleFonts.openSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: secondaryColor),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                                left: 8, top: 4, bottom: 4, right: 10),
                            decoration: BoxDecoration(
                                color: Color(0xff63B400),
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10),
                                    topLeft: Radius.circular(10))),
                            child: Text(
                              'Получено бонусов',
                              style: GoogleFonts.openSans(
                                  fontSize: 8,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                top: 1, bottom: 1, left: 11, right: 5),
                            decoration: BoxDecoration(
                                border: Border.all(color: Color(0xff63B400)),
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10),
                                    topRight: Radius.circular(10))),
                            child: Text(
                              '${widget.order.bonus} Б',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.openSans(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff63B400)),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                                left: 8, top: 4, bottom: 4, right: 10),
                            decoration: BoxDecoration(
                                color: Color(0xffF14635),
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(10),
                                    topLeft: Radius.circular(10))),
                            child: Text(
                              'Использовано бонусов',
                              style: GoogleFonts.openSans(
                                  fontSize: 8,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                top: 1, bottom: 1, left: 11, right: 5),
                            decoration: BoxDecoration(
                                border: Border.all(color: Color(0xffF14635)),
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10),
                                    topRight: Radius.circular(10))),
                            child: Text(
                              '${widget.order.spentBonuses} Б',
                              textAlign: TextAlign.right,
                              style: GoogleFonts.openSans(
                                  fontSize: 11,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xffF14635)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30),
                      child: Text(
                        '${widget.order.createdAt} ',
                        style: GoogleFonts.openSans(
                            fontSize: 11,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff000000)),
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          widget.order.paymentStatus.id == 1 && widget.order.orderStatus.id != 6
              ? Container(
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(vertical: 25, horizontal: 10),
                  child: ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width - 26,
                    child: RaisedButton(
                      padding: EdgeInsets.symmetric(
                        vertical: 14,
                      ),
                      color: secondaryColor,
                      onPressed: () =>
                          orderPaymentTypeChooseDialog(context, widget.order),
                      child: Text(
                        'Продолжить заказ',
                        style: GoogleFonts.openSans(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                    ),
                  ),
                )
              : SizedBox.shrink(),
          widget.order.orderStatus.id != 6 && widget.order.paymentStatus.id == 1
              ? Container(
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
                  child: ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width - 26,
                    child: RaisedButton(
                      padding: EdgeInsets.symmetric(
                        vertical: 14,
                      ),
                      color: secondaryColor,
                      onPressed: () => BlocProvider.of<OrderCubit>(context)
                          .updateOrder(
                              orderId: widget.order.id,
                              form: {'order_status_id': '6'},
                              orderUser: true),
                      child: Text(
                        'Отменить заказ',
                        style: GoogleFonts.openSans(
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                    ),
                  ),
                )
              : SizedBox.shrink()
        ],
      ),
    );
  }
}
