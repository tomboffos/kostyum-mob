import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/data/models/discount.dart';

class DiscountItem extends StatefulWidget {
  final Discount discount;
  const DiscountItem({Key key, this.discount}) : super(key: key);

  @override
  _DiscountItemState createState() => _DiscountItemState();
}

class _DiscountItemState extends State<DiscountItem> {
  bool showText = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 13),
      padding: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.black))),
      child: Column(
        children: [
          GestureDetector(
            onTap: () => setState(() => showText = !showText),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${widget.discount.name}',
                  style: GoogleFonts.roboto(
                      fontSize: 17,
                      fontWeight: FontWeight.w300,
                      color: Color(0xff121212)),
                ),
                Row(
                  children: [
                    Text(
                      '+${widget.discount.value}${widget.discount.bonusType == 'percent' ? '%' : 'KZT'}',
                      style: GoogleFonts.roboto(
                          fontSize: 15,
                          fontWeight: FontWeight.w300,
                          color: Color(0xff63b400)),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Icon(
                      Icons.arrow_downward,
                      color: Color(0xff121212),
                      size: 15,
                    )
                  ],
                )
              ],
            ),
          ),
          Visibility(
            visible: showText,
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 4),
              child: Text(
                '${widget.discount.description}',
                style: GoogleFonts.roboto(
                  fontSize: 13,
                  fontWeight: FontWeight.w300,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          )
        ],
      ),
    );
  }
}
