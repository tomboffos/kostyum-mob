import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/data/models/flag.dart';

class TypeSlider extends StatelessWidget {
  final Flag flag;
  const TypeSlider({Key key, this.flag}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 21, vertical: 6),
      margin: EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: Color(0xffffffff),
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Color(0xff000000))),
      child: Center(
        child: Text(
          flag.name,
          style: GoogleFonts.roboto(color: Color(0xff000000)),
        ),
      ),
    );
  }
}
