import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/data/constants.dart';

class CategoryCatalog extends StatelessWidget {
  final onTap;
  final name;
  const CategoryCatalog({Key key, this.onTap, this.name}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.only(bottom: 4),
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: 20,vertical: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              name,
              style: GoogleFonts.openSans(
                  fontSize: 16, fontWeight: FontWeight.w400, color: primaryColor),
            ),

            Container(
              width: 9,
              height: 9,
              decoration: BoxDecoration(
                color: Color(0xff674fed),
                borderRadius: BorderRadius.circular(50)
              ),
            )


          ],
        ),
      ),
    );
  }
}
