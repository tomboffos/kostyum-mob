import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/address/cubit/address_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/address.dart';

class AddressItem extends StatelessWidget {
  final Address address;
  const AddressItem({Key key, this.address}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 32),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: () => BlocProvider.of<AddressCubit>(context)
                    .removeAddress(addressId: address.id, context: context),
                child: Text('Удалить адрес'),
              )
            ],
          ),
          address.city != null
              ? Text(
                  'Город доставки',
                  style: GoogleFonts.openSans(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: secondaryColor),
                )
              : SizedBox.shrink(),
          address.city != null
              ? Container(
                  margin: EdgeInsets.only(top: 8, bottom: 24),
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 14),
                  decoration: BoxDecoration(
                    border: Border.all(color: secondaryColor),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        address.city.name,
                        style: GoogleFonts.openSans(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff010101)),
                      ),
                      Icon(
                        Icons.place,
                        color: secondaryColor,
                      )
                    ],
                  ),
                )
              : SizedBox.shrink(),
          Text(
            'Улица дом квартира',
            style: GoogleFonts.openSans(
                fontSize: 14,
                fontWeight: FontWeight.w600,
                color: secondaryColor),
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 24),
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 14),
            decoration: BoxDecoration(
              border: Border.all(color: secondaryColor),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  address.street.toString(),
                  style: GoogleFonts.openSans(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff010101)),
                ),
                Icon(
                  Icons.place,
                  color: secondaryColor,
                )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              address.flatNumber != null
                  ? Container(
                      child: Column(children: [
                        Text(
                          'Комментарии',
                          style: GoogleFonts.openSans(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: secondaryColor),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          width: MediaQuery.of(context).size.width - 64,
                          decoration: BoxDecoration(
                              border: Border.all(color: secondaryColor)),
                          padding: EdgeInsets.symmetric(
                              vertical: 14, horizontal: 16),
                          child: Text(address.flatNumber),
                        )
                      ], crossAxisAlignment: CrossAxisAlignment.start),
                    )
                  : SizedBox.shrink(),
            ],
          )
        ],
      ),
    );
  }
}
