import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/user.dart';

class SetPasswordComponent extends StatefulWidget {
  final User user;
  SetPasswordComponent({Key key, this.user}) : super(key: key);

  @override
  _SetPasswordComponentState createState() => _SetPasswordComponentState();
}

class _SetPasswordComponentState extends State<SetPasswordComponent> {
  TextEditingController code = new TextEditingController();
  TextEditingController password = new TextEditingController();
  bool canSendSmsCode = false;
  int seconds = 60;
  Timer _timer;
  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      setState(() {
        seconds -= 1;
      });
      if (seconds == 0) {
        setState(() {
          canSendSmsCode = true;
        });
        _timer.cancel();
      }
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
              child: Text(
            'Введите пожалуйста новый пароль и SMS код ',
            style: TextStyle(fontWeight: FontWeight.w600),
          )),
          SizedBox(
            height: 20,
          ),
          TextField(
            controller: code,
            decoration: InputDecoration(
              hintText: 'Введите смс код',
              contentPadding:
                  EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          TextField(
            obscureText: true,
            controller: password,
            decoration: InputDecoration(
              hintText: 'Новый пароль',
              contentPadding:
                  EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          ButtonTheme(
            minWidth: MediaQuery.of(context).size.width,
            splashColor: Colors.white,
            child: RaisedButton(
              padding: EdgeInsets.symmetric(
                vertical: 14,
              ),
              color: secondaryColor,
              onPressed: () {
                if (canSendSmsCode) {
                  BlocProvider.of<UserCubit>(context).sendForgetRequest(
                      context: context, form: {'phone': widget.user.phone});
                  setState(() {
                    seconds = 60;
                    canSendSmsCode = false;
                  });
                  Timer.periodic(Duration(seconds: 1), (timer) {
                    setState(() {
                      seconds -= 1;
                    });
                    if (seconds == 0) {
                      setState(() {
                        canSendSmsCode = true;
                      });
                      _timer.cancel();
                    }
                  });
                }
              },
              child: Text(
                canSendSmsCode
                    ? 'Переотправить SMS'
                    : 'Отправить SMS через ${seconds.toString()} секунд',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          ButtonTheme(
            minWidth: MediaQuery.of(context).size.width,
            splashColor: Colors.white,
            child: RaisedButton(
              padding: EdgeInsets.symmetric(
                vertical: 14,
              ),
              color: secondaryColor,
              onPressed: () => BlocProvider.of<UserCubit>(context)
                  .setNewPassword(
                      context: context,
                      form: {'password': password.text, 'code': code.text},
                      user: widget.user),
              child: Text(
                'Изменить пароль',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
