import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kostyum/cubit/product/product_cubit.dart';
import 'package:kostyum/data/models/flag.dart';
import 'package:kostyum/presentation/items/types_slider.dart';

class CategoryTypeSlider extends StatelessWidget {
  final List<dynamic> flags;
  final int categoryId;

  const CategoryTypeSlider({Key key, this.flags, this.categoryId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 20, bottom: 15),
      height: 40,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: flags.length,
          itemBuilder: (context, index) => GestureDetector(
                onTap: () => BlocProvider.of<ProductCubit>(context)
                    .getProdutsWithFilter(categoryId: categoryId, form: {
                  'flags[]': flags[index].id.toString(),
                  "sort_type": "price",
                  "sort_value": "ASC"
                }),
                child: TypeSlider(
                  flag: flags[index],
                ),
              )),
    );
  }
}
