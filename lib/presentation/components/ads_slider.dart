import 'package:flutter/cupertino.dart';
import 'package:kostyum/data/models/ad.dart';
import 'package:kostyum/data/models/sale_point.dart';
import 'package:kostyum/presentation/items/ads_banner.dart';

class AdsSlider extends StatelessWidget {
  final adsMain;
  final List<Ad> ads;
  final List<SalePoint> salePoints;
  const AdsSlider({Key key, this.adsMain, this.ads, this.salePoints})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 200,
      margin: EdgeInsets.symmetric(vertical: 12),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) => AdsBanner(
                adsMain: adsMain,
                index: index,
                ad: ads != null ? ads[index] : null,
                salePoint: adsMain ? salePoints[index] : null,
              ),
          itemCount: !adsMain ? ads.length : salePoints.length),
    );
  }
}
