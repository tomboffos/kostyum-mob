import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/data/constants.dart';

class Login extends StatefulWidget {
  const Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController phone =
      new MaskedTextController(mask: '+0 (000) 000-00-00');
  TextEditingController password = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Center(
            child: Text(
          'Пожалуйста войдите чтобы посмотреть избранное',
          textAlign: TextAlign.center,
          style: GoogleFonts.openSans(
              fontSize: 14, fontWeight: FontWeight.bold, color: Colors.black),
        )),
        SizedBox(
          height: 20,
        ),
        TextField(
          controller: phone,
          decoration: InputDecoration(
            hintText: 'Номер телефона',
            contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            focusedBorder:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
            enabledBorder:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        TextField(
          obscureText: true,
          controller: password,
          decoration: InputDecoration(
            hintText: 'Пароль',
            contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            focusedBorder:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
            enabledBorder:
                OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        ButtonTheme(
          minWidth: MediaQuery.of(context).size.width,
          splashColor: Colors.white,
          child: RaisedButton(
            padding: EdgeInsets.symmetric(
              vertical: 14,
            ),
            color: secondaryColor,
            onPressed: () => BlocProvider.of<UserCubit>(context).login(form: {
              "phone": phone.text
                  .replaceAll('+7', '8')
                  .replaceAll(' ', '')
                  .replaceAll(')', '')
                  .replaceAll('(', '')
                  .replaceAll('-', ''),
              "password": password.text
            }, context: context),
            child: Text(
              'Войти',
              style: GoogleFonts.openSans(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        ButtonTheme(
          minWidth: MediaQuery.of(context).size.width,
          splashColor: Colors.white,
          child: RaisedButton(
            padding: EdgeInsets.symmetric(
              vertical: 14,
            ),
            color: secondaryColor,
            onPressed: () => BlocProvider.of<UserCubit>(context).registerOpen(),
            child: Text(
              'Зарегистрироваться',
              style: GoogleFonts.openSans(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        GestureDetector(
            onTap: () => BlocProvider.of<UserCubit>(context).forgetOpen(),
            child: Text('Забыли пароль?'))
      ],
    );
  }
}
