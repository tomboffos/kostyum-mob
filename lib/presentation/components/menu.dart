import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/cubit/favorite/cubit/favorite_cubit.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/presentation/screens/cart/index.dart';
import 'package:kostyum/presentation/screens/catalog.dart';
import 'package:kostyum/presentation/screens/favorite.dart';
import 'package:kostyum/presentation/screens/home.dart';
import 'package:kostyum/presentation/screens/profile/index.dart';
import 'package:kostyum/router.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class Menu extends StatefulWidget {
  final AppRouter router;

  const Menu({Key key, this.router}) : super(key: key);
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  int _currentIndex = 0;
  final _navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
  ];

  onTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  changeIndex({index}) {
    setState(() {
      _currentIndex = index;
    });
  }

  List<Widget> _mainPages = [
    HomePage(),
    Catalog(),
    CartIndex(),
    Favorite(),
    ProfileIndex(),
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<UserCubit>(context).checkIfAuthorized(faceId: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WillPopScope(
        onWillPop: () async {
          final isFirstRouteInCurrentTab =
              await _navigatorKeys[_currentIndex].currentState.maybePop();
          print(isFirstRouteInCurrentTab);
          if (isFirstRouteInCurrentTab) {
            // if not on the 'main' tab
            return false;
          }
          // let system handle back button if we're on the first route
          return true;
        },
        child: IndexedStack(
          index: _currentIndex,
          children: [
            Navigator(
              key: _navigatorKeys[0],
              onGenerateRoute: widget.router.generateMainRouter,
              initialRoute: '/start',
            ),
            Navigator(
              key: _navigatorKeys[1],
              onGenerateRoute: widget.router.generateCatalogRoute,
              initialRoute: '/catalog',
            ),
            Navigator(
              key: _navigatorKeys[2],
              onGenerateRoute: widget.router.generateCartIndex,
              initialRoute: '/cart',
            ),
            Navigator(
              key: _navigatorKeys[3],
              onGenerateRoute: widget.router.generateFavoriteRoute,
              initialRoute: '/favorite',
            ),
            Navigator(
              key: _navigatorKeys[4],
              onGenerateRoute: widget.router.generateProfileRoute,
              initialRoute: '/profile',
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.white,
        selectedLabelStyle: GoogleFonts.openSans(color: Colors.white),
        unselectedLabelStyle: GoogleFonts.openSans(color: Colors.white),
        backgroundColor: Colors.black,
        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                color: Colors.white,
              ),
              title: Text(
                'Главная',
                style: GoogleFonts.openSans(color: Colors.white),
              ),
              backgroundColor: Colors.black),
          BottomNavigationBarItem(
              icon: Icon(Icons.format_list_bulleted_outlined,
                  color: Colors.white),
              title: Text(
                'Каталог',
                style: GoogleFonts.openSans(color: Colors.white),
              ),
              backgroundColor: Colors.black),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart, color: Colors.white),
              title: Text(
                'Корзина',
                style: GoogleFonts.openSans(color: Colors.white),
              ),
              backgroundColor: Colors.black),
          BottomNavigationBarItem(
              icon: Icon(Icons.favorite, color: Colors.white),
              title: Text(
                'Избранное',
                style: GoogleFonts.openSans(color: Colors.white),
              ),
              backgroundColor: Colors.black),
          BottomNavigationBarItem(
              icon: Icon(Icons.person, color: Colors.white),
              title: Text(
                'Профиль',
                style: GoogleFonts.openSans(color: Colors.white),
              ),
              backgroundColor: Colors.black)
        ],
        currentIndex: _currentIndex,
        onTap: onTapped,
      ),
    );
  }
}
