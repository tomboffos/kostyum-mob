import 'package:flutter/material.dart';

class Stars extends StatelessWidget {
  final size;
  final int rating;
  const Stars({Key key, this.size, this.rating}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Icon(
          Icons.star,
          size: size,
          color: rating >= 1 ? Color(0xffF3F804) : Colors.grey,
        ),
        Icon(
          Icons.star,
          size: size,
          color: rating >= 2 ? Color(0xffF3F804) : Colors.grey,
        ),
        Icon(
          Icons.star,
          size: size,
          color: rating >= 3 ? Color(0xffF3F804) : Colors.grey,
        ),
        Icon(
          Icons.star,
          size: size,
          color: rating >= 4 ? Color(0xffF3F804) : Colors.grey,
        ),
        Icon(
          Icons.star,
          size: size,
          color: rating >= 5 ? Color(0xffF3F804) : Colors.grey,
        ),
      ],
    );
  }
}
