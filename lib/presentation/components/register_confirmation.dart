import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/user.dart';

class RegisterConfirmationPage extends StatefulWidget {
  final User user;
  RegisterConfirmationPage({Key key, this.user}) : super(key: key);

  @override
  _RegisterConfirmationPageState createState() =>
      _RegisterConfirmationPageState();
}

class _RegisterConfirmationPageState extends State<RegisterConfirmationPage> {
  TextEditingController code = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
              child: Text(
            'Введите пожалуйста SMS код',
            style: TextStyle(fontWeight: FontWeight.w600),
          )),
          SizedBox(
            height: 20,
          ),
          TextField(
            controller: code,
            decoration: InputDecoration(
              hintText: 'Введите смс код',
              contentPadding:
                  EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          ButtonTheme(
            minWidth: MediaQuery.of(context).size.width,
            splashColor: Colors.white,
            child: RaisedButton(
              padding: EdgeInsets.symmetric(
                vertical: 14,
              ),
              color: secondaryColor,
              onPressed: () => BlocProvider.of<UserCubit>(context)
                  .confirmRegistration(
                      context: context,
                      form: {'code': code.text},
                      user: widget.user),
              child: Text(
                'Подтвердить',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
