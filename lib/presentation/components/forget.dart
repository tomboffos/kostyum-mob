import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/data/constants.dart';

class ForgetComponent extends StatefulWidget {
  const ForgetComponent({Key key}) : super(key: key);

  @override
  _ForgetComponentState createState() => _ForgetComponentState();
}

class _ForgetComponentState extends State<ForgetComponent> {
  TextEditingController phone =
      new MaskedTextController(mask: '+0 (000) 000-00-00');

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
              child: Text(
            'Введите пожалуйста ваш номер ',
            style: TextStyle(fontWeight: FontWeight.w600),
          )),
          SizedBox(
            height: 20,
          ),
          TextField(
            controller: phone,
            decoration: InputDecoration(
              hintText: 'Номер телефона',
              contentPadding:
                  EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          ButtonTheme(
            minWidth: MediaQuery.of(context).size.width,
            splashColor: Colors.white,
            child: RaisedButton(
              padding: EdgeInsets.symmetric(
                vertical: 14,
              ),
              color: secondaryColor,
              onPressed: () => BlocProvider.of<UserCubit>(context)
                  .sendForgetRequest(context: context, form: {
                'phone': phone.text
                    .replaceAll('+7', '8')
                    .replaceAll(' ', '')
                    .replaceAll(')', '')
                    .replaceAll('(', '')
                    .replaceAll('-', ''),
              }),
              child: Text(
                'Отправить SMS',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          GestureDetector(
            onTap: () => BlocProvider.of<UserCubit>(context).logout(),
            child: Center(
              child: Text('Назад'),
            ),
          )
        ],
      ),
    );
  }
}
