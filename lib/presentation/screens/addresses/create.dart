import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/address/cubit/address_cubit.dart';
import 'package:kostyum/cubit/city/cubit/city_cubit.dart';
import 'package:kostyum/data/constants.dart';

class AddressCreate extends StatefulWidget {
  const AddressCreate({Key key}) : super(key: key);

  @override
  _AddressCreateState createState() => _AddressCreateState();
}

class _AddressCreateState extends State<AddressCreate> {
  TextEditingController street = new TextEditingController();
  TextEditingController flatNumber = new TextEditingController();
  TextEditingController entrance = new TextEditingController();
  String cityId = '1';
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<CityCubit>(context).getCities();
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Добавить адрес',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
          child: Container(
        margin: EdgeInsets.only(bottom: 32, left: 32, right: 32, top: 32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Город доставки',
              style: GoogleFonts.openSans(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: secondaryColor),
            ),
            BlocBuilder<CityCubit, CityState>(
              builder: (context, state) {
                if (!(state is CityFetched))
                  return Center(child: CircularProgressIndicator());
                final cities = (state as CityFetched).cities;
                return Container(
                  margin: EdgeInsets.only(top: 20, bottom: 24),
                  child: DropdownButton(
                    isDense: true,
                    onChanged: (value) {
                      setState(() {
                        cityId = value.toString();
                      });
                    },
                    isExpanded: true,
                    value: cityId,
                    items: cities
                        .map((e) => DropdownMenuItem(
                              child: Text(e.name),
                              value: e.id.toString(),
                            ))
                        .toList(),
                  ),
                );
              },
            ),
            Text(
              'Улица дом квартира',
              style: GoogleFonts.openSans(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: secondaryColor),
            ),
            Container(
              margin: EdgeInsets.only(top: 8, bottom: 24),
              padding: EdgeInsets.symmetric(vertical: 14),
              child: TextField(
                controller: street,
                decoration: InputDecoration(
                    hintText: 'Абая',
                    suffixIcon: Icon(
                      Icons.place,
                      color: secondaryColor,
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: secondaryColor,
                        ),
                        borderRadius: BorderRadius.circular(0)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: secondaryColor,
                        ),
                        borderRadius: BorderRadius.circular(0))),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Column(children: [
                    Text(
                      'Комментарии',
                      style: GoogleFonts.openSans(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: secondaryColor),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width - 64,
                      margin: EdgeInsets.only(top: 8),
                      child: TextField(
                        maxLines: 4,
                        controller: flatNumber,
                        decoration: InputDecoration(
                            hintText: '',
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: secondaryColor,
                                ),
                                borderRadius: BorderRadius.circular(0)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: secondaryColor,
                                ),
                                borderRadius: BorderRadius.circular(0))),
                      ),
                    ),
                  ], crossAxisAlignment: CrossAxisAlignment.start),
                ),
              ],
            )
          ],
        ),
      )),
      bottomNavigationBar: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(vertical: 25, horizontal: 10),
        child: ButtonTheme(
          child: RaisedButton(
            padding: EdgeInsets.symmetric(
              vertical: 14,
            ),
            color: secondaryColor,
            onPressed: () => BlocProvider.of<AddressCubit>(context)
                .storeAddresses(context: context, form: {
              "street": street.text,
              "flat_number": flatNumber.text.toString(),
              "city_id": cityId
            }),
            child: Text(
              'Сохранить адрес',
              style: GoogleFonts.openSans(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
