import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/address/cubit/address_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/presentation/items/addresses_item.dart';

class AddressesIndex extends StatelessWidget {
  const AddressesIndex({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<AddressCubit>(context).getAddresses();
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Мои адреса',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      ),
      body: BlocBuilder<AddressCubit, AddressState>(
        builder: (context, state) {
          if (!(state is AddressesFetched))
            return Center(child: CircularProgressIndicator());
          final addresses = (state as AddressesFetched).addresses;
          return RefreshIndicator(
            onRefresh: () async {
              BlocProvider.of<AddressCubit>(context).getAddresses();
            },
            child: SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              child: Container(
                  margin:
                      EdgeInsets.only(top: 32, left: 32, right: 32, bottom: 32),
                  child: ListView.builder(
                    itemBuilder: (context, index) => AddressItem(
                      address: addresses[index],
                    ),
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: addresses.length,
                  )),
            ),
          );
        },
      ),
      bottomNavigationBar: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(vertical: 25, horizontal: 10),
        child: ButtonTheme(
          child: RaisedButton(
            padding: EdgeInsets.symmetric(
              vertical: 14,
            ),
            color: secondaryColor,
            onPressed: () => Navigator.pushNamed(context, '/addresses/create')
                .then((value) =>
                    BlocProvider.of<AddressCubit>(context).getAddresses()),
            child: Text(
              'Добавить адрес',
              style: GoogleFonts.openSans(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
