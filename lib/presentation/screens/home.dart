import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/ads/ads_cubit.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/cubit/category/category_cubit.dart';
import 'package:kostyum/cubit/flag/cubit/flag_cubit.dart';
import 'package:kostyum/cubit/product/product_cubit.dart';
import 'package:kostyum/cubit/product_search/cubit/product_search_cubit.dart';
import 'package:kostyum/cubit/sale_point/cubit/sale_point_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/presentation/components/ads_slider.dart';
import 'package:kostyum/presentation/components/category_slider.dart';
import 'package:kostyum/presentation/items/category_slider.dart';
import 'package:kostyum/presentation/items/product.dart';
import 'package:kostyum/presentation/items/types_slider.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<CategoryCubit>(context).fetchCategories();
    BlocProvider.of<AdsCubit>(context).getAds();
    BlocProvider.of<FlagCubit>(context).getFlags();
    BlocProvider.of<ProductCubit>(context).getProdutsWithFilter(
        form: {"sort_type": "price", "sort_value": "ASC", "flags[]": '1'});
    BlocProvider.of<SalePointCubit>(context).getSalePoints();
    return Scaffold(
      backgroundColor: Color(0xffffffff),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 50,
            ),
            Center(
                child: Image.asset(
              'assets/images/logo.png',
              width: MediaQuery.of(context).size.width / 2,
            )),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 12, vertical: 20),
              child: TextField(
                onChanged: (value) {
                  if (value != '')
                    BlocProvider.of<ProductSearchCubit>(context)
                        .getSearchProducts(form: {
                      'search': value,
                      'sort_type': 'name',
                      'sort_value': 'asc'
                    });
                  else
                    BlocProvider.of<ProductSearchCubit>(context).cancelSearch();
                },
                decoration: InputDecoration(
                  prefixIcon: Icon(
                    Icons.search,
                    color: primaryColor,
                  ),
                  hintText: 'Поиск товара',
                  hintStyle: GoogleFonts.openSans(
                      fontSize: 14, fontWeight: FontWeight.w400),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25),
                    borderSide: BorderSide(color: primaryColor, width: 1),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25),
                    borderSide: BorderSide(color: primaryColor, width: 1),
                  ),
                ),
              ),
            ),
            BlocBuilder<ProductSearchCubit, ProductSearchState>(
              builder: (context, state) {
                if (state is ProductFounded) {
                  final products = (state).products;
                  return Container(
                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                      height: 300,
                      decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black,
                          ),
                          borderRadius: BorderRadius.circular(20)),
                      child: ListView.builder(
                          itemBuilder: (context, index) => GestureDetector(
                                onTap: () {
                                  BlocProvider.of<ProductSearchCubit>(context)
                                      .cancelSearch();
                                  Navigator.pushNamed(context, '/product',
                                      arguments: products[index]);
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                      border: Border(
                                          bottom:
                                              BorderSide(color: Colors.black))),
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10, horizontal: 20),
                                  child: Text('${products[index].name}'),
                                ),
                              ),
                          itemCount: products.length));
                }
                return SizedBox.shrink();
              },
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 12),
              padding: EdgeInsets.only(left: 12, top: 24, bottom: 24),
              decoration: BoxDecoration(
                  border: Border.all(color: primaryColor, width: 2),
                  borderRadius: BorderRadius.circular(25)),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Категории',
                        style: GoogleFonts.roboto(
                            fontWeight: FontWeight.w700,
                            color: secondDarkColor,
                            fontSize: 16),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  BlocBuilder<CategoryCubit, CategoryState>(
                    builder: (context, state) {
                      if (!(state is CategoriesFetched))
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      final categories =
                          (state as CategoriesFetched).categories;
                      return Container(
                        width: MediaQuery.of(context).size.width - 24,
                        height: 200,
                        child: GridView.count(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          crossAxisCount: 2,
                          children: List.generate(
                              categories.length,
                              (index) => GestureDetector(
                                    onTap: () => Navigator.pushNamed(
                                        context, '/products',
                                        arguments: categories[index]),
                                    child: CategorySlider(
                                      category: categories[index],
                                    ),
                                  )),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            BlocBuilder<AdsCubit, AdsState>(
              builder: (context, state) {
                if (!(state is AdsFetched))
                  return Center(child: CircularProgressIndicator());
                final ads = (state as AdsFetched).ads;
                return AdsSlider(
                  adsMain: false,
                  ads: ads,
                );
              },
            ),
            BlocBuilder<FlagCubit, FlagState>(
              builder: (context, state) {
                if (state is FlagLoaded) {
                  final flags = (state).flags;
                  return CategoryTypeSlider(flags: flags);
                }
                return SizedBox.shrink();
              },
            ),
            BlocBuilder<ProductCubit, ProductState>(
              builder: (context, state) {
                if (!(state is ProductFetched))
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                final List<Product> products =
                    (state as ProductFetched).products;
                return Container(
                  width: MediaQuery.of(context).size.width - 24,
                  margin: EdgeInsets.symmetric(horizontal: 12, vertical: 0),
                  child: GridView.count(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    crossAxisCount: 2,
                    childAspectRatio: 1.10 / 2,
                    children: List.generate(
                        products.length,
                        (index) => ProductItem(
                              product: products[index],
                              favoritePage: false,
                              action: () {
                                BlocProvider.of<CartCubit>(context).addProduct(
                                    quantity: 1,
                                    product: products[index],
                                    context: context);
                                BlocProvider.of<CartCubit>(context).getCart();
                              },
                            )),
                  ),
                );
              },
            ),
            BlocBuilder<SalePointCubit, SalePointState>(
              builder: (context, state) {
                if (!(state is SalePointsFetched))
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                final salePoints = (state as SalePointsFetched).salePoints;
                return AdsSlider(
                  adsMain: true,
                  salePoints: salePoints,
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
