import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/filter/cubit/filter_cubit_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/filter.dart';
import 'package:kostyum/presentation/items/filter_check.dart';
import 'package:kostyum/presentation/items/filter_radio.dart';

class FilterShow extends StatefulWidget {
  final bool isMultiple;
  final Filter filter;

  const FilterShow({Key key, this.isMultiple, this.filter}) : super(key: key);

  @override
  _FilterShowState createState() => _FilterShowState();
}

class _FilterShowState extends State<FilterShow> {
  String mainValue;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Фильтр',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              BlocProvider.of<FilterCubitCubit>(context).clearFilters();
              Navigator.pop(context);
            },
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 12),
              child: Text(
                'Сбросить',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: secondaryColor),
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 16),
          child: ListView.builder(
            itemBuilder: (context, index) => widget.isMultiple
                ? FilterCheck(
                    name: '${widget.filter.values[index]}',
                  )
                : RadioFilter(
                    name: '${widget.filter.values[index]}',
                    value: widget.filter.values[index] == mainValue,
                    action: (value) {
                      print(mainValue);
                      if (mainValue != widget.filter.values[index]) {
                        setState(() => mainValue = widget.filter.values[index]);
                      } else {
                        setState(() => mainValue = null);
                      }
                    },
                  ),
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: widget.filter.values.length,
          ),
        ),
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: []),
        padding: EdgeInsets.symmetric(vertical: 25, horizontal: 10),
        child: ButtonTheme(
          splashColor: Colors.white,
          child: RaisedButton(
            padding: EdgeInsets.symmetric(
              vertical: 14,
            ),
            color: secondaryColor,
            onPressed: () {
              BlocProvider.of<FilterCubitCubit>(context).saveFilters(filter: {
                'filters[][value]': mainValue,
                'filters[][filter]': widget.filter.id.toString()
              });
              Navigator.pop(context);
              Navigator.pop(context);
            },
            child: Text(
              'Применить',
              style: GoogleFonts.openSans(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
