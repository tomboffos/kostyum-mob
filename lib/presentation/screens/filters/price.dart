import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/filter/cubit/filter_cubit_cubit.dart';
import 'package:kostyum/data/constants.dart';

class FilterPrice extends StatefulWidget {
  const FilterPrice({Key key}) : super(key: key);

  @override
  _FilterPriceState createState() => _FilterPriceState();
}

class _FilterPriceState extends State<FilterPrice> {
  TextEditingController startPrice = new TextEditingController();
  TextEditingController endPrice = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Цена',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
        actions: [
          GestureDetector(
            onTap: () {
              BlocProvider.of<FilterCubitCubit>(context).clearFilters();
              Navigator.pop(context);
            },
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(right: 12),
              child: Text(
                'Сбросить',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: secondaryColor),
              ),
            ),
          )
        ],
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 40, horizontal: 15),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 100,
                      width: (MediaQuery.of(context).size.width - 40) / 2,
                      child: TextField(
                        keyboardType: TextInputType.number,
                        controller: startPrice,
                        decoration: InputDecoration(
                            prefixText: 'От  ',
                            prefixStyle: TextStyle(color: Colors.black),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0),
                                borderSide: BorderSide(color: secondaryColor)),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0),
                                borderSide: BorderSide(color: secondaryColor))),
                      ),
                    ),
                    Container(
                      height: 100,
                      width: (MediaQuery.of(context).size.width - 40) / 2,
                      child: TextField(
                        keyboardType: TextInputType.number,
                        controller: endPrice,
                        decoration: InputDecoration(
                            prefixText: 'До  ',
                            prefixStyle: TextStyle(color: Colors.black),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0),
                                borderSide: BorderSide(color: secondaryColor)),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0),
                                borderSide: BorderSide(color: secondaryColor))),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: []),
        padding: EdgeInsets.symmetric(vertical: 25, horizontal: 10),
        child: ButtonTheme(
          splashColor: Colors.white,
          child: RaisedButton(
            padding: EdgeInsets.symmetric(
              vertical: 14,
            ),
            color: secondaryColor,
            onPressed: () {
              BlocProvider.of<FilterCubitCubit>(context).saveFilters(filter: {
                'prices[0]': startPrice.text.toString(),
                'prices[1]': endPrice.text.toString()
              });
              Navigator.pop(context);
              Navigator.pop(context);
            },
            child: Text(
              'Применить',
              style: GoogleFonts.openSans(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
