import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/filter/cubit/filter_cubit_cubit.dart';
import 'package:kostyum/data/models/category.dart';
import 'package:kostyum/presentation/items/category_catalog.dart';

class FilterIndex extends StatelessWidget {
  final Category category;
  const FilterIndex({Key key, this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<FilterCubitCubit>(context)
        .getFiltersByCategory(category: category);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Фильтр',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
            margin: EdgeInsets.only(top: 16),
            child: Column(
              children: [
                CategoryCatalog(
                  name: 'Цена',
                  onTap: () => Navigator.pushNamed(context, '/filter/price'),
                ),
                BlocBuilder<FilterCubitCubit, FilterCubitState>(
                  builder: (context, state) {
                    if (!(state is FilterFetched))
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    final filters = (state as FilterFetched).filters;
                    return ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) => CategoryCatalog(
                        name: filters[index].name,
                        onTap: () => Navigator.pushNamed(
                            context, '/filter/show',
                            arguments: filters[index]),
                      ),
                      itemCount: filters.length,
                    );
                  },
                )
              ],
            )),
      ),
    );
  }
}
