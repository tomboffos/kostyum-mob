import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/presentation/items/card.dart';

class CardIndex extends StatelessWidget {
  const CardIndex({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        title: Text(
          'Мои карты',
          style: GoogleFonts.openSans(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Color(0xff191919)),
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: ListView.builder(itemBuilder: (context,index)=>CardItem(),itemCount: 20,),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(left: 10, right: 10, bottom: 25),
        child: ButtonTheme(
            child: RaisedButton(
              padding: EdgeInsets.symmetric(vertical: 20),
              color: secondaryColor,
              child: Text(
                'Сохранить данные',
                style: GoogleFonts.openSans(
                    fontSize: 16, fontWeight: FontWeight.w600, color: Colors.white),
              ),
              onPressed: () => {},
            )),
      ),


    );
  }
}
