import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/presentation/items/cart_item.dart';
import 'package:intl/intl.dart';

class CartIndex extends StatelessWidget {
  const CartIndex({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<CartCubit>(context).getCart();
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Корзина',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      ),
      body: BlocBuilder<CartCubit, CartState>(
        builder: (context, state) {
          if (!(state is CartProcessed))
            return Center(
              child: CircularProgressIndicator(),
            );
          final cart = (state as CartProcessed).cart;
          return RefreshIndicator(
            onRefresh: () async {
              BlocProvider.of<CartCubit>(context).getCart();
              return null;
            },
            child: ListView.builder(
              itemBuilder: (context, index) => CartItem(
                product: cart[index],
              ),
              itemCount: cart.length,
            ),
          );
        },
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(0.5),
              offset: Offset(0, 1),
              spreadRadius: 1,
              blurRadius: 5)
        ]),
        padding: EdgeInsets.symmetric(vertical: 25, horizontal: 10),
        child: BlocBuilder<CartCubit, CartState>(
          builder: (context, state) {
            if (!(state is CartProcessed)) return SizedBox.shrink();
            final cart = (state as CartProcessed).cart;
            int price = 0;
            int discount = 0;
            cart.forEach((element) {
              price += element.product.price * element.quantity;
            });
            return ButtonTheme(
              splashColor: Colors.white,
              child: RaisedButton(
                padding: EdgeInsets.symmetric(
                  vertical: 14,
                ),
                color: secondaryColor,
                onPressed: () => cart.length == 0
                    ? {}
                    : Navigator.pushNamed(
                        context,
                        '/order/create',
                      ),
                child: Text(
                  cart.length != 0
                      ? 'Оформить заказ на ${NumberFormat('###,000', 'zz').format(price)} KZT'
                      : 'Корзина сейчас пуста',
                  style: GoogleFonts.openSans(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: Colors.white),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
