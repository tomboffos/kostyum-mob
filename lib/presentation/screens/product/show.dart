import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/cubit/favorite/cubit/favorite_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/data/models/size.dart';
import 'package:kostyum/presentation/components/stars.dart';
import 'package:kostyum/presentation/items/review.dart';
import 'package:intl/intl.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class ProductShow extends StatefulWidget {
  final Product product;
  const ProductShow({Key key, this.product}) : super(key: key);

  @override
  _ProductShowState createState() => _ProductShowState();
}

class _ProductShowState extends State<ProductShow> {
  bool showProduct = false;
  int quantity = 1;
  PanelController panelController = new PanelController();
  closeSlide() async {
    setState(() {
      showProduct = false;
    });
  }

  ScrollController _scrollController = new ScrollController();

  callback() {
    setState(() {
      showProduct = true;
    });
    panelController.open();
  }

  closeSlideIcon() async {
    await closeSlide();
    panelController.close();
  }

  void goToVideo() {
    final Duration duration = Duration(milliseconds: 400);

    final Curve curve = Curves.ease;

    var scrollPosition = this._scrollController.position;

    scrollPosition.animateTo(
      _scrollController.position.maxScrollExtent,
      duration: duration,
      curve: curve,
    );
  }

  Size size;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        actions: [
          GestureDetector(
            onTap: () => BlocProvider.of<FavoriteCubit>(context)
                .interactFavorites(
                    productId: widget.product.id, context: context),
            child: Container(
                margin: EdgeInsets.only(right: 20),
                child: Icon(
                  widget.product.favorites
                      ? Icons.favorite
                      : Icons.favorite_border,
                  color: Colors.red,
                  size: 30,
                )),
          ),
        ],
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: SlidingUpPanel(
          minHeight: MediaQuery.of(context).size.height,
          maxHeight: MediaQuery.of(context).size.height / 1.2,
          onPanelClosed: closeSlide,
          controller: panelController,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(45), topRight: Radius.circular(45)),
          panel: Visibility(
            visible: showProduct,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 40, horizontal: 16),
              child: widget.product != null
                  ? Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Ваш товар',
                              style: GoogleFonts.openSans(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w700,
                                  color: Color(0xff141414)),
                            ),
                            GestureDetector(
                              onTap: () => closeSlideIcon(),
                              child: Icon(
                                Icons.close,
                                color: Color(0xff626262),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 82,
                              height: 82,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: NetworkImage(
                                          '${widget.product.images.length != 0 ? widget.product.images[0] : ''}'),
                                      fit: BoxFit.fitHeight)),
                            ),
                            SizedBox(
                              width: 14,
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 1.5,
                              child: Text(
                                '${widget.product.name} ${size != null ? ', Размер: ${size.name}' : ''}',
                                style: GoogleFonts.openSans(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xff414141)),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 36,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              children: [
                                ButtonTheme(
                                  minWidth: 28,
                                  child: RaisedButton(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 6, vertical: 2),
                                    color: secondaryColor,
                                    onPressed: () => setState(() => quantity++),
                                    child: Icon(
                                      Icons.add,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                Text(
                                  '$quantity',
                                  style: GoogleFonts.openSans(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xff191919)),
                                ),
                                SizedBox(
                                  width: 8,
                                ),
                                ButtonTheme(
                                    minWidth: 28,
                                    child: RaisedButton(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 6, vertical: 2),
                                      color: secondaryColor,
                                      onPressed: () => setState(() =>
                                          quantity > 1 ? quantity-- : null),
                                      child: Icon(
                                        Icons.remove,
                                        color: Colors.white,
                                      ),
                                    )),
                              ],
                            ),
                            Text(
                              '${NumberFormat('###,000', 'zz').format(widget.product.price * quantity)} KZT',
                              style: GoogleFonts.openSans(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xff14CE04)),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ButtonTheme(
                                minWidth:
                                    (MediaQuery.of(context).size.width - 60) /
                                        2,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(color: Color(0xff545454))),
                                child: FlatButton(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 14, horizontal: 10),
                                  onPressed: () {
                                    BlocProvider.of<CartCubit>(context)
                                        .addProduct(
                                            quantity: quantity,
                                            product: widget.product,
                                            context: context,
                                            size: size);
                                    BlocProvider.of<CartCubit>(context)
                                        .getCart();
                                    closeSlideIcon();
                                  },
                                  child: Text(
                                    'Добавить в корзину',
                                    style: GoogleFonts.openSans(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xff121212)),
                                  ),
                                )),
                            ButtonTheme(
                                minWidth:
                                    (MediaQuery.of(context).size.width - 60) /
                                        2,
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(color: Color(0xff545454))),
                                child: FlatButton(
                                  color: secondaryColor,
                                  padding: EdgeInsets.symmetric(
                                      vertical: 14, horizontal: 10),
                                  onPressed: () {
                                    BlocProvider.of<CartCubit>(context)
                                        .addProduct(
                                            quantity: quantity,
                                            product: widget.product,
                                            context: context,
                                            size: size);
                                    BlocProvider.of<CartCubit>(context)
                                        .getCart();
                                    closeSlideIcon();
                                    Navigator.pushNamed(
                                        context, '/order/create');
                                  },
                                  child: Text('Оформить сразу',
                                      style: GoogleFonts.openSans(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.white)),
                                )),
                          ],
                        )
                      ],
                    )
                  : SizedBox.shrink(),
            ),
          ),
          backdropEnabled: true,
          renderPanelSheet: showProduct,
          body: SingleChildScrollView(
            child: Container(
              constraints: BoxConstraints(minHeight: 1300),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.symmetric(horizontal: 18),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        widget.product.bonus != 0
                            ? Container(
                                alignment: Alignment.center,
                                width: 50,
                                height: 30,
                                padding: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 6),
                                color: Color(0xff63B400),
                                child: Text(
                                  '${widget.product.bonus} Б',
                                  style: GoogleFonts.openSans(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xffffffff)),
                                ),
                              )
                            : SizedBox.shrink(),
                        widget.product.video == null
                            ? SizedBox.shrink()
                            : GestureDetector(
                                onTap: () => goToVideo(),
                                child: FaIcon(
                                  FontAwesomeIcons.youtube,
                                  color: Colors.red,
                                  size: 30,
                                ),
                              ),
                      ],
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 272,
                    color: Colors.white,
                    child: ListView.builder(
                      controller: _scrollController,
                      itemBuilder: (context, index) => widget.product.video ==
                              null
                          ? Container(
                              width: MediaQuery.of(context).size.width,
                              height: 272 - 272 * 0.1,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      fit: BoxFit.fitHeight,
                                      image: NetworkImage(
                                          widget.product.images[index]))),
                            )
                          : widget.product.images.length - 1 >= index
                              ? Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 272 - 272 * 0.1,
                                  child: Stack(
                                    children: [],
                                  ),
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          fit: BoxFit.fitHeight,
                                          image: NetworkImage(
                                              widget.product.images[index]))),
                                )
                              : YoutubePlayer(
                                  controller: YoutubePlayerController(
                                    initialVideoId: widget.product.video
                                        .replaceAll('https://youtu.be/', '')
                                        .replaceAll('http://youtu.be/', ''),
                                    flags: YoutubePlayerFlags(
                                      autoPlay: true,
                                      mute: false,
                                      hideControls: true,
                                    ),
                                  ),
                                  showVideoProgressIndicator: true,
                                ),
                      itemCount: widget.product.video == null
                          ? widget.product.images.length
                          : widget.product.images.length + 1,
                      scrollDirection: Axis.horizontal,
                    ),
                  ),

                  Container(
                    padding: EdgeInsets.symmetric(vertical: 14, horizontal: 12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.product.name,
                          style: GoogleFonts.montserrat(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: secondDarkColor),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        Row(
                          children: [
                            Stars(
                              rating: widget.product.rating,
                            ),
                            SizedBox(
                              width: 14,
                            ),
                            Text(
                              '${widget.product.reviews.length} отзывов',
                              style: GoogleFonts.montserrat(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.black),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        Row(
                          children: [
                            Text(
                              '${NumberFormat('###,000', 'zz').format(widget.product.price)} KZT',
                              style: GoogleFonts.montserrat(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: secondDarkColor),
                            ),
                            SizedBox(width: 20),
                            widget.product.oldPrice != null
                                ? Text(
                                    '${NumberFormat('###,000', 'zz').format(widget.product.oldPrice)} KZT',
                                    style: GoogleFonts.montserrat(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                        color: secondDarkColor,
                                        decoration: TextDecoration.lineThrough),
                                  )
                                : SizedBox.shrink()
                          ],
                        ),
                        SizedBox(
                          height: 21,
                        ),
                        widget.product.sizes.length == 0
                            ? SizedBox.shrink()
                            : Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(bottom: 20),
                                    child: Text('Выберите размер',
                                        style: GoogleFonts.openSans(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,
                                        )),
                                  ),
                                  Container(
                                    height: 50,
                                    margin: EdgeInsets.only(bottom: 20),
                                    width:
                                        MediaQuery.of(context).size.width - 10,
                                    child: ListView.builder(
                                        physics:
                                            AlwaysScrollableScrollPhysics(),
                                        scrollDirection: Axis.horizontal,
                                        itemBuilder: (context, newIndex) =>
                                            Container(
                                              margin:
                                                  EdgeInsets.only(right: 10),
                                              child: ButtonTheme(
                                                minWidth: 55,
                                                child: FlatButton(
                                                  color: size != null &&
                                                          widget
                                                                  .product
                                                                  .sizes[
                                                                      newIndex]
                                                                  .id ==
                                                              size.id
                                                      ? Colors.black
                                                      : Colors.transparent,
                                                  shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5),
                                                    side: BorderSide(
                                                        color: Colors.black),
                                                  ),
                                                  onPressed: () => setState(
                                                      () => size = widget
                                                          .product
                                                          .sizes[newIndex]),
                                                  child: Text(
                                                    '${widget.product.sizes[newIndex].name}',
                                                    style: GoogleFonts.openSans(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: size != null &&
                                                              widget
                                                                      .product
                                                                      .sizes[
                                                                          newIndex]
                                                                      .id ==
                                                                  size.id
                                                          ? Colors.white
                                                          : Colors.black,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                        itemCount: widget.product.sizes.length),
                                  ),
                                ],
                              ),
                        Center(
                          child: ButtonTheme(
                              minWidth: MediaQuery.of(context).size.width - 42,
                              child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                                padding: EdgeInsets.symmetric(vertical: 15),
                                onPressed: () => {},
                                color: secondaryColor,
                                child: Text(
                                  'Характеристики',
                                  style: GoogleFonts.openSans(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.white),
                                ),
                              )),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 20),
                          color: Colors.white,
                          child: Column(
                            children: [
                              widget.product.characteristics != null
                                  ? ListView.builder(
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Container(
                                          margin:
                                              EdgeInsets.symmetric(vertical: 2),
                                          child: Text(
                                            '${widget.product.characteristics[index]['filter']['name']} : ${widget.product.characteristics[index]['value']}',
                                            style: GoogleFonts.openSans(
                                                fontSize: 13,
                                                fontWeight: FontWeight.w400,
                                                color: Color(0xff545454)),
                                          ),
                                        );
                                      },
                                      itemCount:
                                          widget.product.characteristics.length,
                                    )
                                  : SizedBox.shrink(),
                            ],
                          ),
                        ),
                        ButtonTheme(
                          minWidth: MediaQuery.of(context).size.width - 20,
                          child: FlatButton(
                            splashColor: Colors.black,
                            onPressed: () => Navigator.pushNamed(
                                context, '/reviews',
                                arguments: widget.product),
                            shape: RoundedRectangleBorder(
                                side:
                                    BorderSide(color: Colors.black, width: 1)),
                            color: Colors.transparent.withOpacity(0),
                            padding: EdgeInsets.symmetric(vertical: 20),
                            child: Text(
                              'Написать отзыв',
                              style: GoogleFonts.openSans(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                  color: secondDarkColor),
                            ),
                          ),
                        ),
                        Center(
                          child: Container(
                            margin: EdgeInsets.symmetric(vertical: 20),
                            width: MediaQuery.of(context).size.width / 2,
                            child: Center(
                              child: Stars(rating: widget.product.rating),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  ListView.builder(
                    itemBuilder: (context, index) => ReviewItem(
                      review: widget.product.reviews[index],
                    ),
                    itemCount: widget.product.reviews.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                  ),
                  SizedBox(
                    height: 300,
                  )
                  // Container(
                  //   margin: EdgeInsets.symmetric(horizontal: 10),
                  //   child: ButtonTheme(
                  //     minWidth: MediaQuery.of(context).size.width - 20,
                  //     child: FlatButton(
                  //       padding: EdgeInsets.symmetric(vertical: 14, horizontal: 20),
                  //       shape: RoundedRectangleBorder(
                  //           side: BorderSide(color: secondaryColor)),
                  //       onPressed: () => {},
                  //       child: Row(
                  //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //         children: [
                  //           Text(
                  //             'Еще 40 отзывов',
                  //             style: GoogleFonts.openSans(
                  //                 fontSize: 14,
                  //                 fontWeight: FontWeight.w400,
                  //                 color: secondaryColor),
                  //           ),
                  //           Icon(
                  //             Icons.arrow_forward,
                  //             color: secondaryColor,
                  //           )
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  // )
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(vertical: 25, horizontal: 10),
        child: ButtonTheme(
          child: RaisedButton(
            padding: EdgeInsets.symmetric(
              vertical: 14,
            ),
            color: secondaryColor,
            onPressed: () => widget.product.stock ? callback() : null,
            child: Text(
              widget.product.stock
                  ? 'В корзину ${NumberFormat('###,000', 'zz').format(widget.product.price)} KZT'
                  : 'Товар сейчас не доступен',
              style: GoogleFonts.openSans(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
