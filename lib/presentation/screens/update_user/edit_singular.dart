import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/data/constants.dart';

class EditUserParameter extends StatefulWidget {
  final String name;
  final bool mask;
  final String placeholder;
  final String label;
  const EditUserParameter({
    Key key,
    this.name,
    this.mask,
    this.placeholder,
    this.label,
  }) : super(key: key);

  @override
  _EditUserParameterState createState() => _EditUserParameterState();
}

class _EditUserParameterState extends State<EditUserParameter> {
  TextEditingController name = new TextEditingController();
  @override
  void initState() {
    super.initState();
    if (widget.mask) name = MaskedTextController(mask: '+0 (000) 000-00-00');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: false,
        title: Text(
          'Личные данные',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: primaryColor),
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16, vertical: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.label,
                style: GoogleFonts.openSans(
                    fontSize: 14, fontWeight: FontWeight.w700),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                child: TextField(
                  controller: name,
                  obscureText: widget.name == 'password',
                  decoration: InputDecoration(
                      hintText: widget.placeholder,
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0),
                          borderSide: BorderSide(color: secondaryColor)),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(0),
                          borderSide: BorderSide(color: secondaryColor))),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              ButtonTheme(
                  minWidth: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    color: secondaryColor,
                    child: Text(
                      'Сохранить данные',
                      style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                    onPressed: () => BlocProvider.of<UserCubit>(context).update(
                        form: {
                          '${widget.name}': widget.name == 'phone'
                              ? name.text
                                  .replaceAll('+7', '8')
                                  .replaceAll(' ', '')
                                  .replaceAll(')', '')
                                  .replaceAll('(', '')
                                  .replaceAll('-', '')
                              : name.text,
                        },
                        context:
                            context).then((value) => {Navigator.pop(context)}),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
