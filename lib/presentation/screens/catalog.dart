import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/category/category_cubit.dart';
import 'package:kostyum/cubit/filter/cubit/filter_cubit_cubit.dart';
import 'package:kostyum/cubit/parent_category/parent_category_cubit.dart';
import 'package:kostyum/presentation/items/category_catalog.dart';

class Catalog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<CategoryCubit>(context).fetchCategories();
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        leading: BlocBuilder<ParentCategoryCubit, ParentCategoryState>(
          builder: (context, state) {
            if (state is ParentCategoryChoose)
              return GestureDetector(
                onTap: () {
                  BlocProvider.of<CategoryCubit>(context).fetchCategories();
                  BlocProvider.of<ParentCategoryCubit>(context).clearCategory();
                },
                child: Center(
                  child: Icon(
                    Icons.arrow_back,
                    color: Colors.black,
                  ),
                ),
              );
            return SizedBox.shrink();
          },
        ),
        title: Text(
          'Каталог',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
        elevation: 2,
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          BlocProvider.of<CategoryCubit>(context).fetchCategories();
          return true;
        },
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Container(
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height,
            ),
            margin: EdgeInsets.only(top: 20),
            child: BlocBuilder<CategoryCubit, CategoryState>(
              builder: (context, state) {
                if (!(state is CategoriesFetched))
                  return Container(
                    margin: EdgeInsets.symmetric(vertical: 50),
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                final categories = (state as CategoriesFetched).categories;
                return ListView.builder(
                  itemBuilder: (context, index) => CategoryCatalog(
                      name: '${categories[index].name}',
                      onTap: categories[index].childCount == 0
                          ? () {
                              BlocProvider.of<FilterCubitCubit>(context)
                                  .clearFilters();
                              Navigator.pushNamed(context, '/products',
                                  arguments: categories[index]);
                            }
                          : () {
                              BlocProvider.of<CategoryCubit>(context)
                                  .fetchCategories(
                                      categoryId: categories[index].id);
                              BlocProvider.of<ParentCategoryCubit>(context)
                                  .chooseCategory(
                                      parentCategoryId: categories[index].id);
                            }),
                  itemCount: categories.length,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
