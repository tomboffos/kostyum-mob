import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/cubit/favorite/cubit/favorite_cubit.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/data/models/size.dart';
import 'package:kostyum/presentation/components/login.dart';
import 'package:kostyum/presentation/components/register.dart';
import 'package:kostyum/presentation/items/product.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class Favorite extends StatefulWidget {
  @override
  _FavoriteState createState() => _FavoriteState();
}

class _FavoriteState extends State<Favorite> {
  PanelController panelController = new PanelController();
  bool showProduct = false;
  Product product;
  int quantity = 1;

  callback({Product productMain}) {
    setState(() {
      showProduct = true;
      product = productMain;
    });
    panelController.open();
  }

  closeSlide() async {
    setState(() {
      showProduct = false;
    });
  }

  closeSlideIcon() async {
    await closeSlide();
    panelController.close();
  }

  Size size;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<UserCubit>(context).checkIfAuthorized();
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        title: Text(
          'Избранное',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      ),
      body: SlidingUpPanel(
        backdropEnabled: true,
        renderPanelSheet: showProduct,
        onPanelClosed: closeSlide,
        controller: panelController,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(45), topRight: Radius.circular(45)),
        panel: Visibility(
          visible: showProduct,
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 40, horizontal: 16),
            child: product != null
                ? Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Ваш товар',
                            style: GoogleFonts.openSans(
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                color: Color(0xff141414)),
                          ),
                          GestureDetector(
                            onTap: () => closeSlideIcon(),
                            child: Icon(
                              Icons.close,
                              color: Color(0xff626262),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 82,
                            height: 82,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: NetworkImage('${product.images[0]}'),
                                    fit: BoxFit.cover)),
                          ),
                          SizedBox(
                            width: 14,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 1.5,
                            child: Text(
                              '${product.name} ${size != null ? ', Размер: ${size.name}' : ''} ',
                              style: GoogleFonts.openSans(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff414141)),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 36,
                      ),
                      product.sizes.length == 0
                          ? SizedBox.shrink()
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(bottom: 20),
                                  child: Text('Выберите размер',
                                      style: GoogleFonts.openSans(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                      )),
                                ),
                                Container(
                                  height: 50,
                                  margin: EdgeInsets.only(bottom: 20),
                                  width: MediaQuery.of(context).size.width - 10,
                                  child: ListView.builder(
                                      physics: AlwaysScrollableScrollPhysics(),
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context, newIndex) =>
                                          Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: ButtonTheme(
                                              minWidth: 55,
                                              child: FlatButton(
                                                color: size != null &&
                                                        product.sizes[newIndex]
                                                                .id ==
                                                            size.id
                                                    ? Colors.black
                                                    : Colors.transparent,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                  side: BorderSide(
                                                      color: Colors.black),
                                                ),
                                                onPressed: () => setState(() =>
                                                    size = product
                                                        .sizes[newIndex]),
                                                child: Text(
                                                  '${product.sizes[newIndex].name}',
                                                  style: GoogleFonts.openSans(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w400,
                                                    color: size != null &&
                                                            product
                                                                    .sizes[
                                                                        newIndex]
                                                                    .id ==
                                                                size.id
                                                        ? Colors.white
                                                        : Colors.black,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                      itemCount: product.sizes.length),
                                ),
                              ],
                            ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              ButtonTheme(
                                minWidth: 28,
                                child: RaisedButton(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 6, vertical: 2),
                                  color: secondaryColor,
                                  onPressed: () => setState(() => quantity++),
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                '$quantity',
                                style: GoogleFonts.openSans(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff191919)),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              ButtonTheme(
                                  minWidth: 28,
                                  child: RaisedButton(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 6, vertical: 2),
                                    color: secondaryColor,
                                    onPressed: () => setState(
                                        () => quantity > 1 ? quantity-- : null),
                                    child: Icon(
                                      Icons.remove,
                                      color: Colors.white,
                                    ),
                                  )),
                            ],
                          ),
                          Text(
                            '${NumberFormat('###,000', 'zz').format(product.price * quantity)} KZT',
                            style: GoogleFonts.openSans(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: Color(0xff14CE04)),
                          )
                        ],
                      ),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ButtonTheme(
                              minWidth:
                                  (MediaQuery.of(context).size.width - 60) / 2,
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: Color(0xff545454))),
                              child: FlatButton(
                                padding: EdgeInsets.symmetric(
                                    vertical: 14, horizontal: 10),
                                onPressed: () {
                                  BlocProvider.of<CartCubit>(context)
                                      .addProduct(
                                          quantity: quantity,
                                          product: product,
                                          context: context,
                                          size: size);
                                  BlocProvider.of<CartCubit>(context).getCart();
                                  closeSlideIcon();
                                },
                                child: Text(
                                  'Добавить в корзину',
                                  style: GoogleFonts.openSans(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xff121212)),
                                ),
                              )),
                          ButtonTheme(
                              minWidth:
                                  (MediaQuery.of(context).size.width - 60) / 2,
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: Color(0xff545454))),
                              child: FlatButton(
                                color: secondaryColor,
                                padding: EdgeInsets.symmetric(
                                    vertical: 14, horizontal: 10),
                                onPressed: () {
                                  BlocProvider.of<CartCubit>(context)
                                      .addProduct(
                                          quantity: quantity,
                                          product: product,
                                          context: context,
                                          size: size);
                                  BlocProvider.of<CartCubit>(context).getCart();
                                  closeSlideIcon();
                                  Navigator.pushNamed(context, '/order/create');
                                },
                                child: Text('Оформить сразу',
                                    style: GoogleFonts.openSans(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white)),
                              )),
                        ],
                      )
                    ],
                  )
                : SizedBox.shrink(),
          ),
        ),
        body: BlocBuilder<UserCubit, UserState>(
          builder: (context, state) {
            if (state is UserInitial)
              return Container(
                child: Center(child: CircularProgressIndicator()),
              );
            if (state is UserAreNotLogged)
              return Container(
                  margin: EdgeInsets.symmetric(horizontal: 40),
                  child: Center(child: Login()));
            if (state is UserRegisterOpen)
              return Container(
                child: Register(),
                margin: EdgeInsets.symmetric(horizontal: 40),
              );
            BlocProvider.of<FavoriteCubit>(context).getFavorites();

            return Container(
              margin: EdgeInsets.only(top: 20, left: 12, right: 12),
              child: BlocBuilder<FavoriteCubit, FavoriteState>(
                builder: (context, state) {
                  if (!(state is FavoriteLoaded))
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  final products = (state as FavoriteLoaded).products;
                  return RefreshIndicator(
                    onRefresh: () async {
                      BlocProvider.of<FavoriteCubit>(context).getFavorites();
                      return null;
                    },
                    child: products.length == 0
                        ? Center(
                            child: Text('Пока что нет товаров в избранных',
                                style: GoogleFonts.openSans(fontSize: 14)),
                          )
                        : GridView.count(
                            crossAxisCount: 2,
                            childAspectRatio: 1.10 / 2,
                            children: List.generate(
                              products.length,
                              (index) => ProductItem(
                                product: products[index],
                                favoritePage: true,
                                openSlide: callback,
                              ),
                            ),
                          ),
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
