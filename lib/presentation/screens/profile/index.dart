import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/presentation/components/forget.dart';
import 'package:kostyum/presentation/components/login.dart';
import 'package:kostyum/presentation/components/register.dart';
import 'package:kostyum/presentation/components/register_confirmation.dart';
import 'package:kostyum/presentation/components/set_password.dart';
import 'package:kostyum/presentation/items/category_catalog.dart';

class ProfileIndex extends StatefulWidget {
  const ProfileIndex({Key key}) : super(key: key);

  @override
  State<ProfileIndex> createState() => _ProfileIndexState();
}

class _ProfileIndexState extends State<ProfileIndex> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<UserCubit>(context).checkIfAuthorized();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: false,
        title: Text(
          'Профиль',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: primaryColor),
        ),
      ),
      body: BlocBuilder<UserCubit, UserState>(
        builder: (context, state) {
          if (state is UserInitial)
            return Center(
              child: CircularProgressIndicator(),
            );

          if (state is UserAreNotLogged)
            return Container(
                margin: EdgeInsets.symmetric(horizontal: 40), child: Login());
          if (state is UserRegisterOpen)
            return Container(
              child: Register(),
              margin: EdgeInsets.symmetric(horizontal: 40),
            );
          if (state is UserForgetOpen) return ForgetComponent();

          if (state is UserForgetPassword) {
            final user = (state).user;
            return SetPasswordComponent(
              user: user,
            );
          }

          if (state is RegisterConfirmation) {
            final user = (state).user;
            return RegisterConfirmationPage(
              user: user,
            );
          }
          final user = (state as UserLogged).user;
          return SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(top: 5),
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 13),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(15),
                          bottomRight: Radius.circular(15)),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            offset: Offset(0, 1),
                            spreadRadius: 1)
                      ]),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Center(
                        child: Image.asset(
                          'assets/images/logo.png',
                          width: 153,
                          height: 40,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 20, bottom: 2),
                        child: Text(
                          '${user.name}',
                          style: GoogleFonts.openSans(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: secondaryColor),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 21),
                        child: Text(
                          '${user.phone}',
                          style: GoogleFonts.openSans(
                              fontSize: 13,
                              fontWeight: FontWeight.w400,
                              color: secondDarkColor),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  'Сделано заказов',
                                  style: GoogleFonts.openSans(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 24, vertical: 2),
                                  decoration: BoxDecoration(
                                      color: secondaryColor,
                                      borderRadius: BorderRadius.circular(20)),
                                  child: Text(
                                    '${user.orderCount}',
                                    style: GoogleFonts.openSans(
                                        fontSize: 11,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  'Бонусов',
                                  style: GoogleFonts.openSans(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.black),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 24, vertical: 2),
                                  decoration: BoxDecoration(
                                      color: Color(0xff63b400),
                                      borderRadius: BorderRadius.circular(20)),
                                  child: Text(
                                    user.balance.toString(),
                                    style: GoogleFonts.openSans(
                                        fontSize: 11,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                CategoryCatalog(
                  name: 'Личные данные',
                  onTap: () => Navigator.pushNamed(context, '/profile/edit',
                          arguments: user)
                      .then((value) => BlocProvider.of<UserCubit>(context)
                          .checkIfAuthorized()),
                ),
                CategoryCatalog(
                  name: 'История заказов',
                  onTap: () => Navigator.pushNamed(context, '/orders/index')
                      .then((value) => BlocProvider.of<UserCubit>(context)
                          .checkIfAuthorized()),
                ),
                CategoryCatalog(
                  name: 'Мои акции',
                  onTap: () => Navigator.pushNamed(context, '/discounts'),
                ),
                // CategoryCatalog(
                //   name: 'Мои карты',
                //   onTap: () => Navigator.pushNamed(context, '/cards/index'),
                // ),
                CategoryCatalog(
                  name: 'Мои адреса',
                  onTap: () => Navigator.pushNamed(context, '/addresses/index'),
                ),
                user.roleId == 2
                    ? CategoryCatalog(
                        name: 'Заказы курьера',
                        onTap: () =>
                            Navigator.pushNamed(context, '/orders/courier'))
                    : SizedBox.shrink(),
                user.roleId == 3
                    ? CategoryCatalog(
                        name: 'Оформить покупку',
                        onTap: () => Navigator.pushNamed(
                            context, '/order/vendor/create'))
                    : SizedBox.shrink(),
                user.roleId == 3
                    ? CategoryCatalog(
                        name: 'Оформленные покупки',
                        onTap: () =>
                            Navigator.pushNamed(context, '/order/vendor/index'))
                    : SizedBox.shrink(),
                CategoryCatalog(
                  name: 'Выход',
                  onTap: () => BlocProvider.of<UserCubit>(context)
                      .logout(context: context),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
