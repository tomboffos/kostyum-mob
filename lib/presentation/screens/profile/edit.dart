import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/user.dart';
import 'package:kostyum/presentation/items/category_catalog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileEdit extends StatefulWidget {
  final User user;
  const ProfileEdit({Key key, this.user}) : super(key: key);

  @override
  _ProfileEditState createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  final picker = new ImagePicker();
  bool receive;
  Future chooseImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      }
    });
  }

  File _image;

  Future takeImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      }
    });
  }

  bool faceId = false;
  Future faceIDOn(bool value) async {
    (await SharedPreferences.getInstance()).setBool('faceId', value);
  }

  getFaceId() async {
    bool face = (await SharedPreferences.getInstance()).get('faceId');
    setState(() {
      faceId = face;
    });
  }

  showDialogChooseImage({context}) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: Container(
              height: MediaQuery.of(context).size.height * 0.25,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    child: ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width / 2,
                        child: RaisedButton(
                          padding: EdgeInsets.symmetric(vertical: 20),
                          color: secondaryColor,
                          child: Text(
                            'Сфотать',
                            style: GoogleFonts.openSans(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.white),
                          ),
                          onPressed: () => takeImage(),
                        )),
                  ),
                  Container(
                    child: ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width / 2,
                        child: RaisedButton(
                          padding: EdgeInsets.symmetric(vertical: 20),
                          color: secondaryColor,
                          child: Text(
                            'Выбрать фото',
                            style: GoogleFonts.openSans(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.white),
                          ),
                          onPressed: () => chooseImage(),
                        )),
                  ),
                  Container(
                    child: ButtonTheme(
                        minWidth: MediaQuery.of(context).size.width / 2,
                        child: RaisedButton(
                          padding: EdgeInsets.symmetric(vertical: 20),
                          color: Colors.grey,
                          child: Text(
                            'Отменить',
                            style: GoogleFonts.openSans(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color: Colors.white),
                          ),
                          onPressed: () => Navigator.of(context).pop(),
                        )),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      receive = widget.user.notification;
    });
    name.text = widget.user.name;
    phone.text = widget.user.phone.replaceAll('8', '+7');
    getFaceId();
  }

  TextEditingController name = new TextEditingController();
  TextEditingController phone =
      new MaskedTextController(mask: '+0 (000) 000-00-00');
  TextEditingController password = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: false,
        title: Text(
          'Личные данные',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: primaryColor),
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 0),
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16),
                child: GestureDetector(
                  onTap: () => showDialogChooseImage(context: context),
                  child: Center(
                    child: Container(
                      margin: EdgeInsets.only(top: 20, bottom: 48),
                      width: 102,
                      height: 102,
                      padding: EdgeInsets.all(11),
                      decoration: BoxDecoration(
                          border: Border.all(color: secondaryColor),
                          borderRadius: BorderRadius.circular(100)),
                      child: CircleAvatar(
                        foregroundImage: _image != null
                            ? FileImage(_image)
                            : NetworkImage(''),
                        backgroundColor: Colors.grey,
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                  margin: EdgeInsets.only(bottom: 20, left: 16, right: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Отключить touchID/FaceId',
                        style: GoogleFonts.openSans(fontSize: 15),
                      ),
                      CupertinoSwitch(
                          value: faceId,
                          onChanged: (bool value) {
                            setState(() => faceId = !faceId);

                            faceIDOn(value);
                          })
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(bottom: 40, left: 16, right: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Получать уведомления',
                        style: GoogleFonts.openSans(fontSize: 15),
                      ),
                      CupertinoSwitch(
                          value: receive,
                          onChanged: (bool value) {
                            setState(() => receive = !receive);
                            BlocProvider.of<UserCubit>(context).update(form: {
                              'receive_notifications': receive.toString()
                            });
                          })
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(bottom: 20, left: 16, right: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Имя : ${widget.user.name}',
                        style: GoogleFonts.openSans(fontSize: 17),
                      ),
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(bottom: 20, left: 16, right: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Телефон : ${widget.user.phone}',
                        style: GoogleFonts.openSans(fontSize: 17),
                      ),
                    ],
                  )),
              CategoryCatalog(
                name: 'Изменить номер телефона ',
                onTap: () => Navigator.pushNamed(
                    context, '/user/profile/edit/singular', arguments: {
                  'label': 'Введите свой новый номер телефона чтобы сохранить',
                  'placeholder': '+7 (000) 000-00-00',
                  'mask': true,
                  'name': 'phone'
                }).then((value) => (value) =>
                    BlocProvider.of<UserCubit>(context).checkIfAuthorized()),
              ),
              CategoryCatalog(
                name: 'Изменить Имя ',
                onTap: () => Navigator.pushNamed(
                    context, '/user/profile/edit/singular', arguments: {
                  'label': 'Введите свое новое имя чтобы сохранить',
                  'placeholder': 'Александр',
                  'mask': false,
                  'name': 'name'
                }).then((value) => (value) =>
                    BlocProvider.of<UserCubit>(context).checkIfAuthorized()),
              ),
              CategoryCatalog(
                name: 'Изменить пароль',
                onTap: () => Navigator.pushNamed(
                    context, '/user/profile/edit/singular', arguments: {
                  'label': 'Введите свой новый пароль чтобы сохранить',
                  'placeholder': 'Пароль',
                  'mask': false,
                  'name': 'password'
                }).then((value) => (value) =>
                    BlocProvider.of<UserCubit>(context).checkIfAuthorized()),
              ),
            ],
          ),
        ),
      ),
      // bottomNavigationBar: Container(
      //   padding: EdgeInsets.only(left: 10, right: 10, bottom: 25),
      //   child: ButtonTheme(
      //       child: RaisedButton(
      //     padding: EdgeInsets.symmetric(vertical: 20),
      //     color: secondaryColor,
      //     child: Text(
      //       'Сохранить данные',
      //       style: GoogleFonts.openSans(
      //           fontSize: 16, fontWeight: FontWeight.w600, color: Colors.white),
      //     ),
      //     onPressed: () => BlocProvider.of<UserCubit>(context).update(form: {
      //       'name': name.text,
      //       'phone': phone.text
      //           .replaceAll('+7', '8')
      //           .replaceAll(' ', '')
      //           .replaceAll(')', '')
      //           .replaceAll('(', '')
      //           .replaceAll('-', ''),
      //       'password': password.text,
      //       'receive_notifications': receive.toString()
      //     }, context: context),
      //   )),
      // ),
    );
  }
}
