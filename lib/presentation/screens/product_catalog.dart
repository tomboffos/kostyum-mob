import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/cubit/flag/cubit/flag_cubit.dart';
import 'package:kostyum/cubit/product/product_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/category.dart';
import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/data/models/size.dart';
import 'package:kostyum/presentation/components/category_slider.dart';
import 'package:kostyum/presentation/items/product.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:intl/intl.dart';

class ProductCatalog extends StatefulWidget {
  final Category category;

  const ProductCatalog({Key key, this.category}) : super(key: key);
  @override
  _ProductCatalogState createState() => _ProductCatalogState();
}

_ProductCatalogState _productCatalogState = new _ProductCatalogState();

class _ProductCatalogState extends State<ProductCatalog> {
  int page = 1;
  ScrollController scrollController = new ScrollController();
  String sortValue = 'ASC';
  void setupScrollController({context}) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          page++;
          BlocProvider.of<ProductCubit>(context).getProdutsWithFilter(
              categoryId: widget.category.id,
              form: {
                "sort_type": "price",
                "sort_value": sortValue,
                "page": page.toString()
              });
        }
      }
    });
  }

  bool showProduct = false;
  int quantity = 1;
  Product product;
  PanelController panelController = new PanelController();
  callback({Product productMain}) {
    setState(() {
      showProduct = true;
      product = productMain;
    });
    panelController.open();
  }

  closeSlide() async {
    setState(() {
      showProduct = false;
    });
  }

  closeSlideIcon() async {
    await closeSlide();
    panelController.close();
  }

  @override
  void initState() {
    super.initState();
  }

  dialogOrder({context}) {}

  Size size;
  @override
  Widget build(BuildContext context) {
    final productProvider = BlocProvider.of<ProductCubit>(context);
    setupScrollController(context: context);
    BlocProvider.of<ProductCubit>(context).getProdutsWithFilter(
        categoryId: widget.category.id,
        form: {"sort_type": "price", "sort_value": "ASC"});
    BlocProvider.of<FlagCubit>(context).getFlags();

    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        title: Text(
          'Все товары',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 2,
      ),
      backgroundColor: Colors.white,
      body: SlidingUpPanel(
        onPanelClosed: closeSlide,
        controller: panelController,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(45), topRight: Radius.circular(45)),
        panel: Visibility(
          visible: showProduct,
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 40, horizontal: 16),
            child: product != null
                ? Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Ваш товар',
                            style: GoogleFonts.openSans(
                                fontSize: 16,
                                fontWeight: FontWeight.w700,
                                color: Color(0xff141414)),
                          ),
                          GestureDetector(
                            onTap: () => closeSlideIcon(),
                            child: Icon(
                              Icons.close,
                              color: Color(0xff626262),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 82,
                            height: 82,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: NetworkImage(
                                        '${product.images.length != 0 ? product.images[0] : ''}'),
                                    fit: BoxFit.fitHeight)),
                          ),
                          SizedBox(
                            width: 14,
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 1.5,
                            child: Text(
                              '${product.name} ${size != null ? ', Размер: ${size.name}' : ''} ',
                              style: GoogleFonts.openSans(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff414141)),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 36,
                      ),
                      product.sizes.length == 0
                          ? SizedBox.shrink()
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(bottom: 20),
                                  child: Text('Выберите размер',
                                      style: GoogleFonts.openSans(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                      )),
                                ),
                                Container(
                                  height: 50,
                                  margin: EdgeInsets.only(bottom: 20),
                                  width: MediaQuery.of(context).size.width - 10,
                                  child: ListView.builder(
                                      physics: AlwaysScrollableScrollPhysics(),
                                      scrollDirection: Axis.horizontal,
                                      itemBuilder: (context, newIndex) =>
                                          Container(
                                            margin: EdgeInsets.only(right: 10),
                                            child: ButtonTheme(
                                              minWidth: 55,
                                              child: FlatButton(
                                                color: size != null &&
                                                        product.sizes[newIndex]
                                                                .id ==
                                                            size.id
                                                    ? Colors.black
                                                    : Colors.transparent,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                  side: BorderSide(
                                                      color: Colors.black),
                                                ),
                                                onPressed: () => setState(() =>
                                                    size = product
                                                        .sizes[newIndex]),
                                                child: Text(
                                                  '${product.sizes[newIndex].name}',
                                                  style: GoogleFonts.openSans(
                                                    fontSize: 14,
                                                    fontWeight: FontWeight.w400,
                                                    color: size != null &&
                                                            product
                                                                    .sizes[
                                                                        newIndex]
                                                                    .id ==
                                                                size.id
                                                        ? Colors.white
                                                        : Colors.black,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                      itemCount: product.sizes.length),
                                ),
                              ],
                            ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              ButtonTheme(
                                minWidth: 28,
                                child: RaisedButton(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 6, vertical: 2),
                                  color: secondaryColor,
                                  onPressed: () => setState(() => quantity++),
                                  child: Icon(
                                    Icons.add,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Text(
                                '$quantity',
                                style: GoogleFonts.openSans(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff191919)),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              ButtonTheme(
                                  minWidth: 28,
                                  child: RaisedButton(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 6, vertical: 2),
                                    color: secondaryColor,
                                    onPressed: () => setState(
                                        () => quantity > 1 ? quantity-- : null),
                                    child: Icon(
                                      Icons.remove,
                                      color: Colors.white,
                                    ),
                                  )),
                            ],
                          ),
                          Text(
                            '${NumberFormat('###,000', 'zz').format(product.price * quantity)} KZT',
                            style: GoogleFonts.openSans(
                                fontSize: 18,
                                fontWeight: FontWeight.w600,
                                color: Color(0xff14CE04)),
                          )
                        ],
                      ),
                      Spacer(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ButtonTheme(
                              minWidth:
                                  (MediaQuery.of(context).size.width - 60) / 2,
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: Color(0xff545454))),
                              child: FlatButton(
                                padding: EdgeInsets.symmetric(
                                    vertical: 14, horizontal: 10),
                                onPressed: () {
                                  BlocProvider.of<CartCubit>(context)
                                      .addProduct(
                                          quantity: quantity,
                                          product: product,
                                          context: context,
                                          size: size);
                                  BlocProvider.of<CartCubit>(context).getCart();
                                  closeSlideIcon();
                                },
                                child: Text(
                                  'Добавить в корзину',
                                  style: GoogleFonts.openSans(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xff121212)),
                                ),
                              )),
                          ButtonTheme(
                              minWidth:
                                  (MediaQuery.of(context).size.width - 60) / 2,
                              shape: RoundedRectangleBorder(
                                  side: BorderSide(color: Color(0xff545454))),
                              child: FlatButton(
                                color: secondaryColor,
                                padding: EdgeInsets.symmetric(
                                    vertical: 14, horizontal: 10),
                                onPressed: () {
                                  BlocProvider.of<CartCubit>(context)
                                      .addProduct(
                                          quantity: quantity,
                                          product: product,
                                          context: context,
                                          size: size);
                                  BlocProvider.of<CartCubit>(context).getCart();
                                  closeSlideIcon();
                                  Navigator.pushNamed(context, '/order/create');
                                },
                                child: Text('Оформить сразу',
                                    style: GoogleFonts.openSans(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white)),
                              )),
                        ],
                      )
                    ],
                  )
                : SizedBox.shrink(),
          ),
        ),
        backdropEnabled: true,
        renderPanelSheet: showProduct,
        body: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          controller: scrollController,
          child: Container(
            margin: EdgeInsets.only(bottom: 100),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 17),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap: () => Navigator.pushNamed(
                                context, '/filter/index',
                                arguments: widget.category)
                            .then((value) =>
                                BlocProvider.of<ProductCubit>(context)
                                    .getProdutsWithFilter(
                                        categoryId: widget.category.id,
                                        form: {
                                      "sort_type": "price",
                                      "sort_value": "ASC"
                                    })),
                        child: Container(
                          child: Row(
                            children: [
                              Icon(
                                Icons.tune,
                                color: secondaryColor,
                                size: 24,
                              ),
                              SizedBox(
                                width: 11,
                              ),
                              Text(
                                'Фильтры',
                                style: GoogleFonts.openSans(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    color: secondaryColor),
                              )
                            ],
                          ),
                        ),
                      ),
                      VerticalDivider(color: secondaryColor),
                      GestureDetector(
                        onTap: () => showDialog(
                            context: context,
                            builder: (mainContext) => BlocProvider.value(
                                  value: productProvider,
                                  child: AlertDialog(
                                    content: Container(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              3,
                                      width: MediaQuery.of(context).size.width -
                                          45,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          ButtonTheme(
                                            minWidth: 185,
                                            child: RaisedButton(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 20),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(0),
                                              ),
                                              color: Color(0xff674FED),
                                              onPressed: () {
                                                sortValue = 'ASC';
                                                BlocProvider.of<ProductCubit>(
                                                        context)
                                                    .getProdutsWithFilter(
                                                        categoryId:
                                                            widget.category.id,
                                                        form: {
                                                      "sort_type": "price",
                                                      "sort_value": "ASC"
                                                    });

                                                Navigator.pop(mainContext);
                                              },
                                              child: Text(
                                                'По возрастанию цены ',
                                                style: GoogleFonts.montserrat(
                                                    color: Color(0xffffffff),
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 14),
                                              ),
                                            ),
                                          ),
                                          ButtonTheme(
                                            minWidth: 185,
                                            child: RaisedButton(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 20),
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(0),
                                              ),
                                              color: Color(0xff674FED),
                                              onPressed: () {
                                                sortValue = 'DESC';
                                                BlocProvider.of<ProductCubit>(
                                                        context)
                                                    .getProdutsWithFilter(
                                                        categoryId:
                                                            widget.category.id,
                                                        form: {
                                                      "sort_type": "price",
                                                      "sort_value": "DESC"
                                                    });

                                                Navigator.pop(mainContext);
                                              },
                                              child: Text(
                                                'По убыванию цены ',
                                                style: GoogleFonts.montserrat(
                                                    color: Color(0xffffffff),
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 14),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                )),
                        child: Container(
                          child: Row(
                            children: [
                              Icon(
                                Icons.swap_vert,
                                color: secondaryColor,
                                size: 24,
                              ),
                              SizedBox(
                                width: 11,
                              ),
                              Text(
                                'Сортировка',
                                style: GoogleFonts.openSans(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    color: secondaryColor),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                BlocBuilder<ProductCubit, ProductState>(
                  builder: (context, state) {
                    if (!(state is ProductFetched))
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    final List<Product> products =
                        (state as ProductFetched).products;
                    return Container(
                        color: Colors.white,
                        child: Column(
                          children: [
                            // BlocBuilder<FlagCubit, FlagState>(
                            //   builder: (context, state) {
                            //     if (state is FlagLoaded) {
                            //       final flags = (state as FlagLoaded).flags;
                            //       return CategoryTypeSlider(
                            //           flags: flags,
                            //           categoryId: widget.category.id);
                            //     }
                            //     return SizedBox.shrink();
                            //   },
                            // ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: 20, left: 12, right: 12, bottom: 100),
                              child: GridView.count(
                                scrollDirection: Axis.vertical,
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                crossAxisCount: 2,
                                childAspectRatio: 1.10 / 2,
                                children: List.generate(
                                    products.length,
                                    (index) => ProductItem(
                                          categoryId: widget.category.id,
                                          openSlide: callback,
                                          product: products[index],
                                          favoritePage: false,
                                        )),
                              ),
                            ),
                          ],
                        ));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
