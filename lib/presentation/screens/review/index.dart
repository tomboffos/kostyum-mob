import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/product/product_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/presentation/components/stars.dart';

class ReviewIndex extends StatefulWidget {
  final Product product;
  const ReviewIndex({Key key, this.product}) : super(key: key);

  @override
  _ReviewIndexState createState() => _ReviewIndexState();
}

class _ReviewIndexState extends State<ReviewIndex> {
  TextEditingController advantages = new TextEditingController();
  TextEditingController disadvantages = new TextEditingController();
  TextEditingController comments = new TextEditingController();
  int rating = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Новый отзыв',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: primaryColor),
        ),
        centerTitle: false,
        elevation: 1,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.network(
                    widget.product.images[0],
                    width: 52,
                    height: 52,
                    fit: BoxFit.cover,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Text(
                    widget.product.name,
                    style: GoogleFonts.openSans(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: secondDarkColor),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 30),
                child: Center(
                  child: Text(
                    'Оцените данный товар',
                    style: GoogleFonts.openSans(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: secondaryColor),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 30, bottom: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                      onTap: () => setState(() => rating = 1),
                      child: Icon(
                        Icons.star,
                        size: 50,
                        color: rating >= 1 ? Color(0xffF3F804) : Colors.grey,
                      ),
                    ),
                    GestureDetector(
                      onTap: () => setState(() => rating = 2),
                      child: Icon(
                        Icons.star,
                        size: 50,
                        color: rating >= 2 ? Color(0xffF3F804) : Colors.grey,
                      ),
                    ),
                    GestureDetector(
                      onTap: () => setState(() => rating = 3),
                      child: Icon(
                        Icons.star,
                        size: 50,
                        color: rating >= 3 ? Color(0xffF3F804) : Colors.grey,
                      ),
                    ),
                    GestureDetector(
                      onTap: () => setState(() => rating = 4),
                      child: Icon(
                        Icons.star,
                        size: 50,
                        color: rating >= 4 ? Color(0xffF3F804) : Colors.grey,
                      ),
                    ),
                    GestureDetector(
                      onTap: () => setState(() => rating = 5),
                      child: Icon(
                        Icons.star,
                        size: 50,
                        color: rating >= 5 ? Color(0xffF3F804) : Colors.grey,
                      ),
                    ),
                  ],
                ),
                width: MediaQuery.of(context).size.width / 1.5,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                child: TextField(
                  controller: advantages,
                  decoration: InputDecoration(
                    hintText: 'Достоинства',
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0),
                        borderSide: BorderSide(color: secondaryColor)),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 10, right: 10, bottom: 12),
                child: TextField(
                  maxLines: 3,
                  controller: disadvantages,
                  decoration: InputDecoration(
                    hintText: 'Недостатки',
                    hintStyle: GoogleFonts.openSans(
                        fontSize: 14, fontWeight: FontWeight.w400),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0),
                        borderSide: BorderSide(color: secondaryColor)),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: TextField(
                  controller: comments,
                  decoration: InputDecoration(
                    hintText: 'Комментарии',
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0),
                        borderSide: BorderSide(
                          color: secondaryColor,
                        )),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              ButtonTheme(
                minWidth: MediaQuery.of(context).size.width - 20,
                child: FlatButton(
                  splashColor: Colors.black,
                  onPressed: () =>
                      BlocProvider.of<ProductCubit>(context).storeReview(form: {
                    'advantages': advantages.text.toString(),
                    'disadvantages': disadvantages.text.toString(),
                    'comments': comments.text.toString(),
                    'product_id': widget.product.id.toString(),
                    'rating': rating.toString()
                  }, context: context),
                  shape: RoundedRectangleBorder(
                      side: BorderSide(color: secondaryColor, width: 1)),
                  color: Colors.transparent.withOpacity(0),
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Text(
                    'Отправить отзыв',
                    style: GoogleFonts.openSans(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: secondaryColor),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
