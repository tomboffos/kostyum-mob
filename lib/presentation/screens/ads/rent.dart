import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/cubit/product/product_cubit.dart';
import 'package:kostyum/data/models/sale_point.dart';
import 'package:kostyum/presentation/items/product.dart';

class RentScreen extends StatelessWidget {
  final SalePoint salePoint;
  const RentScreen({Key key, this.salePoint}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<ProductCubit>(context).getProdutsWithFilter(
        form: {"sort_type": "price", "sort_value": "ASC", "rent": "1"});
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Назад',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 24, horizontal: 14),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Аренда смокингов',
                style: GoogleFonts.roboto(
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: Color(0xff414141)),
              ),
              SizedBox(
                height: 20,
              ),
              Html(data: salePoint.description),
              BlocBuilder<ProductCubit, ProductState>(
                builder: (context, state) {
                  if (!(state is ProductFetched))
                    return Center(child: CircularProgressIndicator());
                  final products = (state as ProductFetched).products;
                  return Container(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    child: GridView.count(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      crossAxisCount: 2,
                      childAspectRatio: 1.1 / 2,
                      children: List.generate(
                          products.length,
                          (index) => GestureDetector(
                                child: ProductItem(
                                  product: products[index],
                                  action: () {
                                    BlocProvider.of<CartCubit>(context)
                                        .addProduct(
                                            quantity: 1,
                                            product: products[index],
                                            context: context);
                                    BlocProvider.of<CartCubit>(context)
                                        .getCart();
                                  },
                                ),
                              )),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
