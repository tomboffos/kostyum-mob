import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kostyum/data/models/sale_point.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

class PointScreen extends StatefulWidget {
  final SalePoint salePoint;
  const PointScreen({Key key, this.salePoint}) : super(key: key);

  @override
  _PointScreenState createState() => _PointScreenState();
}

class _PointScreenState extends State<PointScreen> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(43.240607, 76.913735),
    zoom: 19.7746,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Назад',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 25, horizontal: 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${widget.salePoint.name}',
                style: GoogleFonts.roboto(
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                    color: Color(0xff414141)),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 14),
                decoration: BoxDecoration(
                    border: Border.all(
                      color: Color.fromRGBO(33, 33, 33, 0.05),
                      width: 14,
                    ),
                    borderRadius: BorderRadius.circular(25)),
                child: Container(
                  height: 222,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage('${widget.salePoint.image}'),
                          fit: BoxFit.cover),
                      borderRadius: BorderRadius.circular(10)),
                ),
              ),
              Container(
                  margin: EdgeInsets.only(top: 18),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: Color(0xffF1F1F1),
                      borderRadius: BorderRadius.circular(15)),
                  height: 250,
                  child: GoogleMap(
                    markers: {
                      Marker(
                          markerId: MarkerId('point'),
                          position: LatLng(widget.salePoint.location['lat'],
                              widget.salePoint.location['lng']))
                    },
                    myLocationEnabled: false,
                    myLocationButtonEnabled: false,
                    initialCameraPosition: CameraPosition(
                      target: LatLng(widget.salePoint.location['lat'],
                          widget.salePoint.location['lng']),
                      zoom: 15.7746,
                    ),
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);
                    },
                  )),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: GestureDetector(
                  onTap: () => launch(
                      'tel:${widget.salePoint.phone.replaceAll(' ', '').replaceAll('(', '').replaceAll(')', '')}'),
                  child: Text(
                    'Номер телефона : ${widget.salePoint.phone}',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: GestureDetector(
                  onTap: () => launch('${widget.salePoint.addressLink}'),
                  child: Text(
                    'Адрес : ${widget.salePoint.address}',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Html(
                data: '${widget.salePoint.description}',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
