import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/discount/cubit/discount_cubit.dart';
import 'package:kostyum/cubit/order/cubit/order_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/presentation/items/action_item.dart';
import 'package:kostyum/presentation/items/bonus_report.dart';

class DiscountScreen extends StatefulWidget {
  const DiscountScreen({Key key}) : super(key: key);

  @override
  _DiscountScreenState createState() => _DiscountScreenState();
}

class _DiscountScreenState extends State<DiscountScreen> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<DiscountCubit>(context).getDiscounts();
    BlocProvider.of<OrderCubit>(context).getOrders();
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Назад',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(left: 40, right: 40, top: 40, bottom: 40),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ButtonTheme(
                      child: FlatButton(
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: secondaryColor)),
                    color: index == 0 ? secondaryColor : Colors.transparent,
                    onPressed: () => setState(() => index = 0),
                    child: Text(
                      'Акции',
                      style: GoogleFonts.roboto(
                          fontSize: 14,
                          fontWeight: FontWeight.w300,
                          color:
                              index == 0 ? Color(0xffffffff) : secondaryColor),
                    ),
                  )),
                  ButtonTheme(
                      child: FlatButton(
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: secondaryColor)),
                    color: index == 1 ? secondaryColor : Colors.transparent,
                    onPressed: () => setState(() => index = 1),
                    child: Text(
                      'Расход',
                      style: GoogleFonts.roboto(
                          fontSize: 14,
                          fontWeight: FontWeight.w300,
                          color: index == 1 ? Colors.white : secondaryColor),
                    ),
                  )),
                  ButtonTheme(
                      child: FlatButton(
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: secondaryColor)),
                    color: index == 2 ? secondaryColor : Colors.transparent,
                    onPressed: () => setState(() => index = 2),
                    child: Text(
                      'Приход',
                      style: GoogleFonts.roboto(
                          fontSize: 14,
                          fontWeight: FontWeight.w300,
                          color: index == 2 ? Colors.white : secondaryColor),
                    ),
                  )),
                ],
              ),
              SizedBox(
                height: 24,
              ),
              Visibility(
                visible: index == 0,
                child: Container(
                  child: BlocBuilder<DiscountCubit, DiscountState>(
                    builder: (context, state) {
                      if (!(state is DiscountFetched))
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      final discounts = (state as DiscountFetched).discounts;
                      return ListView.builder(
                        itemBuilder: (context, index) =>
                            DiscountItem(discount: discounts[index]),
                        itemCount: discounts.length,
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                      );
                    },
                  ),
                ),
              ),
              Visibility(
                visible: index == 1,
                child: BlocBuilder<OrderCubit, OrderState>(
                  builder: (context, state) {
                    if (!(state is OrdersFetched))
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    final orders = (state as OrdersFetched).orders;
                    final spendedBonuses = orders
                        .where((element) => element.spentBonuses > 0)
                        .toList();
                    final earnedBonuses =
                        orders.where((element) => element.bonus > 0).toList();
                    return Container(
                      child: ListView.builder(
                        itemBuilder: (context, index) => BonusItem(
                            earn: false, order: spendedBonuses[index]),
                        itemCount: spendedBonuses.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                      ),
                    );
                  },
                ),
              ),
              Visibility(
                visible: index == 2,
                child: BlocBuilder<OrderCubit, OrderState>(
                  builder: (context, state) {
                    if (!(state is OrdersFetched))
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    final orders = (state as OrdersFetched).orders;
                    final spendedBonuses = orders
                        .where((element) => element.spentBonuses > 0)
                        .toList();
                    final earnedBonuses = orders
                        .where((element) =>
                            element.bonus > 0 && element.paymentStatus.id == 2)
                        .toList();
                    return Container(
                      child: ListView.builder(
                        itemBuilder: (context, index) =>
                            BonusItem(earn: true, order: earnedBonuses[index]),
                        itemCount: earnedBonuses.length,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                      ),
                    );
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
