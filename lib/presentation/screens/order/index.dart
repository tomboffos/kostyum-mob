import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/order/cubit/order_cubit.dart';
import 'package:kostyum/presentation/items/order.dart';

class OrderIndex extends StatelessWidget {
  const OrderIndex({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<OrderCubit>(context).getOrders();
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        title: Text(
          'История заказов',
          style: GoogleFonts.openSans(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Color(0xff191919)),
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: BlocBuilder<OrderCubit, OrderState>(
        builder: (context, state) {
          if (!(state is OrdersFetched))
            return Center(
              child: CircularProgressIndicator(),
            );
          final orders = (state as OrdersFetched).orders;
          return SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 13),
              child: orders.length == 0
                  ? Center(
                      child: Text('Пока что у вас нет заказов',
                          style: GoogleFonts.openSans(fontSize: 14)),
                    )
                  : ListView.builder(
                      itemBuilder: (context, index) => OrderItem(
                        order: orders[index],
                      ),
                      itemCount: orders.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                    ),
            ),
          );
        },
      ),
    );
  }
}
