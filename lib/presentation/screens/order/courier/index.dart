import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/order/cubit/order_cubit.dart';
import 'package:kostyum/data/models/order_courier.dart';
import 'package:kostyum/presentation/items/order.dart';

class OrderCourierIndex extends StatelessWidget {
  const OrderCourierIndex({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<OrderCubit>(context).getCourierOrders();
    return Scaffold(
        appBar: AppBar(
          centerTitle: false,
          backgroundColor: Colors.white,
          title: Text(
            'Заказы',
            style: GoogleFonts.openSans(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: Color(0xff191919)),
          ),
          iconTheme: IconThemeData(color: Colors.black),
        ),
        body: BlocBuilder<OrderCubit, OrderState>(builder: (context, state) {
          if (!(state is OrdersFetched))
            return Center(
              child: CircularProgressIndicator(),
            );
          final orders = (state as OrdersFetched).orders;
          return SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(top: 13),
              child: ListView.builder(
                itemBuilder: (context, index) => OrderCourier(
                  order: orders[index],
                ),
                itemCount: orders.length,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
              ),
            ),
          );
        }));
  }
}
