import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:kostyum/cubit/address/cubit/address_cubit.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/cubit/discount/cubit/discount_cubit.dart';
import 'package:kostyum/cubit/payment_type/cubit/payment_type_cubit.dart';
import 'package:kostyum/cubit/sale_point/cubit/sale_point_cubit.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/address.dart';
import 'package:kostyum/data/models/payment_type.dart';
import 'package:kostyum/data/models/sale_point.dart';
import 'package:kostyum/presentation/items/address_order.dart';
import 'package:kostyum/presentation/items/addresses_item.dart';

class OrderCreate extends StatefulWidget {
  const OrderCreate({Key key}) : super(key: key);

  @override
  _OrderCreateState createState() => _OrderCreateState();
}

class _OrderCreateState extends State<OrderCreate> {
  bool delivery = true;
  Address address;
  PaymentType paymentType;
  bool useBonuses = false;
  SalePoint salePoint;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<CartCubit>(context).getCart();
    BlocProvider.of<AddressCubit>(context).getAddresses();
    BlocProvider.of<PaymentTypeCubit>(context).getPaymentTypes();
    BlocProvider.of<UserCubit>(context).checkIfAuthorized();
    BlocProvider.of<SalePointCubit>(context).getSalePoints();

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'Оформление заказа',
          style: GoogleFonts.openSans(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
        ),
      ),
      body: BlocBuilder<CartCubit, CartState>(
        builder: (context, state) {
          if (!(state is CartProcessed))
            return Center(
              child: CircularProgressIndicator(),
            );
          final cart = (state as CartProcessed).cart;
          int price = 0;
          int quantity = 0;
          int bonus = 0;
          cart.forEach((element) {
            price += element.product.price * element.quantity;
            quantity += element.quantity;
            bonus += element.product.bonus * element.quantity;
          });
          BlocProvider.of<DiscountCubit>(context).fetchDiscount(price: price);
          return BlocBuilder<UserCubit, UserState>(
            builder: (context, state) {
              if (state is UserAreNotLogged)
                return Center(
                  child: Text(
                      'Вы не вошли в систему пожалуйста зайдите в систему'),
                );
              else if (state is UserLogged) {
                final user = (state).user;
                return SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 17, vertical: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  delivery = true;
                                });
                              },
                              child: Container(
                                width:
                                    (MediaQuery.of(context).size.width - 34) /
                                        2,
                                padding: EdgeInsets.symmetric(vertical: 10),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            color: delivery
                                                ? Colors.black
                                                : Colors.white))),
                                child: Text(
                                  'Доставка',
                                  style: GoogleFonts.openSans(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: delivery
                                          ? secondaryColor
                                          : Colors.black),
                                ),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  delivery = false;
                                });
                              },
                              child: Container(
                                width:
                                    (MediaQuery.of(context).size.width - 34) /
                                        2,
                                padding: EdgeInsets.symmetric(vertical: 10),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            color: delivery
                                                ? Colors.white
                                                : Colors.black))),
                                child: Text(
                                  'Самовывоз',
                                  style: GoogleFonts.openSans(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600,
                                      color: delivery
                                          ? Colors.black
                                          : secondaryColor),
                                ),
                              ),
                            )
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 21),
                          child: Text(
                            'Адрес доставки',
                            style: GoogleFonts.openSans(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: secondaryColor),
                          ),
                        ),
                        delivery
                            ? BlocBuilder<AddressCubit, AddressState>(
                                builder: (context, state) {
                                  if (state is AddressInitial)
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  if (state is AddressesFetched) {
                                    final addresses = state.addresses;
                                    return GestureDetector(
                                      onTap: () => showDialog(
                                          context: context,
                                          builder: (_) => AlertDialog(
                                                content: Container(
                                                  height: 200,
                                                  width: 500,
                                                  child: addresses.length == 0
                                                      ? Center(
                                                          child: ButtonTheme(
                                                            splashColor:
                                                                Colors.white,
                                                            child: RaisedButton(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      vertical:
                                                                          14,
                                                                      horizontal:
                                                                          15),
                                                              color:
                                                                  secondaryColor,
                                                              onPressed: () {
                                                                Navigator.pushNamed(
                                                                        context,
                                                                        '/addresses/create')
                                                                    .then((value) =>
                                                                        BlocProvider.of<AddressCubit>(context)
                                                                            .getAddresses());
                                                                Navigator.pop(
                                                                    _);
                                                              },
                                                              child: Text(
                                                                'Добавить адрес',
                                                                style: GoogleFonts.openSans(
                                                                    fontSize:
                                                                        16,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w600,
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                            ),
                                                          ),
                                                        )
                                                      : ListView.builder(
                                                          itemCount:
                                                              addresses.length,
                                                          itemBuilder: (context,
                                                                  index) =>
                                                              GestureDetector(
                                                                onTap: () {
                                                                  setState(() {
                                                                    salePoint =
                                                                        null;
                                                                    address =
                                                                        addresses[
                                                                            index];
                                                                  });
                                                                  Navigator.pop(
                                                                      context);
                                                                },
                                                                child:
                                                                    AddressOrder(
                                                                  address:
                                                                      addresses[
                                                                          index],
                                                                  addresses:
                                                                      addresses,
                                                                ),
                                                              )),
                                                ),
                                              )),
                                      child: Container(
                                        padding: EdgeInsets.all(16),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.black)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              delivery
                                                  ? address != null
                                                      ? 'г. ${address.city.name}, ${address.street}'
                                                      : 'Выберите адрес из списка'
                                                  : 'Выберите точку самовывоза',
                                              style: GoogleFonts.openSans(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xff010101)),
                                            ),
                                            Icon(
                                              Icons.arrow_forward,
                                              color: Colors.black,
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  }
                                  return SizedBox.shrink();
                                },
                              )
                            : BlocBuilder<SalePointCubit, SalePointState>(
                                builder: (context, state) {
                                  if (state is SalePointInitial)
                                    return Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  if (state is SalePointsFetched) {
                                    final salePoints = state.salePoints
                                        .where(
                                            (element) => element.rent == false)
                                        .toList();
                                    return GestureDetector(
                                      onTap: () => showDialog(
                                          context: context,
                                          builder: (_) => AlertDialog(
                                                content: Container(
                                                  height: 200,
                                                  width: 500,
                                                  child: ListView.builder(
                                                      itemCount:
                                                          salePoints.length,
                                                      itemBuilder:
                                                          (context, index) =>
                                                              GestureDetector(
                                                                  onTap: () {
                                                                    setState(
                                                                        () {
                                                                      salePoint =
                                                                          salePoints[
                                                                              index];
                                                                      address =
                                                                          null;
                                                                    });
                                                                    Navigator.pop(
                                                                        context);
                                                                  },
                                                                  child:
                                                                      Container(
                                                                    margin: EdgeInsets.only(
                                                                        bottom:
                                                                            10),
                                                                    padding:
                                                                        EdgeInsets.all(
                                                                            10),
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      border: Border.all(
                                                                          color:
                                                                              secondaryColor),
                                                                    ),
                                                                    child: Text(
                                                                        '${salePoints[index].name}'),
                                                                  ))),
                                                ),
                                              )),
                                      child: Container(
                                        padding: EdgeInsets.all(16),
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                                color: Colors.black)),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              salePoint != null
                                                  ? '${salePoint.name}'
                                                  : 'Выберите точку самовывоза',
                                              style: GoogleFonts.openSans(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400,
                                                  color: Color(0xff010101)),
                                            ),
                                            Icon(
                                              Icons.arrow_forward,
                                              color: Colors.black,
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  }
                                  return SizedBox.shrink();
                                },
                              ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 21),
                          child: Text(
                            'Способ оплаты',
                            style: GoogleFonts.openSans(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: secondaryColor),
                          ),
                        ),
                        BlocBuilder<PaymentTypeCubit, PaymentTypeState>(
                          builder: (context, state) {
                            if (state is PaymentTypeInitial)
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            if (state is PaymentTypeFetched) {
                              final paymentTypes = state.paymentTypes;
                              return GestureDetector(
                                onTap: () => showDialog(
                                    context: context,
                                    builder: (_) => AlertDialog(
                                          content: Container(
                                            height: 200,
                                            width: 500,
                                            child: ListView.builder(
                                                itemCount: paymentTypes.length,
                                                itemBuilder: (context, index) =>
                                                    GestureDetector(
                                                        onTap: () {
                                                          setState(() {
                                                            paymentType =
                                                                paymentTypes[
                                                                    index];
                                                          });
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                        child: Container(
                                                          margin: EdgeInsets
                                                              .symmetric(
                                                                  vertical: 10),
                                                          padding: EdgeInsets
                                                              .symmetric(
                                                                  vertical: 5,
                                                                  horizontal:
                                                                      5),
                                                          decoration: BoxDecoration(
                                                              border: Border.all(
                                                                  color:
                                                                      primaryColor)),
                                                          child: Text(
                                                              '${paymentTypes[index].name}'),
                                                        ))),
                                          ),
                                        )),
                                child: Container(
                                  padding: EdgeInsets.all(16),
                                  margin: EdgeInsets.only(bottom: 20),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black)),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        paymentType != null
                                            ? '${paymentType.name}'
                                            : 'Выберите способ оплаты',
                                        style: GoogleFonts.openSans(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xff010101)),
                                      ),
                                      Icon(
                                        Icons.arrow_forward,
                                        color: Colors.black,
                                      )
                                    ],
                                  ),
                                ),
                              );
                            }
                            return SizedBox.shrink();
                          },
                        ),
                        user.balance != 0
                            ? Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Оплатить бонусами'),
                                  Container(
                                    child: Row(
                                      children: [
                                        Text(
                                          '${user.balance} Б',
                                          style: GoogleFonts.openSans(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w600,
                                              color: secondaryColor),
                                        ),
                                        SizedBox(
                                          width: 14,
                                        ),
                                        CupertinoSwitch(
                                          activeColor: secondaryColor,
                                          value: useBonuses,
                                          onChanged: (bool value) => setState(
                                              () => useBonuses = !useBonuses),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              )
                            : SizedBox.shrink(),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          margin: EdgeInsets.only(bottom: 10),
                          padding: EdgeInsets.only(bottom: 12),
                          decoration: BoxDecoration(
                              border: Border(
                                  bottom:
                                      BorderSide(color: Color(0xffBEBEBE)))),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Сумма заказа ${NumberFormat.decimalPattern().format(quantity)}шт',
                                style: GoogleFonts.openSans(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff131528)),
                              ),
                              Text(
                                '${NumberFormat('###,000', 'zz').format(price)} KZT',
                                style: GoogleFonts.openSans(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xff131528)),
                              )
                            ],
                          ),
                        ),
                        Visibility(
                          visible: false,
                          child: Container(
                            margin: EdgeInsets.only(bottom: 6),
                            padding: EdgeInsets.only(bottom: 12),
                            decoration: BoxDecoration(
                                border: Border(
                                    bottom:
                                        BorderSide(color: Color(0xffBEBEBE)))),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Доставка',
                                  style: GoogleFonts.openSans(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xff131528)),
                                ),
                                Text(
                                  '1200 ₸',
                                  style: GoogleFonts.openSans(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                      color: Color(0xff131528)),
                                )
                              ],
                            ),
                          ),
                        ),
                        useBonuses
                            ? Container(
                                margin: EdgeInsets.only(bottom: 10),
                                padding: EdgeInsets.only(bottom: 12),
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Color(0xffBEBEBE)))),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Оплата бонусами',
                                      style: GoogleFonts.openSans(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff131528)),
                                    ),
                                    Text(
                                      '-${user.balance} Б',
                                      style: GoogleFonts.openSans(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff131528)),
                                    )
                                  ],
                                ),
                              )
                            : SizedBox.shrink(),
                        Container(
                          margin: EdgeInsets.only(bottom: 0, top: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'ИТОГО',
                                style: GoogleFonts.openSans(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xff363585)),
                              ),
                              Text(
                                '${NumberFormat('###,000', 'zz').format(useBonuses ? price - user.balance : price)} KZT',
                                style: GoogleFonts.openSans(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xff14CE04)),
                              )
                            ],
                          ),
                        ),
                        BlocBuilder<DiscountCubit, DiscountState>(
                          builder: (context, state) {
                            if (state is DiscountInitial)
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            if (state is DiscountOneFetched) {
                              final discount = (state).discount;
                              return Container(
                                margin: EdgeInsets.only(bottom: 10, top: 10),
                                padding: EdgeInsets.only(bottom: 12),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Будет начислено',
                                      style: GoogleFonts.openSans(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black),
                                    ),
                                    Text(
                                      // '+ ${NumberFormat('###,000', 'zz').format(cart.fold(0, (previousValue, element) => (element.product.bonus * element.quantity) + previousValue))} Б',
                                      '+${discount.bonusType == 'percent' ? NumberFormat('###,000', 'zz').format(useBonuses ? (price - user.balance) * discount.value * 0.01 : price * discount.value * 0.01) : NumberFormat('###,000', 'zz').format(price + discount.value)} Б',
                                      style: GoogleFonts.openSans(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff674FED)),
                                    )
                                  ],
                                ),
                              );
                            }
                            return SizedBox.shrink();
                          },
                        ),
                      ],
                    ),
                  ),
                );
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            },
          );
        },
      ),
      bottomNavigationBar: BlocBuilder<CartCubit, CartState>(
        builder: (context, state) {
          if (!(state is CartProcessed))
            return Center(
              child: CircularProgressIndicator(),
            );
          final cart = (state as CartProcessed).cart;
          int price = 0;
          cart.forEach((element) {
            price += element.product.price * element.quantity;
          });
          return BlocBuilder<UserCubit, UserState>(
            builder: (context, state) {
              if (!(state is UserLogged)) return SizedBox.shrink();
              final user = (state as UserLogged).user;
              return Container(
                decoration: BoxDecoration(color: Colors.white, boxShadow: []),
                padding: EdgeInsets.symmetric(vertical: 25, horizontal: 10),
                child: ButtonTheme(
                  splashColor: Colors.white,
                  child: RaisedButton(
                    padding: EdgeInsets.symmetric(
                      vertical: 14,
                    ),
                    color: secondaryColor,
                    onPressed: () => delivery
                        ? BlocProvider.of<CartCubit>(context)
                            .storeOrder(context: context, form: {
                            'payment_type_id': paymentType != null
                                ? paymentType.id.toString()
                                : 'null',
                            'delivery_type_id': delivery ? '1' : '2',
                            'address_id': address != null
                                ? address.id.toString()
                                : 'null',
                            'spent_bonuses': user.balance == 0
                                ? '0'
                                : useBonuses
                                    ? user.balance.toString()
                                    : '0'
                          })
                        : BlocProvider.of<CartCubit>(context)
                            .storeOrder(context: context, form: {
                            'payment_type_id': paymentType != null
                                ? paymentType.id.toString()
                                : 'null',
                            'delivery_type_id': delivery ? '1' : '2',
                            'sale_point_id': salePoint != null
                                ? salePoint.id.toString()
                                : 'null',
                            'spent_bonuses': user.balance == 0
                                ? '0'
                                : useBonuses
                                    ? user.balance.toString()
                                    : '0'
                          }),
                    child: Text(
                      'Оформить заказ на ${NumberFormat('###,000', 'zz').format(useBonuses ? price - user.balance : price)} KZT',
                      style: GoogleFonts.openSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
