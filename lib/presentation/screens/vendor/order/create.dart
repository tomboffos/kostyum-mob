import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:kostyum/cubit/address/cubit/address_cubit.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/cubit/payment_type/cubit/payment_type_cubit.dart';
import 'package:kostyum/cubit/sale_point/cubit/sale_point_cubit.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/cubit/users/cubit/users_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/data/models/address.dart';
import 'package:kostyum/data/models/payment_type.dart';
import 'package:kostyum/data/models/sale_point.dart';
import 'package:kostyum/data/models/user.dart';
import 'package:kostyum/presentation/items/address_order.dart';
import 'package:kostyum/presentation/items/addresses_item.dart';

class OrderVendorCreate extends StatefulWidget {
  const OrderVendorCreate({Key key}) : super(key: key);

  @override
  _OrderVendorCreateState createState() => _OrderVendorCreateState();
}

class _OrderVendorCreateState extends State<OrderVendorCreate> {
  bool delivery = true;
  Address address;
  PaymentType paymentType;
  bool useBonuses = false;
  TextEditingController bonus = new TextEditingController();
  TextEditingController price = new TextEditingController();
  TextEditingController userName = new TextEditingController();
  SalePoint salePoint;
  User user;
  String priceText = '';
  String bonusText = '';
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<CartCubit>(context).getCart();
    BlocProvider.of<AddressCubit>(context).getAddresses();
    BlocProvider.of<PaymentTypeCubit>(context).getPaymentTypes();
    BlocProvider.of<UserCubit>(context).checkIfAuthorized();
    BlocProvider.of<SalePointCubit>(context).getSalePoints();
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: false,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          title: Text(
            'Оформление заказа',
            style: GoogleFonts.openSans(
                fontSize: 16, fontWeight: FontWeight.w600, color: Colors.black),
          ),
        ),
        body: BlocBuilder<UserCubit, UserState>(
          builder: (context, state) {
            if (state is UserAreNotLogged)
              return Center(
                child:
                    Text('Вы не вошли в систему пожалуйста зайдите в систему'),
              );
            else if (state is UserLogged) {
              return GestureDetector(
                onTap: () =>
                    FocusScope.of(context).requestFocus(new FocusNode()),
                child: SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 17, vertical: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 21),
                          child: Text(
                            'Выбери точку продажи',
                            style: GoogleFonts.openSans(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: secondaryColor),
                          ),
                        ),
                        BlocBuilder<SalePointCubit, SalePointState>(
                          builder: (context, state) {
                            if (state is SalePointInitial)
                              return Center(
                                child: CircularProgressIndicator(),
                              );
                            if (state is SalePointsFetched) {
                              final salePoints = state.salePoints
                                  .where((element) => element.rent == false)
                                  .toList();
                              return GestureDetector(
                                onTap: () => showDialog(
                                    context: context,
                                    builder: (_) => AlertDialog(
                                          content: Container(
                                            height: 200,
                                            width: 500,
                                            child: ListView.builder(
                                                itemCount: salePoints.length,
                                                itemBuilder: (context, index) =>
                                                    GestureDetector(
                                                        onTap: () {
                                                          setState(() {
                                                            salePoint =
                                                                salePoints[
                                                                    index];
                                                            address = null;
                                                          });
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                        child: Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  bottom: 10),
                                                          padding:
                                                              EdgeInsets.all(
                                                                  10),
                                                          decoration:
                                                              BoxDecoration(
                                                            border: Border.all(
                                                                color:
                                                                    secondaryColor),
                                                          ),
                                                          child: Text(
                                                              '${salePoints[index].name}'),
                                                        ))),
                                          ),
                                        )),
                                child: Container(
                                  padding: EdgeInsets.all(16),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black)),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        salePoint != null
                                            ? '${salePoint.name}'
                                            : 'Выберите точку самовывоза',
                                        style: GoogleFonts.openSans(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w400,
                                            color: Color(0xff010101)),
                                      ),
                                      Icon(
                                        Icons.arrow_forward,
                                        color: Colors.black,
                                      )
                                    ],
                                  ),
                                ),
                              );
                            }
                            return SizedBox.shrink();
                          },
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 21),
                          child: Text(
                            'Цена заказа',
                            style: GoogleFonts.openSans(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: secondaryColor),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8, bottom: 10),
                          padding: EdgeInsets.symmetric(vertical: 0),
                          child: TextField(
                            controller: price,
                            keyboardType: TextInputType.number,
                            onChanged: (value) =>
                                setState(() => priceText = value),
                            decoration: InputDecoration(
                                hintText: '0',
                                suffixIcon: Icon(
                                  Icons.money,
                                  color: secondaryColor,
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: primaryColor,
                                    ),
                                    borderRadius: BorderRadius.circular(0)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: primaryColor,
                                    ),
                                    borderRadius: BorderRadius.circular(0))),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 21),
                          child: Text(
                            'Покупатель',
                            style: GoogleFonts.openSans(
                                fontSize: 14,
                                fontWeight: FontWeight.w600,
                                color: secondaryColor),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8, bottom: 10),
                          padding: EdgeInsets.symmetric(vertical: 0),
                          child: TextField(
                            controller: userName,
                            onChanged: (string) =>
                                BlocProvider.of<UsersCubit>(context)
                                    .getUsers(search: string),
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                hintText: 'Пользователь',
                                suffixIcon: Icon(
                                  Icons.person,
                                  color: secondaryColor,
                                ),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: primaryColor,
                                    ),
                                    borderRadius: BorderRadius.circular(0)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: primaryColor,
                                    ),
                                    borderRadius: BorderRadius.circular(0))),
                          ),
                        ),
                        BlocBuilder<UsersCubit, UsersState>(
                            builder: (context, state) {
                          if (!(state is UsersReceived)) {
                            return SizedBox.shrink();
                          }
                          final users = (state as UsersReceived).users;

                          return Container(
                              margin: EdgeInsets.only(
                                  left: 0, right: 0, bottom: 20),
                              height: 300,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black,
                                  ),
                                  borderRadius: BorderRadius.circular(20)),
                              child: ListView.builder(
                                  itemBuilder: (context, index) =>
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            user = users[index];
                                            userName.text =
                                                user.name.replaceAll(' ', '');
                                          });
                                          BlocProvider.of<UsersCubit>(context)
                                              .clearUsers();
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                              border: Border(
                                                  bottom: BorderSide(
                                                      color: Colors.black))),
                                          padding: EdgeInsets.symmetric(
                                              vertical: 10, horizontal: 20),
                                          child: Text(
                                              '${users[index].phone} - ${users[index].name.replaceAll(' ', '')}'),
                                        ),
                                      ),
                                  itemCount: users.length));
                        }),
                        user != null && user.balance != 0
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.symmetric(vertical: 21),
                                    child: Text(
                                      'Бонусы пользователя : макс(${user.balance})',
                                      style: GoogleFonts.openSans(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600,
                                          color: secondaryColor),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 8, bottom: 10),
                                    padding: EdgeInsets.symmetric(vertical: 0),
                                    child: TextField(
                                      controller: bonus,
                                      keyboardType: TextInputType.number,
                                      onChanged: (value) =>
                                          setState(() => bonusText = value),
                                      decoration: InputDecoration(
                                          hintText: '0',
                                          suffixIcon: Icon(
                                            Icons.money,
                                            color: secondaryColor,
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                color: primaryColor,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(0)),
                                          enabledBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                color: primaryColor,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(0))),
                                    ),
                                  ),
                                ],
                              )
                            : SizedBox.shrink(),
                        SizedBox(
                          height: 20,
                        ),
                        priceText != ''
                            ? Container(
                                margin: EdgeInsets.symmetric(vertical: 10),
                                child: Text(
                                    'Итого к оплате : ${int.parse(priceText) - (bonusText != '' ? int.parse(bonusText) : 0)}',
                                    style: GoogleFonts.openSans(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w600,
                                        color: secondaryColor)),
                              )
                            : SizedBox.shrink(),
                        useBonuses
                            ? Container(
                                margin: EdgeInsets.only(bottom: 10),
                                padding: EdgeInsets.only(bottom: 12),
                                decoration: BoxDecoration(
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Color(0xffBEBEBE)))),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Оплата бонусами',
                                      style: GoogleFonts.openSans(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff131528)),
                                    ),
                                    Text(
                                      '-${user.balance} Б',
                                      style: GoogleFonts.openSans(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Color(0xff131528)),
                                    )
                                  ],
                                ),
                              )
                            : SizedBox.shrink(),
                      ],
                    ),
                  ),
                ),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
        bottomNavigationBar: Container(
          decoration: BoxDecoration(color: Colors.white, boxShadow: []),
          padding: EdgeInsets.symmetric(vertical: 25, horizontal: 10),
          child: ButtonTheme(
            splashColor: Colors.white,
            child: RaisedButton(
              padding: EdgeInsets.symmetric(
                vertical: 14,
              ),
              color: secondaryColor,
              onPressed: () {
                if (bonus.text != '' && int.parse(bonus.text) > user.balance) {
                  AwesomeDialog(
                    context: context,
                    dialogType: DialogType.ERROR,
                    animType: AnimType.BOTTOMSLIDE,
                    title: 'Ошибка',
                    desc: 'Максимальный бонус ${user.balance}',
                  )..show();
                } else {
                  BlocProvider.of<CartCubit>(context)
                      .storeOrderByVendor(context: context, form: {
                    'payment_type_id': '4',
                    'delivery_type_id': '2',
                    'sale_point_id':
                        salePoint != null ? salePoint.id.toString() : '',
                    'spent_bonuses': user != null && user.balance == 0
                        ? '0'
                        : bonus.text != ''
                            ? bonus.text
                            : '0',
                    'user_id': user != null ? user.id.toString() : '0',
                    'price': price.text
                  });
                  setState(() {
                    user = null;
                    bonus.text = '';
                    price = null;
                  });
                }
              },
              child: Text(
                'Оформить заказ',
                style: GoogleFonts.openSans(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
          ),
        ));
  }
}
