import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/order_vendor/cubit/order_vendor_cubit.dart';
import 'package:kostyum/data/constants.dart';
import 'package:kostyum/presentation/items/order_vendor_item.dart';

class OrderVendorIndex extends StatefulWidget {
  const OrderVendorIndex({Key key}) : super(key: key);

  @override
  _OrderVendorIndexState createState() => _OrderVendorIndexState();
}

class _OrderVendorIndexState extends State<OrderVendorIndex> {
  TextEditingController search = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    BlocProvider.of<OrderVendorCubit>(context).getOrders();
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        title: Text(
          'Заказы',
          style: GoogleFonts.openSans(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Color(0xff191919)),
        ),
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: BlocBuilder<OrderVendorCubit, OrderVendorState>(
          builder: (context, state) {
        if (!(state is OrderVendorFetched))
          return Center(
            child: CircularProgressIndicator(),
          );
        final orders = (state as OrderVendorFetched).orders;
        return SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin:
                    EdgeInsets.only(top: 8, bottom: 24, left: 16, right: 16),
                padding: EdgeInsets.symmetric(vertical: 14),
                child: TextField(
                  controller: search,
                  onChanged: (value) =>
                      BlocProvider.of<OrderVendorCubit>(context)
                          .getOrders(search: value),
                  decoration: InputDecoration(
                      hintText: 'Введите номер заказа',
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: secondaryColor,
                          ),
                          borderRadius: BorderRadius.circular(0)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: secondaryColor,
                          ),
                          borderRadius: BorderRadius.circular(0))),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 13),
                child: ListView.builder(
                  itemBuilder: (context, index) => OrderVendorItem(
                    order: orders[index],
                  ),
                  itemCount: orders.length,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
