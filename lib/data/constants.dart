import 'dart:ui';

const primaryColor = Color(0xff202020);
const secondDarkColor = Color(0xff414141);
const secondaryColor = Color(0xff674FED);
const redColor = Color(0xffff0000);