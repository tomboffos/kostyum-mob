import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:kostyum/data/models/cart_product.dart';
import 'package:kostyum/data/models/category.dart';
import 'package:kostyum/data/models/order.dart';
import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/data/models/user.dart';
import 'package:kostyum/presentation/items/order.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:local_auth/local_auth.dart';

import 'models/address.dart';

class NetworkService {
  final apiEndPoint = 'https://kostyum.it-lead.net/api';

  Future<List<dynamic>> fetchCategories({parentId}) async {
    try {
      final response = await get(
          Uri.parse(
              '$apiEndPoint/categories${parentId != null ? '/$parentId' : ''}'),
          headers: {"Accept": "application/json"});
      return jsonDecode(response.body)['data'] as List;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<dynamic>> fetchAds() async {
    try {
      final response = await get(Uri.parse('$apiEndPoint/advertisements'),
          headers: {"Accept": "application/json"});
      return jsonDecode(response.body)['data'] as List;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<List<dynamic>> fetchProducts(
      {categoryId, Map<String, dynamic> filter}) async {
    try {
      final token = (await SharedPreferences.getInstance()).get('token');
      if (filter != null) {
        dynamic additionalFilter =
            (await SharedPreferences.getInstance()).get('filter');
        String queryString = '';
        filter.forEach((key, value) {
          queryString += '&$key=$value';
        });
        if (additionalFilter != null) {
          jsonDecode(additionalFilter).forEach((key, value) {
            queryString += '&$key=$value';
          });
        }
        print(
            '$apiEndPoint/products?${categoryId != null ? 'category=$categoryId' : ''}$queryString');
        if (token != null) {
          final response = await get(
              Uri.parse(
                  '$apiEndPoint/auth/products?${categoryId != null ? 'category=$categoryId' : ''}$queryString'),
              headers: {
                "Authorization": "Bearer $token",
                "Accept": 'application/json'
              });

          return jsonDecode(response.body)['data'] as List;
        }
        final response = await get(
            Uri.parse(
                '$apiEndPoint/products?${categoryId != null ? 'category=$categoryId' : ''}$queryString'),
            headers: {
              "Authorization": "Bearer $token",
              "Accept": 'application/json'
            });
        return jsonDecode(response.body)['data'] as List;
      } else {
        final response = await get(
            Uri.parse(
                '$apiEndPoint/products?${categoryId != null ? 'category=$categoryId' : ''}'),
            headers: {
              "Authorization": "Bearer $token",
              "Accept": "application/json"
            });
        print(response.body);
        return jsonDecode(response.body)['data'] as List;
      }
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<dynamic> login({form, context}) async {
    final device_token =
        (await SharedPreferences.getInstance()).getString('device_token');
    form['token'] = device_token;
    final response = await post(Uri.parse('$apiEndPoint/auth/login'),
        body: form, headers: {"Accept": "application/json"});
    print(response.body);
    print(form);
    if (response.statusCode == 400) {
      showErrorDialog(
          context: context, message: 'Не правильный логин или пароль');
      return null;
    }

    if (response.statusCode == 403) {
      showErrorDialog(
          context: context, message: jsonDecode(response.body)['message']);
      return null;
    }

    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');
      showErrorDialog(context: context, message: errorMessage);
      return null;
    }

    (await SharedPreferences.getInstance()).setString(
        'token', jsonDecode(response.body)['token']['plainTextToken']);

    return jsonDecode(response.body)['user'] as Map<String, dynamic>;
  }

  Future<dynamic> register({context, form}) async {
    final device_token =
        (await SharedPreferences.getInstance()).get('device_token');
    form['token'] = device_token;
    final response = await post(Uri.parse('$apiEndPoint/auth/register'),
        body: form, headers: {'Accept': 'application/json'});
    print(response.statusCode);
    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');
      showErrorDialog(context: context, message: errorMessage);
      return null;
    }

    if (response.statusCode == 403) {
      showErrorDialog(
          context: context, message: jsonDecode(response.body)['message']);
      return null;
    }

    print(jsonDecode(response.body)['user']);
    return jsonDecode(response.body)['user'] as Map<String, dynamic>;
  }

  showDialog() {}
  bool checked = false;
  Future<dynamic> checkUserToken({bool faceId}) async {
    final LocalAuthentication auth = LocalAuthentication();

    final faceIdPermission =
        (await SharedPreferences.getInstance()).getBool('faceId');

    if (faceIdPermission == null)
      (await SharedPreferences.getInstance()).setBool('faceId', false);
    print(faceIdPermission);
    if (faceId != null) {
      bool checkBioMetric = false;
      print(checkBioMetric);
      if (!checked && !faceIdPermission)
        checkBioMetric = await auth.authenticate(
            localizedReason: 'Пожалуйста войдите чтобы авторизоваться',
            useErrorDialogs: true,
            biometricOnly: true);

      if (!checkBioMetric && !checked) return null;
      checked = checkBioMetric;
    }
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(Uri.parse('$apiEndPoint/user'), headers: {
      "Authorization": "Bearer $token",
      'Accept': 'application/json'
    });
    print(jsonDecode(response.body));

    if (response.statusCode == 401)
      (await SharedPreferences.getInstance()).remove('token');
    return jsonDecode(response.body)['data'] as Map<String, dynamic>;
  }

  Future<List<dynamic>> fetchFavorites() async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(Uri.parse('$apiEndPoint/favorites'),
        headers: {"Authorization": "Bearer $token"});
    return jsonDecode(response.body)['data'] as List;
  }

  Future<List<dynamic>> sendFavorite({productId, context}) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = (await post(Uri.parse('$apiEndPoint/favorites/$productId'),
        headers: {
          "Authorization": "Bearer $token",
          "Accept": "application/json"
        }));
    print(response.statusCode);
    if (response.statusCode == 401) Navigator.pushNamed(context, '/profile');
    return jsonDecode(response.body)['data'] as List;
  }

  showSuccessMessage({context, message}) {
    AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        title: 'Успешно',
        desc: '$message',
        showCloseIcon: true)
      ..show();
  }

  showSuccessStoreMessage({context, message}) {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.SUCCES,
      animType: AnimType.BOTTOMSLIDE,
      title: 'Успешно',
      desc: '$message',
      btnOkText: 'Продолжить покупки',
      btnOkOnPress: () => Navigator.pushNamed(context, '/catalog'),
      showCloseIcon: true,
    )..show();
  }

  showErrorDialog({context, message}) {
    AwesomeDialog(
      context: context,
      showCloseIcon: true,
      dialogType: DialogType.ERROR,
      animType: AnimType.BOTTOMSLIDE,
      title: 'Ошибка',
      desc: '${message.replaceAll('[', '').replaceAll(']', '')}',
    )..show();
  }

  Future sendRequestReview({form, context}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    if (token == null)
      showErrorDialog(context: context, message: 'Вы не авторизованы');
    else {
      final response = (await post(Uri.parse('$apiEndPoint/reviews'),
          body: form,
          headers: {
            "Authorization": "Bearer $token",
            'Accept': 'application/json'
          }));
      print(response.body);
      if (response.statusCode == 201)
        showSuccessMessage(context: context, message: 'Отзыв сохранен');
      if (response.statusCode == 422)
        showErrorDialog(context: context, message: 'Ошибка');
    }
  }

  Future<List<dynamic>> fetchAddresses() async {
    try {
      final token = (await SharedPreferences.getInstance()).get('token');

      final response = (await get(Uri.parse('$apiEndPoint/addresses'),
          headers: {"Authorization": "Bearer $token"}));
      return jsonDecode(response.body)['data'] as List;
    } catch (e) {
      return null;
    }
  }

  Future storeAddress({form, context}) async {
    try {
      final token = (await SharedPreferences.getInstance()).get('token');
      final response = (await post(Uri.parse('$apiEndPoint/addresses'),
          headers: {
            "Authorization": "Bearer $token",
            "Accept": "application/json"
          },
          body: form));
      if (response.statusCode == 201) {
        showSuccessMessage(context: context, message: 'Адрес сохранен');
        Timer(Duration(milliseconds: 500), () {
          Navigator.pop(context);
          Navigator.pop(context);
        });
      }

      if (response.statusCode == 422) {
        String errorMessage = '';
        jsonDecode(response.body)['errors']
            .forEach((key, value) => errorMessage += '$value \n');
        showErrorDialog(context: context, message: errorMessage);
      }
    } catch (e) {
      return null;
    }
  }

  Future<List<dynamic>> fetchPaymentTypes() async {
    try {
      final response = await get(Uri.parse('$apiEndPoint/payment-types'));

      return jsonDecode(response.body)['data'] as List;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<dynamic> fetchFlags() async {
    try {
      final response = await get(Uri.parse('$apiEndPoint/flags'));
      return jsonDecode(response.body) as List;
    } catch (e) {
      return [];
    }
  }

  Future<List<dynamic>> fetchFiltersByCategory({Category category}) async {
    try {
      final response =
          await get(Uri.parse('$apiEndPoint/filters?category=${category.id}'));

      return jsonDecode(response.body) as List;
    } catch (e) {
      return [];
    }
  }

  updateRequestUser({Map form, context}) async {
    try {
      final token = (await SharedPreferences.getInstance()).get('token');
      final response = await post(
        Uri.parse('$apiEndPoint/user'),
        body: form,
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        },
      );
      print(form);
      print(response.body);

      if (response.statusCode == 422) {
        String errorMessage = '';
        jsonDecode(response.body)['errors']
            .forEach((key, value) => errorMessage += '$value \n');
        showErrorDialog(context: context, message: errorMessage);
      }
      print(response.statusCode);

      if (response.statusCode == 200) {
        showSuccessMessage(
            context: context, message: 'Данные успешно обновлены');
      }
    } catch (e) {
      print(e);
    }
  }

  processRequestOrder({context, Map form}) async {
    try {
      List<String> cart =
          (await SharedPreferences.getInstance()).getStringList('cart');
      final token = (await SharedPreferences.getInstance()).get('token');
      print(token);
      form['basket'] = jsonEncode(cart.map((e) {
        dynamic product = jsonDecode(e);
        if (product['size'] != null)
          return {
            'product_id': product['product']['id'].toString(),
            'quantity': product['quantity'].toString(),
            'size_id': product['size']['id'].toString()
          };
        return {
          'product_id': product['product']['id'].toString(),
          'quantity': product['quantity'].toString(),
        };
      }).toList());
      final response = (await post(Uri.parse('$apiEndPoint/orders'),
          headers: {
            'Authorization': 'Bearer $token',
            'Accept': 'application/json'
          },
          body: form));
      print(jsonDecode(response.body));
      if (response.statusCode == 422) {
        String errorMessage = '';
        jsonDecode(response.body)['errors']
            .forEach((key, value) => errorMessage += '$value \n');
        showErrorDialog(context: context, message: errorMessage);
      }

      if (response.statusCode == 200) {
        if (jsonDecode(response.body)['payment'] == null) {
          showSuccessStoreMessage(
            context: context,
            message: 'Заказ успешно оформлен',
          );
          (await SharedPreferences.getInstance()).remove('cart');
          return 0;
        }
        if (jsonDecode(response.body)['payment'] != null &&
            jsonDecode(response.body)['order']['payment_type']['id'] == 1) {
          Navigator.pushNamed(context, '/payment/zood',
              arguments: jsonDecode(response.body)['payment']['payment_url']);
          return 0;
        } else if (jsonDecode(response.body)['payment'] != null &&
            jsonDecode(response.body)['order']['payment_type']['id'] == 3) {
          print(
              'https://kostyum.it-lead.net/epay/${jsonDecode(response.body)['order']['id']}');
          Navigator.pushNamed(context, '/payment/epay',
              arguments:
                  'https://kostyum.it-lead.net/epay/${jsonDecode(response.body)['order']['id']}');
          return 0;
        }
        print(jsonDecode(response.body)['payment']);
      }
    } catch (e) {
      print(e);
    }
  }

  Future<List<dynamic>> fetchOrders() async {
    try {
      final token = (await SharedPreferences.getInstance()).get('token');
      final response = (await get(Uri.parse('$apiEndPoint/orders'),
          headers: {'Authorization': 'Bearer $token'}));
      print(response.body);
      return jsonDecode(response.body)['data'] as List;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<dynamic>> fetchOrdersCouriers() async {
    try {
      final token = (await SharedPreferences.getInstance()).get('token');
      final response = (await get(Uri.parse('$apiEndPoint/courier/orders'),
          headers: {
            'Authorization': 'Bearer $token',
            'Accept': 'application/json'
          }));
      return jsonDecode(response.body)['data'] as List;
    } catch (e) {
      print(e);
      return [];
    }
  }

  updateOrderRequest({form, int orderId}) async {
    try {
      final token = (await SharedPreferences.getInstance()).get('token');
      print(token);
      final response = await put(
          Uri.parse('$apiEndPoint/courier/orders/$orderId'),
          headers: {
            'Authorization': 'Bearer $token',
            'Accept': 'application/json'
          },
          body: form);
    } catch (e) {
      print(e);
    }
  }

  Future<List<dynamic>> fetchSalePoints() async {
    try {
      final response = await get(Uri.parse('$apiEndPoint/sale-points'),
          headers: {'Accept': 'application/json'});
      return jsonDecode(response.body)['data'] as List;
    } catch (e) {
      print(e);
      return [];
    }
  }

  Future<List<dynamic>> fetchDiscounts() async {
    try {
      final response = await get(Uri.parse('$apiEndPoint/discounts'),
          headers: {'Accept': 'application/json'});
      print(response.body);
      return jsonDecode(response.body) as List;
    } catch (e) {
      return [];
    }
  }

  storeOrderByVendorRequest({form, context}) async {
    try {
      final token = (await SharedPreferences.getInstance()).get('token');
      final response = (await post(Uri.parse('$apiEndPoint/vendor/orders'),
          headers: {
            'Authorization': 'Bearer $token',
            'Accept': 'application/json'
          },
          body: form));
      print(response.body);
      if (response.statusCode == 422) {
        String errorMessage = '';
        jsonDecode(response.body)['errors']
            .forEach((key, value) => errorMessage += '$value \n');
        showErrorDialog(context: context, message: errorMessage);
      }

      if (response.statusCode == 201) {
        showSuccessMessage(context: context, message: 'Заказ успешно оформлен');

        print(jsonDecode(response.body)['payment']);
      }
    } catch (e) {
      print(e);
    }
  }

  Future<List<dynamic>> fetchUsers({search}) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(
        Uri.parse(
            '$apiEndPoint/vendor/users${search != null ? '?search=$search' : ''}'),
        headers: {'Authorization': 'Bearer $token'});

    return jsonDecode(response.body)['data'] as List;
  }

  sendForgetRequest({context, form}) async {
    final response = await post(Uri.parse('$apiEndPoint/auth/forget'),
        body: form, headers: {'Accept': 'application/json'});
    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');
      showErrorDialog(context: context, message: errorMessage);
      return null;
    }
    return jsonDecode(response.body)['user'];
  }

  saveNewPassword({context, form, User user}) async {
    final response = await post(
        Uri.parse('$apiEndPoint/auth/forget/${user.id}'),
        body: form,
        headers: {'Accept': 'application/json'});
    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');
      showErrorDialog(context: context, message: errorMessage);
      return null;
    }

    if (response.statusCode == 400) {
      showErrorDialog(
          context: context, message: jsonDecode(response.body)['message']);
    }
    (await SharedPreferences.getInstance())
        .setString('token', jsonDecode(response.body)['token']);

    return jsonDecode(response.body)['user'] as Map<String, dynamic>;
  }

  requestConfirmation({context, form, User user}) async {
    print(user);
    final response = await post(
        Uri.parse('$apiEndPoint/auth/register/${user.id}'),
        body: form,
        headers: {'Accept': 'application/json'});
    if (response.statusCode == 422) {
      String errorMessage = '';
      jsonDecode(response.body)['errors']
          .forEach((key, value) => errorMessage += '$value \n');
      showErrorDialog(context: context, message: errorMessage);
      return null;
    }

    if (response.statusCode == 400) {
      showErrorDialog(
          context: context, message: jsonDecode(response.body)['message']);
    }
    (await SharedPreferences.getInstance())
        .setString('token', jsonDecode(response.body)['token']);

    return jsonDecode(response.body)['user'] as Map<String, dynamic>;
  }

  Future<List<dynamic>> fetchCities() async {
    final response = await get(Uri.parse('$apiEndPoint/cities'),
        headers: {'Accept': 'application/json'});
    return jsonDecode(response.body) as List;
  }

  Future<List<dynamic>> fetchVendorOrders({search}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await get(
        Uri.parse(
            '$apiEndPoint/vendor/orders?${search != null ? 'search=$search' : ''}'),
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        });
    return jsonDecode(response.body)['data'] as List;
  }

  updateFetchOrder({form, Order order}) async {
    final token = (await SharedPreferences.getInstance()).get('token');
    final response = await put(
        Uri.parse('$apiEndPoint/vendor/orders/${order.id}'),
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        },
        body: form);
  }

  Future fetchOneDiscount({price}) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(
        Uri.parse('$apiEndPoint/auth/discount?price=$price'),
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        });

    return jsonDecode(response.body);
  }

  Future fetchRemoveAddress({addressId, context}) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response =
        await delete(Uri.parse('$apiEndPoint/addresses/$addressId'), headers: {
      'Accept': 'application/json',
      'Authorization': "Bearer $token"
    });
    print(response.body);
  }

  continueOrderRequest({context, orderId, paymentTypeId}) async {
    final token = (await SharedPreferences.getInstance()).get('token');

    final response = await get(
        Uri.parse(
            '$apiEndPoint/orders/$orderId?payment_type_id=$paymentTypeId'),
        headers: {
          'Authorization': 'Bearer $token',
          'Accept': 'application/json'
        });

    if (response.statusCode == 200) {
      if (jsonDecode(response.body)['payment'] == null) {
        showSuccessStoreMessage(
          context: context,
          message: 'Заказ успешно оформлен',
        );
        (await SharedPreferences.getInstance()).remove('cart');
        return 0;
      }
      if (jsonDecode(response.body)['payment'] != null &&
          jsonDecode(response.body)['order']['payment_type']['id'] == 1) {
        Navigator.pushNamed(context, '/payment/zood',
            arguments: jsonDecode(response.body)['payment']['payment_url']);
        return 0;
      } else if (jsonDecode(response.body)['payment'] != null &&
          jsonDecode(response.body)['order']['payment_type']['id'] == 3) {
        print(
            'https://kostyum.it-lead.net/epay/${jsonDecode(response.body)['order']['id']}');
        Navigator.pushNamed(context, '/payment/epay',
            arguments:
                'https://kostyum.it-lead.net/epay/${jsonDecode(response.body)['order']['id']}');
        return 0;
      }
      print(jsonDecode(response.body)['payment']);
    }
  }
}
