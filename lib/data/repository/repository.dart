import 'dart:convert';

import 'package:kostyum/data/models/ad.dart';
import 'package:kostyum/data/models/address.dart';
import 'package:kostyum/data/models/cart_product.dart';
import 'package:kostyum/data/models/category.dart';
import 'package:kostyum/data/models/city.dart';
import 'package:kostyum/data/models/discount.dart';
import 'package:kostyum/data/models/filter.dart';
import 'package:kostyum/data/models/flag.dart';
import 'package:kostyum/data/models/order.dart';
import 'package:kostyum/data/models/payment_type.dart';
import 'package:kostyum/data/models/sale_point.dart';
import 'package:kostyum/data/models/user.dart';
import 'package:kostyum/data/network_service.dart';
import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/presentation/items/product.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Repository {
  final NetworkService networkService;

  Repository({this.networkService});

  Future<List<Category>> getCategories({categoryId}) async {
    final categoriesJson =
        await networkService.fetchCategories(parentId: categoryId);
    return categoriesJson
        .map((category) => Category.fromJson(category))
        .toList();
  }

  Future<List<Product>> processFavorites() async {
    return (await networkService.fetchFavorites())
        .map((product) => Product.fromJson(product))
        .toList();
  }

  Future<List<Ad>> processAds() async {
    return (await networkService.fetchAds())
        .map((ad) => Ad.fromJson(ad))
        .toList();
  }

  Future<List<Product>> processProducts({categoryId, filter}) async {
    return (await networkService.fetchProducts(
            categoryId: categoryId, filter: filter))
        .map((product) => Product.fromJson(product))
        .toList();
  }

  Future<bool> checkToken() async {
    return (await SharedPreferences.getInstance()).getString('token') != null;
  }

  Future<User> login({form, context}) async {
    return User.fromJson(
        await networkService.login(form: form, context: context));
  }

  Future<User> register({form, context}) async {
    return User.fromJson(
        await networkService.register(form: form, context: context));
  }

  Future<User> checkUserToken({bool faceId}) async {
    return await networkService.checkUserToken(faceId: faceId) != null
        ? User.fromJson(await networkService.checkUserToken())
        : null;
  }

  Future storeCart({List<CartProduct> cart}) async {
    List<String> mainCart =
        cart.map((item) => jsonEncode(item.toJson())).toList();

    (await SharedPreferences.getInstance()).setStringList('cart', mainCart);
  }

  Future<List<CartProduct>> getCart() async {
    List<String> cart =
        (await SharedPreferences.getInstance()).getStringList('cart');
    return cart != null
        ? cart
            .map((product) => CartProduct.fromJson(jsonDecode(product)))
            .toList()
        : [];
  }

  Future<List<Product>> interactFavorite({productId, context}) async {
    return (await networkService.sendFavorite(
            productId: productId, context: context))
        .map((product) => Product.fromJson(product))
        .toList();
  }

  Future storeReview({form, context}) async {
    return (await networkService.sendRequestReview(
        form: form, context: context));
  }

  Future<List<Address>> processAddresses() async {
    return (await networkService.fetchAddresses())
        .map((address) => Address.fromJson(address))
        .toList();
  }

  Future requestAddress({form, context}) async {
    await networkService.storeAddress(form: form, context: context);
  }

  Future<List<PaymentType>> processPaymentTypes() async {
    return (await networkService.fetchPaymentTypes())
        .map((paymentType) => PaymentType.fromJson(paymentType))
        .toList();
  }

  Future<dynamic> processFlags() async {
    return (await networkService.fetchFlags())
        .map((flag) => Flag.fromJson(flag))
        .toList();
  }

  Future<List<Filter>> processFiltersByCategory({Category category}) async {
    return (await networkService.fetchFiltersByCategory(category: category))
        .map((filter) => Filter.fromJson(filter))
        .toList();
  }

  Future updateUser({Map form, context}) async {
    await networkService.updateRequestUser(form: form, context: context);
  }

  Future processOrder({context, form}) async {
    await networkService.processRequestOrder(context: context, form: form);
  }

  Future<List<Order>> processOrders() async {
    return (await networkService.fetchOrders())
        .map((json) => Order.fromJson(json))
        .toList();
  }

  void saveFilters({Map filter}) async {
    dynamic oldFilter = (await SharedPreferences.getInstance()).get('filter');
    print(oldFilter);
    if (oldFilter != null) {
      jsonDecode(oldFilter).forEach((key, value) {
        if (filter[key] != jsonDecode(oldFilter)[key])
          jsonDecode(oldFilter)[key] = filter[key];
      });
      (await SharedPreferences.getInstance())
          .setString('filter', jsonEncode(oldFilter));
    } else {
      (await SharedPreferences.getInstance())
          .setString('filter', jsonEncode(filter));
    }
  }

  void clearFilter() async {
    (await SharedPreferences.getInstance()).remove('filter');
  }

  logout() async {
    (await SharedPreferences.getInstance()).remove('token');
  }

  Future<List<Order>> processCourierOrders() async {
    return (await networkService.fetchOrdersCouriers())
        .map((json) => Order.fromJson(json))
        .toList();
  }

  updateOrderProcess({form, int orderId}) async {
    await (networkService.updateOrderRequest(form: form, orderId: orderId));
  }

  Future<List<SalePoint>> processSalePoints() async {
    return (await networkService.fetchSalePoints())
        .map((point) => SalePoint.fromJson(point))
        .toList();
  }

  Future<List<Discount>> processDiscounts() async {
    return (await networkService.fetchDiscounts())
        .map((e) => Discount.fromJson(e))
        .toList();
  }

  void processOrderByVendor({context, form}) async {
    await networkService.storeOrderByVendorRequest(
        form: form, context: context);
  }

  Future<List<User>> getUsers({search}) async {
    return (await networkService.fetchUsers(search: search))
        .map((e) => User.fromJson(e))
        .toList();
  }

  Future<User> processForgetRequest({context, form}) async {
    return User.fromJson(
        await networkService.sendForgetRequest(context: context, form: form));
  }

  Future<User> processNewPassword({context, form, User user}) async {
    return User.fromJson(await networkService.saveNewPassword(
        context: context, form: form, user: user));
  }

  Future<User> processConfirmation({context, form, User user}) async {
    return User.fromJson(await networkService.requestConfirmation(
        context: context, form: form, user: user));
  }

  Future<List<City>> processCities() async {
    return (await networkService.fetchCities())
        .map((city) => City.fromJson(city))
        .toList();
  }

  Future<List<Order>> processVendorOrders({search}) async {
    return (await networkService.fetchVendorOrders(search: search))
        .map((order) => Order.fromJson(order))
        .toList();
  }

  Future updateOrder({form, Order order}) async {
    await networkService.updateFetchOrder(form: form, order: order);
  }

  Future<Discount> processDiscount({price}) async {
    return Discount.fromJson(
        await networkService.fetchOneDiscount(price: price));
  }

  Future removeProcessAddress({addressId, context}) async {
    await networkService.fetchRemoveAddress(
        addressId: addressId, context: context);
  }

  Future continueOrderProcess({context, orderId, paymentTypeId}) async {
    await networkService.continueOrderRequest(
        context: context, orderId: orderId, paymentTypeId: paymentTypeId);
  }
}
