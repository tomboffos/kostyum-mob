class SalePoint {
  int id;
  String name;
  String image;
  String description;
  dynamic location;
  bool rent;
  String address;
  String addressLink;
  String phone;

  SalePoint.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        image = json['image'],
        description = json['description'],
        location = json['location'],
        rent = json['rent'],
        address = json['address'],
        addressLink = json['address_link'],
        phone = json['phone'];
}
