class PaymentType {
  String name;
  int id;

  PaymentType.fromJson(Map json)
      : name = json['name'],
        id = json['id'];
}
