import 'package:kostyum/data/models/product.dart';
import 'package:kostyum/data/models/size.dart';

class CartProduct {
  Product product;
  int quantity;
  dynamic size;

  CartProduct.fromJson(Map json)
      : product = Product.fromJson(json['product']),
        quantity = json['quantity'],
        size = json['size'] != null ? Size.fromOrderJson(json['size']) : null;

  CartProduct.fromJsonProduct(Map json)
      : product = json['product'],
        quantity = json['quantity'],
        size = json['size'];

  Map<String, dynamic> toJson() {
    return {
      'product': this.product.toJson(),
      'quantity': this.quantity,
      'size': size != null ? this.size.toJson() : null
    };
  }
}
