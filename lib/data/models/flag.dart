class Flag {
  int id;
  String name;
  int order;

  Flag.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        order = json['order'];
}
