class User {
  String name;
  int id;
  String email;
  String image;
  String phone;
  int balance;
  int roleId;
  int orderCount;
  bool notification;

  User.fromJson(Map json)
      : name = json['name'],
        id = json['id'],
        email = json['email'],
        image = json['image'],
        phone = json['phone'],
        balance = json['balance'],
        roleId = json['role_id'],
        orderCount = json['order_count'],
        notification = json['notification'];

  Map<String, dynamic> toJson() {
    return {
      'name': this.name,
      'email': this.email,
      'id': this.id,
      'image': this.image,
      'phone': this.phone,
      'balance': this.balance,
      'role_id': this.roleId,
      'order_count': this.orderCount
    };
  }
}
