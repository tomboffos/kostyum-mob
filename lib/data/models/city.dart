class City {
  int id;
  String name;

  City.fromJson(Map json)
      : id = json['id'],
        name = json['name'];
}
