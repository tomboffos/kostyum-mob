class OrderStatus {
  int id;
  String name;

  OrderStatus.fromJson(Map json)
      : id = json['id'],
        name = json['name'];
}
