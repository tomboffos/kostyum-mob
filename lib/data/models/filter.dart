class Filter {
  int id;
  String name;
  int filterType;
  List<dynamic> values;
  Filter.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        filterType = json['filter_type']['id'],
        values = json['values'];
}
