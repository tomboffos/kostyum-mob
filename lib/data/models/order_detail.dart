import 'package:kostyum/data/models/product.dart';

class OrderDetail {
  int id;
  dynamic product;
  int quantity;

  OrderDetail.fromJson(Map json)
      : id = json['id'],
        product = json['product'],
        quantity = json['quantity'];
}
