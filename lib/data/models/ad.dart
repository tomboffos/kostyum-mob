import 'category.dart';

class Ad {
  int id;
  String name;
  String image;
  String link;
  Category category;

  Ad.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        image = json['image'],
        link = json['link'],
        category = json['category'] != null
            ? Category.fromJson(json['category'])
            : null;
}
