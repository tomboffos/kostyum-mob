class Size {
  int id;
  String name;

  Size.fromJson(Map json)
      : id = json['size']['id'],
        name = json['size']['name'];

  Size.fromOrderJson(Map json)
      : id = json['id'],
        name = json['name'];

  Map<String, dynamic> toJson() {
    return {'name': this.name, 'id': this.id};
  }
}
