class Category {
  int id;
  String name;
  String image;
  dynamic categoryId;
  int childCount;

  Category.fromJson(Map json)
      : id = json['id'] as int,
        name = json['name'],
        image = json['image'],
        categoryId = json['category_id'],
        childCount = json['child_count'];

  Map<String, dynamic> toJson() {
    return {
      'id': this.id,
      'name': this.name,
      'image': this.image,
      'categoryId': this.categoryId,
      'childCount': this.childCount
    };
  }
}
