import 'package:kostyum/data/models/address.dart';
import 'package:kostyum/data/models/order_status.dart';
import 'package:kostyum/data/models/payment_status.dart';
import 'package:kostyum/data/models/payment_type.dart';
import 'package:kostyum/data/models/user.dart';

import 'order_detail.dart';

class Order {
  int id;
  dynamic user;
  dynamic deliveryType;
  dynamic paymentType;
  dynamic address;
  dynamic items;
  int bonus;
  int spentBonuses;
  int quantity;
  int price;
  String createdAt;
  OrderStatus orderStatus;
  PaymentStatus paymentStatus;

  Order.fromJson(Map json)
      : id = json['id'],
        user = json['user'] != null ? User.fromJson(json['user']) : null,
        deliveryType = json['delivery_type'],
        paymentType = json['payment_type'] != null
            ? PaymentType.fromJson(json['payment_type'])
            : null,
        address =
            json['address'] != null ? Address.fromJson(json['address']) : null,
        items =
            json['items'].map((item) => OrderDetail.fromJson(item)).toList(),
        bonus = json['bonus'],
        spentBonuses = json['spent_bonuses'],
        quantity = json['quantity'],
        price = json['price'],
        createdAt = json['created_at'],
        orderStatus = OrderStatus.fromJson(json['order_status']),
        paymentStatus = PaymentStatus.fromJson(json['payment_status']);
}
