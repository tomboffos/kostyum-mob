class PaymentStatus {
  int id;
  String name;

  PaymentStatus.fromJson(Map json)
      : id = json['id'],
        name = json['name'];
}
