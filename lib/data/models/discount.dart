class Discount {
  int id;
  String name;
  String description;
  int value;
  String bonusType;
  int amount;

  Discount.fromJson(Map json)
      : id = json['id'],
        name = json['name'],
        description = json['description'],
        value = json['value'],
        bonusType = json['bonus_type'],
        amount = json['amount'];
}
