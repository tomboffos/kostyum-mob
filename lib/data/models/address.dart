import 'package:kostyum/data/models/city.dart';

class Address {
  int id;
  String street;
  String flatNumber;
  String entrance;
  dynamic city;

  Address.fromJson(Map json)
      : street = json['street'],
        flatNumber = json['flat_number'],
        entrance = json['entrance'],
        id = json['id'],
        city = json['city'] != null ? City.fromJson(json['city']) : null;
}
