import 'dart:convert';

import 'package:kostyum/data/models/category.dart';
import 'package:kostyum/data/models/review.dart';
import 'package:kostyum/data/models/size.dart';
import 'package:kostyum/presentation/screens/favorite.dart';

class Product {
  int id;
  String name;
  int price;
  dynamic discount;
  dynamic bonus;
  List<dynamic> images;
  String video;
  bool stock;
  Category category;
  bool favorites;
  int rating;
  List<dynamic> reviews;
  String description;
  List<dynamic> characteristics;
  int oldPrice;
  List<Size> sizes;
  Product.fromJson(Map json)
      : id = json['id'] as int,
        name = json['name'],
        price = json['price'] as int,
        category = Category.fromJson(json['category']),
        images = json['images'] as List,
        bonus = json['bonus'] as int,
        video = json['video'],
        stock = json['stock'] as bool,
        discount = json['discount'] != null ? json['discount'].round() : 0,
        favorites = json['favorites'] as bool,
        rating = json['rating'].round() as int,
        reviews = json['reviews'].length != 0
            ? json['reviews'].map((review) => Review.fromJson(review)).toList()
            : [],
        description = json['description'],
        characteristics = json['filter'] != null
            ? sortCharacteristics(filter: json['filter'])
            : [],
        oldPrice = json['old_price'],
        sizes = json['sizes'] != null
            ? json['sizes'].length != 0
                ? json['sizes']
                    .map((size) => Size.fromJson(size))
                    .toList()
                    .cast<Size>()
                : [].cast<Size>()
            : [];

  Map<String, dynamic> toJson() {
    return {
      'id': this.id,
      'name': this.name,
      'price': this.price,
      'video': this.video,
      'images': this.images,
      'bonus': this.bonus,
      'stock': this.stock,
      'discount': this.discount,
      'category': this.category.toJson(),
      'favorites': this.favorites,
      'rating': this.rating,
      'reviews': [],
    };
  }
}

List<dynamic> sortCharacteristics({List<dynamic> filter}) {
  if (filter[0]['order'] == null) return filter;
  filter.sort((firstElement, secondElement) => int.parse(firstElement['order'])
      .compareTo(int.parse(secondElement['order'])));
  return filter;
}
