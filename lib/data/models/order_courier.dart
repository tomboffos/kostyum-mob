import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kostyum/cubit/order/cubit/order_cubit.dart';

import '../constants.dart';
import 'order.dart';

class OrderCourier extends StatelessWidget {
  final Order order;

  const OrderCourier({Key key, this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 7, vertical: 10),
          margin: EdgeInsets.only(bottom: 9),
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 1.75,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 60,
                      width: 60,
                      margin: EdgeInsets.only(left: 24),
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(
                                  '${jsonDecode(order.items[0].product['images'])[0]}'),
                              fit: BoxFit.cover)),
                    ),
                    Container(
                      // width: MediaQuery.of(context).size.width / 1.5,
                      child: Text(
                          'Адрес : ${order.address.street}, ${order.address.flatNumber} кв, ${order.address.entrance} подьезд'),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 11),
                      child: Text(
                        'Номер заказа : ${order.id}',
                        style: GoogleFonts.openSans(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: secondaryColor),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 11),
                      child: Text(
                        'Доставлено',
                        style: GoogleFonts.openSans(
                            fontSize: 11,
                            fontWeight: FontWeight.w700,
                            color: secondaryColor),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: Text(
                        '${order.price} тг.',
                        style: GoogleFonts.openSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: secondaryColor),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 8),
                      child: Row(
                        children: [],
                      ),
                    ),
                    Container(
                      child: Text(
                        'Статус оплаты :${order.paymentStatus.name} ',
                        style: GoogleFonts.openSans(
                            fontSize: 11,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff000000)),
                        textAlign: TextAlign.right,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text(
                        'Статус :${order.orderStatus.name} ',
                        style: GoogleFonts.openSans(
                            fontSize: 11,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff000000)),
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            order.paymentStatus.id == 1
                ? ButtonTheme(
                    minWidth: (MediaQuery.of(context).size.width - 20) / 2,
                    child: RaisedButton(
                      padding: EdgeInsets.symmetric(
                        vertical: 14,
                      ),
                      color: secondaryColor,
                      onPressed: () => BlocProvider.of<OrderCubit>(context)
                          .updateOrder(
                              orderId: order.id,
                              form: {'payment_status_id': '2'}),
                      child: Text(
                        'Заказ оплачен',
                        style: GoogleFonts.openSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                    ),
                  )
                : SizedBox.shrink(),
            order.orderStatus.id != 4
                ? ButtonTheme(
                    minWidth: (MediaQuery.of(context).size.width - 20) / 2,
                    child: RaisedButton(
                      padding: EdgeInsets.symmetric(
                        vertical: 14,
                      ),
                      color: secondaryColor,
                      onPressed: () => BlocProvider.of<OrderCubit>(context)
                          .updateOrder(
                              orderId: order.id,
                              form: {'order_status_id': '4'}),
                      child: Text(
                        'Заказ доставлен',
                        style: GoogleFonts.openSans(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                    ),
                  )
                : SizedBox.shrink(),
          ],
        )
      ],
    );
  }
}
