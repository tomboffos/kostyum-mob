import 'package:kostyum/data/models/user.dart';

class Review {
  User user;
  String advantages;
  String disadvantages;
  String comments;
  int rating;

  Review.fromJson(Map json)
      : user = User.fromJson(json['user']),
        advantages = json['advantages'],
        disadvantages = json['disadvantages'],
        comments = json['comments'],
        rating = json['rating'] as int;

  Map<String, dynamic> toJson() {
    return {
      'user': this.user.toJson(),
      'advantages': this.advantages,
      'disadvantages': this.disadvantages,
      'comments': this.comments,
      'rating': this.rating
    };
  }
}
