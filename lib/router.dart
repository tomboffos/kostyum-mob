import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kostyum/cubit/address/cubit/address_cubit.dart';
import 'package:kostyum/cubit/ads/ads_cubit.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/cubit/category/category_cubit.dart';
import 'package:kostyum/cubit/filter/cubit/filter_cubit_cubit.dart';
import 'package:kostyum/cubit/order/cubit/order_cubit.dart';
import 'package:kostyum/cubit/parent_category/parent_category_cubit.dart';
import 'package:kostyum/cubit/payment_type/cubit/payment_type_cubit.dart';
import 'package:kostyum/cubit/product/product_cubit.dart';
import 'package:kostyum/cubit/product_search/cubit/product_search_cubit.dart';
import 'package:kostyum/cubit/sale_point/cubit/sale_point_cubit.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/data/network_service.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:kostyum/presentation/payment/epay.dart';
import 'package:kostyum/presentation/payment/zood.dart';
import 'package:kostyum/presentation/screens/addresses/create.dart';
import 'package:kostyum/presentation/screens/addresses/index.dart';
import 'package:kostyum/presentation/screens/ads/discounts.dart';
import 'package:kostyum/presentation/screens/ads/point.dart';
import 'package:kostyum/presentation/screens/ads/product/index.dart';
import 'package:kostyum/presentation/screens/ads/rent.dart';
import 'package:kostyum/presentation/screens/cards/index.dart';
import 'package:kostyum/presentation/screens/cart/index.dart';
import 'package:kostyum/presentation/screens/catalog.dart';
import 'package:kostyum/presentation/screens/favorite.dart';
import 'package:kostyum/presentation/screens/filters/index.dart';
import 'package:kostyum/presentation/screens/filters/price.dart';
import 'package:kostyum/presentation/screens/filters/show.dart';
import 'package:kostyum/presentation/screens/home.dart';
import 'package:kostyum/presentation/screens/order/courier/index.dart';
import 'package:kostyum/presentation/screens/order/create.dart';
import 'package:kostyum/presentation/screens/order/index.dart';
import 'package:kostyum/presentation/screens/product/show.dart';
import 'package:kostyum/presentation/screens/product_catalog.dart';
import 'package:kostyum/presentation/screens/profile/edit.dart';
import 'package:kostyum/presentation/screens/profile/index.dart';
import 'package:kostyum/presentation/screens/review/index.dart';
import 'package:kostyum/presentation/screens/update_user/edit_singular.dart';
import 'package:kostyum/presentation/screens/vendor/order/create.dart';
import 'package:kostyum/presentation/screens/vendor/order/index.dart';

import 'cubit/city/cubit/city_cubit.dart';
import 'cubit/discount/cubit/discount_cubit.dart';
import 'cubit/favorite/cubit/favorite_cubit.dart';
import 'cubit/flag/cubit/flag_cubit.dart';
import 'cubit/order_vendor/cubit/order_vendor_cubit.dart';
import 'cubit/users/cubit/users_cubit.dart';

class AppRouter {
  Repository repository;

  AppRouter() {
    repository = Repository(networkService: NetworkService());
  }

  Route generateRouter(RouteSettings settings) {
    dynamic arguments = settings.arguments;

    switch (settings.name) {
      case '/catalog':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          CategoryCubit(repository: repository)),
                ], child: Catalog()));
      case '/start':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          FilterCubitCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          CategoryCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => AdsCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          ParentCategoryCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => UserCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          FavoriteCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => CartCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => FlagCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          ProductCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          ProductSearchCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          SalePointCubit(repository: repository)),
                ], child: HomePage()));
      case '/catalog':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          FilterCubitCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          CategoryCubit(repository: repository)),
                ], child: Catalog()));

      case '/cart':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            FilterCubitCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            CategoryCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => AdsCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ParentCategoryCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => UserCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            FavoriteCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => FlagCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ProductCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ProductSearchCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            SalePointCubit(repository: repository)),
                  ],
                  child: CartIndex(),
                ));
      case '/profile':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            FilterCubitCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            CategoryCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => AdsCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ParentCategoryCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => UserCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            FavoriteCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => FlagCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ProductCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ProductSearchCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            SalePointCubit(repository: repository)),
                  ],
                  child: ProfileIndex(),
                ));

      case '/catalog':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          CategoryCubit(repository: repository)),
                ], child: Catalog()));
      case '/favorite':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          FilterCubitCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          CategoryCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => AdsCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          ParentCategoryCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => UserCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          FavoriteCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => CartCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => FlagCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          ProductCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          ProductSearchCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          SalePointCubit(repository: repository)),
                ], child: Favorite()));
      case '/products':
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => ProductCubit(repository: repository),
                    ),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => FlagCubit(repository: repository)),
                  ],
                  child: ProductCatalog(
                    category: arguments,
                  ),
                ));

      case '/product':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            FavoriteCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository))
                  ],
                  child: ProductShow(
                    product: arguments,
                  ),
                ));

      case '/reviews':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => ProductCubit(repository: repository),
                  child: ReviewIndex(
                    product: arguments,
                  ),
                ));
      case '/profile/edit':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                create: (BuildContext context) =>
                    UserCubit(repository: repository),
                child: ProfileEdit(user: arguments)));
      case '/orders/index':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                    create: (context) => OrderCubit(repository: repository),
                  ),
                  BlocProvider(
                      create: (context) => CartCubit(repository: repository))
                ], child: OrderIndex()));
      case '/cards/index':
        return MaterialPageRoute(builder: (context) => CardIndex());
      case '/filter/index':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                          create: (context) =>
                              FilterCubitCubit(repository: repository)),
                    ],
                    child: FilterIndex(
                      category: arguments,
                    )));
      case '/point':
        return MaterialPageRoute(
            builder: (context) => PointScreen(
                  salePoint: arguments,
                ));
      case '/rent':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            ProductCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                  ],
                  child: RentScreen(
                    salePoint: arguments,
                  ),
                ));
      case '/discounts':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) => OrderCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          DiscountCubit(repository: repository)),
                ], child: DiscountScreen()));
      case '/filter/show/multiple':
        return MaterialPageRoute(
            builder: (context) => FilterShow(
                  isMultiple: true,
                ));
      case '/filter/show':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => FilterCubitCubit(repository: repository),
                  child: FilterShow(
                    isMultiple: false,
                    filter: arguments,
                  ),
                ));
      case '/filter/price':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => FilterCubitCubit(repository: repository),
                  child: FilterPrice(),
                ));
      case '/addresses/index':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => AddressCubit(repository: repository),
                  child: AddressesIndex(),
                ));
      case '/addresses/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            AddressCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CityCubit(repository: repository)),
                  ],
                  child: AddressCreate(),
                ));
      case '/order/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            AddressCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            PaymentTypeCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => UserCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            SalePointCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            DiscountCubit(repository: repository)),
                  ],
                  child: OrderCreate(),
                ));
      case '/orders/courier':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) => OrderCubit(repository: repository))
                  ],
                  child: OrderCourierIndex(),
                ));

      case '/payment/zood':
        return MaterialPageRoute(
            builder: (context) => ZoodPayment(
                  url: arguments,
                ));
      case '/payment/epay':
        return MaterialPageRoute(
            builder: (context) => EpayPayment(
                  url: arguments,
                ));
      case '/ad/products':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ProductCubit(repository: repository)),
                  ],
                  child: ProductAdsIndex(
                    ad: arguments,
                  ),
                ));
      case '/order/vendor/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) => CartCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          AddressCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          PaymentTypeCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => UserCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          SalePointCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => UsersCubit(repository: repository)),
                ], child: OrderVendorCreate()));
      case '/order/vendor/index':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          OrderVendorCubit(repository: repository)),
                ], child: OrderVendorIndex()));
      case '/user/profile/edit/singular':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                          create: (context) =>
                              UserCubit(repository: repository)),
                    ],
                    child: EditUserParameter(
                        name: arguments['name'],
                        label: arguments['label'],
                        mask: arguments['mask'],
                        placeholder: arguments['placeholder'])));
      default:
        return null;
    }
  }

  Route generateMainRouter(RouteSettings settings) {
    dynamic arguments = settings.arguments;

    switch (settings.name) {
      case '/catalog':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          CategoryCubit(repository: repository)),
                ], child: Catalog()));
      case '/start':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          FilterCubitCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          CategoryCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => AdsCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          ParentCategoryCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => UserCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          FavoriteCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => CartCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => FlagCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          ProductCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          ProductSearchCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          SalePointCubit(repository: repository)),
                ], child: HomePage()));

      case '/products':
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => ProductCubit(repository: repository),
                    ),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => FlagCubit(repository: repository)),
                  ],
                  child: ProductCatalog(
                    category: arguments,
                  ),
                ));

      case '/product':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            FavoriteCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository))
                  ],
                  child: ProductShow(
                    product: arguments,
                  ),
                ));

      case '/reviews':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => ProductCubit(repository: repository),
                  child: ReviewIndex(
                    product: arguments,
                  ),
                ));

      case '/filter/index':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                          create: (context) =>
                              FilterCubitCubit(repository: repository)),
                    ],
                    child: FilterIndex(
                      category: arguments,
                    )));
      case '/point':
        return MaterialPageRoute(
            builder: (context) => PointScreen(
                  salePoint: arguments,
                ));
      case '/rent':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            ProductCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                  ],
                  child: RentScreen(
                    salePoint: arguments,
                  ),
                ));

      case '/filter/show/multiple':
        return MaterialPageRoute(
            builder: (context) => FilterShow(
                  isMultiple: true,
                ));
      case '/filter/show':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => FilterCubitCubit(repository: repository),
                  child: FilterShow(
                    isMultiple: false,
                    filter: arguments,
                  ),
                ));
      case '/filter/price':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => FilterCubitCubit(repository: repository),
                  child: FilterPrice(),
                ));

      case '/addresses/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            AddressCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CityCubit(repository: repository)),
                  ],
                  child: AddressCreate(),
                ));
      case '/order/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            AddressCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            PaymentTypeCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => UserCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            SalePointCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            DiscountCubit(repository: repository)),
                  ],
                  child: OrderCreate(),
                ));
      case '/reviews':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => ProductCubit(repository: repository),
                  child: ReviewIndex(
                    product: arguments,
                  ),
                ));
      case '/ad/products':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ProductCubit(repository: repository)),
                  ],
                  child: ProductAdsIndex(
                    ad: arguments,
                  ),
                ));
      case '/payment/zood':
        return MaterialPageRoute(
            builder: (context) => ZoodPayment(
                  url: arguments,
                ));
      case '/payment/epay':
        return MaterialPageRoute(
            builder: (context) => EpayPayment(
                  url: arguments,
                ));

      default:
        return null;
    }
  }

  Route generateCatalogRoute(RouteSettings settings) {
    dynamic arguments = settings.arguments;

    switch (settings.name) {
      case '/product':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            FavoriteCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository))
                  ],
                  child: ProductShow(
                    product: arguments,
                  ),
                ));
      case '/catalog':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          CategoryCubit(repository: repository)),
                ], child: Catalog()));
      case '/reviews':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => ProductCubit(repository: repository),
                  child: ReviewIndex(
                    product: arguments,
                  ),
                ));
      case '/order/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            AddressCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            PaymentTypeCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => UserCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            SalePointCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            DiscountCubit(repository: repository)),
                  ],
                  child: OrderCreate(),
                ));
      case '/filter/show':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => FilterCubitCubit(repository: repository),
                  child: FilterShow(
                    isMultiple: false,
                    filter: arguments,
                  ),
                ));
      case '/filter/price':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => FilterCubitCubit(repository: repository),
                  child: FilterPrice(),
                ));

      case '/filter/index':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                          create: (context) =>
                              FilterCubitCubit(repository: repository)),
                    ],
                    child: FilterIndex(
                      category: arguments,
                    )));
      case '/products':
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => ProductCubit(repository: repository),
                    ),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => FlagCubit(repository: repository)),
                  ],
                  child: ProductCatalog(
                    category: arguments,
                  ),
                ));
      case '/payment/zood':
        return MaterialPageRoute(
            builder: (context) => ZoodPayment(
                  url: arguments,
                ));
      case '/payment/epay':
        return MaterialPageRoute(
            builder: (context) => EpayPayment(
                  url: arguments,
                ));
      case '/addresses/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            AddressCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CityCubit(repository: repository)),
                  ],
                  child: AddressCreate(),
                ));

      default:
        return null;
    }
  }

  Route generateCartIndex(RouteSettings settings) {
    final arguments = settings.arguments;
    switch (settings.name) {
      case '/products':
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => ProductCubit(repository: repository),
                    ),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => FlagCubit(repository: repository)),
                  ],
                  child: ProductCatalog(
                    category: arguments,
                  ),
                ));
      case '/cart':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            FilterCubitCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            CategoryCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => AdsCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ParentCategoryCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => UserCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            FavoriteCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => FlagCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ProductCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ProductSearchCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            SalePointCubit(repository: repository)),
                  ],
                  child: CartIndex(),
                ));
      case '/product':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            FavoriteCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository))
                  ],
                  child: ProductShow(
                    product: arguments,
                  ),
                ));
      case '/reviews':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => ProductCubit(repository: repository),
                  child: ReviewIndex(
                    product: arguments,
                  ),
                ));
      case '/catalog':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          CategoryCubit(repository: repository)),
                ], child: Catalog()));
      case '/order/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            AddressCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            PaymentTypeCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => UserCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            SalePointCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            DiscountCubit(repository: repository)),
                  ],
                  child: OrderCreate(),
                ));
      case '/filter/show':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => FilterCubitCubit(repository: repository),
                  child: FilterShow(
                    isMultiple: false,
                    filter: arguments,
                  ),
                ));
      case '/filter/price':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => FilterCubitCubit(repository: repository),
                  child: FilterPrice(),
                ));

      case '/filter/index':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                          create: (context) =>
                              FilterCubitCubit(repository: repository)),
                    ],
                    child: FilterIndex(
                      category: arguments,
                    )));

      case '/payment/zood':
        return MaterialPageRoute(
            builder: (context) => ZoodPayment(
                  url: arguments,
                ));
      case '/payment/epay':
        return MaterialPageRoute(
            builder: (context) => EpayPayment(
                  url: arguments,
                ));
      case '/addresses/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            AddressCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CityCubit(repository: repository)),
                  ],
                  child: AddressCreate(),
                ));

      default:
        return null;
    }
  }

  Route generateFavoriteRoute(RouteSettings settings) {
    final arguments = settings.arguments;
    switch (settings.name) {
      case '/addresses/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            AddressCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CityCubit(repository: repository)),
                  ],
                  child: AddressCreate(),
                ));
      case '/cart':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            FilterCubitCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            CategoryCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => AdsCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ParentCategoryCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => UserCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            FavoriteCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => FlagCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ProductCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ProductSearchCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            SalePointCubit(repository: repository)),
                  ],
                  child: CartIndex(),
                ));
      case '/product':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            FavoriteCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository))
                  ],
                  child: ProductShow(
                    product: arguments,
                  ),
                ));
      case '/reviews':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => ProductCubit(repository: repository),
                  child: ReviewIndex(
                    product: arguments,
                  ),
                ));
      case '/catalog':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          CategoryCubit(repository: repository)),
                ], child: Catalog()));
      case '/order/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            AddressCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            PaymentTypeCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => UserCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            SalePointCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            DiscountCubit(repository: repository)),
                  ],
                  child: OrderCreate(),
                ));
      case '/filter/show':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => FilterCubitCubit(repository: repository),
                  child: FilterShow(
                    isMultiple: false,
                    filter: arguments,
                  ),
                ));
      case '/filter/price':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => FilterCubitCubit(repository: repository),
                  child: FilterPrice(),
                ));

      case '/filter/index':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                          create: (context) =>
                              FilterCubitCubit(repository: repository)),
                    ],
                    child: FilterIndex(
                      category: arguments,
                    )));

      case '/payment/zood':
        return MaterialPageRoute(
            builder: (context) => ZoodPayment(
                  url: arguments,
                ));
      case '/payment/epay':
        return MaterialPageRoute(
            builder: (context) => EpayPayment(
                  url: arguments,
                ));
      case '/products':
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => ProductCubit(repository: repository),
                    ),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => FlagCubit(repository: repository)),
                  ],
                  child: ProductCatalog(
                    category: arguments,
                  ),
                ));
      case '/favorite':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          FilterCubitCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          CategoryCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => AdsCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          ParentCategoryCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => UserCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          FavoriteCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => CartCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => FlagCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          ProductCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          ProductSearchCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          SalePointCubit(repository: repository)),
                ], child: Favorite()));

      default:
        return null;
    }
  }

  Route generateProfileRoute(RouteSettings settings) {
    dynamic arguments = settings.arguments;
    switch (settings.name) {
      case '/profile':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            FilterCubitCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            CategoryCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => AdsCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ParentCategoryCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => UserCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            FavoriteCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CartCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => FlagCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ProductCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            ProductSearchCubit(repository: repository)),
                    BlocProvider(
                        create: (context) =>
                            SalePointCubit(repository: repository)),
                  ],
                  child: ProfileIndex(),
                ));
      case '/order/vendor/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) => CartCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          AddressCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          PaymentTypeCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => UserCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          SalePointCubit(repository: repository)),
                  BlocProvider(
                      create: (context) => UsersCubit(repository: repository)),
                ], child: OrderVendorCreate()));
      case '/order/vendor/index':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) =>
                          OrderVendorCubit(repository: repository)),
                ], child: OrderVendorIndex()));
      case '/user/profile/edit/singular':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider(
                          create: (context) =>
                              UserCubit(repository: repository)),
                    ],
                    child: EditUserParameter(
                        name: arguments['name'],
                        label: arguments['label'],
                        mask: arguments['mask'],
                        placeholder: arguments['placeholder'])));
      case '/profile/edit':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                create: (BuildContext context) =>
                    UserCubit(repository: repository),
                child: ProfileEdit(user: arguments)));
      case '/orders/index':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                    create: (context) => OrderCubit(repository: repository),
                  ),
                  BlocProvider(
                      create: (context) => CartCubit(repository: repository))
                ], child: OrderIndex()));
      case '/cards/index':
        return MaterialPageRoute(builder: (context) => CardIndex());
      case '/discounts':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(providers: [
                  BlocProvider(
                      create: (context) => OrderCubit(repository: repository)),
                  BlocProvider(
                      create: (context) =>
                          DiscountCubit(repository: repository)),
                ], child: DiscountScreen()));
      case '/addresses/create':
        return MaterialPageRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                        create: (context) =>
                            AddressCubit(repository: repository)),
                    BlocProvider(
                        create: (context) => CityCubit(repository: repository)),
                  ],
                  child: AddressCreate(),
                ));
      case '/addresses/index':
        return MaterialPageRoute(
            builder: (context) => BlocProvider(
                  create: (context) => AddressCubit(repository: repository),
                  child: AddressesIndex(),
                ));
      case '/payment/zood':
        return MaterialPageRoute(
            builder: (context) => ZoodPayment(
                  url: arguments,
                ));
      case '/payment/epay':
        return MaterialPageRoute(
            builder: (context) => EpayPayment(
                  url: arguments,
                ));
      default:
        return null;
    }
  }
}
