import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/number_symbols.dart';
import 'package:intl/number_symbols_data.dart';
import 'package:kostyum/cubit/ads/ads_cubit.dart';
import 'package:kostyum/cubit/cart/cubit/cart_cubit.dart';
import 'package:kostyum/cubit/favorite/cubit/favorite_cubit.dart';
import 'package:kostyum/cubit/filter/cubit/filter_cubit_cubit.dart';
import 'package:kostyum/cubit/flag/cubit/flag_cubit.dart';
import 'package:kostyum/cubit/parent_category/parent_category_cubit.dart';
import 'package:kostyum/cubit/product/product_cubit.dart';
import 'package:kostyum/cubit/user/cubit/user_cubit.dart';
import 'package:kostyum/data/network_service.dart';
import 'package:kostyum/data/repository/repository.dart';
import 'package:kostyum/presentation/components/menu.dart';
import 'package:kostyum/router.dart';
import 'package:kostyum/services/notification.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'cubit/category/category_cubit.dart';
import 'cubit/product_search/cubit/product_search_cubit.dart';
import 'cubit/sale_point/cubit/sale_point_cubit.dart';
import 'data/constants.dart';

void main() {
  numberFormatSymbols['zz'] = new NumberSymbols(
    NAME: "zz",
    DECIMAL_SEP: '.',
    GROUP_SEP: '\u00A0',
    PERCENT: '%',
    ZERO_DIGIT: '0',
    PLUS_SIGN: '+',
    MINUS_SIGN: '-',
    EXP_SYMBOL: 'e',
    PERMILL: '\u2030',
    INFINITY: '\u221E',
    NAN: 'NaN',
    DECIMAL_PATTERN: '#,##0.###',
    SCIENTIFIC_PATTERN: '#E0',
    PERCENT_PATTERN: '#,##0%',
    CURRENCY_PATTERN: '\u00A4#,##0.00',
    DEF_CURRENCY_CODE: 'AUD',
  );

  runApp(MyApp(
    router: AppRouter(),
  ));
}

class MyApp extends StatefulWidget {
  final AppRouter router;
  MyApp({Key key, this.router}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Repository repository = new Repository(networkService: NetworkService());

  initFaceId() async {
    if ((await SharedPreferences.getInstance()).getBool('faceId') == null)
      (await SharedPreferences.getInstance()).setBool('faceId', true);
  }

  @override
  void initState() {
    super.initState();
    FirebaseService().init();
    initFaceId();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Kostyum Kz',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: primaryColor,
          appBarTheme: AppBarTheme(
            backwardsCompatibility: false,
            systemOverlayStyle: SystemUiOverlayStyle.dark, // 2
          )),
      home: MultiBlocProvider(
        providers: [
          BlocProvider(
              create: (context) => FilterCubitCubit(repository: repository)),
          BlocProvider(
              create: (context) => CategoryCubit(repository: repository)),
          BlocProvider(create: (context) => AdsCubit(repository: repository)),
          BlocProvider(
              create: (context) => ParentCategoryCubit(repository: repository)),
          BlocProvider(create: (context) => UserCubit(repository: repository)),
          BlocProvider(
              create: (context) => FavoriteCubit(repository: repository)),
          BlocProvider(create: (context) => CartCubit(repository: repository)),
          BlocProvider(create: (context) => FlagCubit(repository: repository)),
          BlocProvider(
              create: (context) => ProductCubit(repository: repository)),
          BlocProvider(
              create: (context) => ProductSearchCubit(repository: repository)),
          BlocProvider(
              create: (context) => SalePointCubit(repository: repository)),
        ],
        child: Menu(
          router: widget.router,
        ),
      ),
    );
  }
}

int numberWithSpaces(x) {}
